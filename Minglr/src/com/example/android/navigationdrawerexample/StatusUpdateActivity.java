package com.example.android.navigationdrawerexample;

import java.util.ArrayList;
import java.util.zip.Inflater;

import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.adapters.ArrayAdapterNotifications;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.views.View_StatusUpdate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

public class StatusUpdateActivity extends Activity {

	public StatusUpdateActivity() {
		// TODO Auto-generated constructor stub
	}
	
	public void showPostStatusUpdate(){
		System.out.println("showPostStatusUpdate");
		
	}
	
	// ======================================================================================================== //
    // ****************************************** NOTIFICATION LIST ******************************************* //
    // ======================================================================================================== //
	
	/**
	 * The notification list must be assigned so that we can hide and remove it
	 */
	private AlertDialog _notificationList;
	
	/*
	 * The front-end function call. Will display and download the notifications
	 */
	protected void displayNotificationList(boolean display){
		// - Hide notification list
		if(!display){
			
		}
		// - Show notification list
		else{
			
			// - Create the basics for the notification popupView
			// - and assign a new view as it
			View convertView = createNotificationListView();
	        
	        // - Assign listView
	        final ListView lv = (ListView) convertView.findViewById(R.id.listView1);
	        
	        // - Show the view, even before we have the notification
	        _notificationList.show();
	        
	        //
	        // - Download the nofifications
	        downloadNotifications(new ApiDownloadInterface() {
				
	        	// - NOT USED
				@Override
				public void objectFetchComplete(ApiObject result, String variableName) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void arrayFetchComplete(ArrayList<ApiObject> result,
						String variableName) {

					// - Create an adapter
			        ArrayAdapterNotifications adapter = new ArrayAdapterNotifications(
			        		getBaseContext(), 
			        		android.R.layout.simple_list_item_2, 
			        		result);
			        
			        // - Set adapter for listView
			        lv.setAdapter(adapter);
					
				}
			});
	        
	        
	        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					
					System.out.println("Clicked row: " + arg2);
					ApiObject notification = (ApiObject) arg0.getItemAtPosition(arg2);
					ApiObject metaData = ApiObjectHandler.refineObject(notification, "metadata");
					
					//
					// - What type of notification
					String type = notification.getObject().get("type");
					
					//
					// - See if this is a comment or nice on the users post
					if(type.equals("new_status_comment")){
						Intent commentIntent = new Intent(getBaseContext(), CommentActivity.class);
						commentIntent.putExtra("status_id", metaData.getObject().get("status_id"));
						startActivity(commentIntent);
					}
					// - Open user profile (friend request and so on)
					else if(type.equals("friend_request")){
						Intent profileIntent = new Intent(getBaseContext(), ActivityDefaultProfile.class);
						profileIntent.putExtra("user_id", metaData.getObject().get("user_id"));
						startActivity(profileIntent);
					}
					
					
					_notificationList.dismiss();
				}
			});
		}
	}
	
	/**
	 * Simply creates and allocates/assignes the notification popupView
	 * @return
	 */
	private View createNotificationListView(){
		
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
		// - Get inflater
		LayoutInflater inflater = getLayoutInflater();
        
		//
		// - Create the convert view
        View convertView = (View) inflater.inflate(R.layout.popup_cluster_list, null);
        
        // - Assign values to alertDialog
        dialogBuilder.setView(convertView);
        dialogBuilder.setTitle("Notifications");
        
        //
        // - By doing this, creating the _notificaitonList from the dialogBuilder like this
        // - allows us to "dismiss()" the _notificationList later
        _notificationList = dialogBuilder.create();
        
        return convertView;
	}
	
	private void downloadNotifications(final ApiDownloadInterface interfaceObject){
		
		ApiDownload notificationsDownload = new ApiDownload();
		notificationsDownload.startDownload("user/notifications", "notifications", false, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
				interfaceObject.arrayFetchComplete(result, variableName);
			}
		});
	}
	
	// ======================================================================================================== //
    // ****************************************** NOTIFICATION LIST ******************************************* //
    // ======================================================================================================== //

}
