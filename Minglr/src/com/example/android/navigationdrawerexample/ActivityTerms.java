package com.example.android.navigationdrawerexample;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * This class only shows the text for the terms and conditions from the settings menu
 * @author Robert
 *
 */
public class ActivityTerms extends Activity {

	public ActivityTerms() {
		// TODO Auto-generated constructor stub
	}

	@Override 
	protected void onCreate(Bundle bundle){
		setContentView(R.layout.activity_plain_text_view);
		TextView textView = (TextView)findViewById(R.id.textView1);
		textView.setText(R.string.lorem_ipsum);
		
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		mainLayout.setBackgroundResource(R.drawable.clean_background);
		
		textView.setMaxLines(10000);
		textView.setMovementMethod(new ScrollingMovementMethod());
		
		super.onCreate(bundle);
	}
}
