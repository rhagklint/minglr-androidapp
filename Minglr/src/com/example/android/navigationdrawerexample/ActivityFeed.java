package com.example.android.navigationdrawerexample;


import android.os.Bundle;
import android.app.Activity;


public class ActivityFeed extends Activity {
	// - Variables
	int _position;
	
	// - Constructors
	public ActivityFeed(){}
	public ActivityFeed(int index){_position = index;}
	// - 
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activity_feed);
		// Show the Up button in the action bar.
		//setupActionBar();
		
		System.out.println("OnCreate: ActivityFeed");
		
		/*
		// - Add fragment listView (feed) to this activity
		Fragment fragment = FragmentStore.getFragmentForRow(_position);
    	FragmentManager fragmentManager = getFragmentManager();
    	fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    	*/
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	/*
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}
	*/

	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_feed, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	*/

}

