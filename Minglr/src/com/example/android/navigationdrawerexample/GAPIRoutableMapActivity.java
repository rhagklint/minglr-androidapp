package com.example.android.navigationdrawerexample;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;


/**
 * This class should be used as an abtract class which can route out
 * polylines between two coordinates
 * @author Robert
 *
 */
public class GAPIRoutableMapActivity extends FragmentActivity {
	
	// - Coordinates for start and finish lat lng
	private LatLng _startCoordinate;
	private LatLng _destCoordinate;

	public GAPIRoutableMapActivity() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedBundleInstance){
		
		super.onCreate(savedBundleInstance);
		setContentView(R.layout.activity_google_map);
		
		_startCoordinate = (LatLng) getIntent().getExtras().get("start");
		_destCoordinate = (LatLng) getIntent().getExtras().get("destination");
		
		System.out.println("Start: " + _startCoordinate.toString());
		System.out.println("Destination: " + _destCoordinate.toString());
		
		MarkerOptions startMarkerOptions = new MarkerOptions().position(_startCoordinate).title("Your position");
		MarkerOptions destMarkerOptions = new MarkerOptions().position(_destCoordinate).title("Destination");
		
		getMap().addMarker(startMarkerOptions);
		getMap().addMarker(destMarkerOptions);
		
		//
		// - Start downloading the directions
		startDownloadingGoogleDirections(_startCoordinate, _destCoordinate);
		
	}
	
	
	/**
	 * Returns the map
	 */
	private GoogleMap getMap(){
		GoogleMap map = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
		return map;
	}
	
	
	/**
	 * Downloads the coordinates from google api
	 * @param start
	 * @param destination
	 */
	private void startDownloadingGoogleDirections(LatLng start, LatLng destination){
		
		// - Create the url
		String url= 
				"http://maps.googleapis.com/maps/api/directions/json?origin=" 
				+ start.latitude + "," + start.longitude +"&destination=" 
				+ destination.latitude + "," + destination.longitude + "&sensor=false";
		
		
	}

}
