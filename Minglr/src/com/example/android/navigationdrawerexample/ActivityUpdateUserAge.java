package com.example.android.navigationdrawerexample;

import java.sql.Date;
import java.util.Calendar;

import android.R.integer;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.google.android.gms.common.api.a.c;

public class ActivityUpdateUserAge extends ActivityUpdateUser {
	
	private DatePicker _datePicker;
	private LinearLayout _datePickerLayout;
	private boolean _datePickerVisible = false;

	public ActivityUpdateUserAge() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		setContentView(R.layout.activity_update_user_age);
		
		// - Layout with textView and button layout
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, R.id.change_layout);
		
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		mainLayout.setBackgroundResource(R.drawable.clean_background);
		
		TextView changeTextView = (TextView)findViewById(R.id.change_text_view);
		changeTextView.setTextColor(Color.WHITE);
		changeTextView.setPadding(25, 0, 0, 0);

		
		// - Assign date picker
		_datePicker = (DatePicker)findViewById(R.id.date_picker);
		_datePickerLayout = (LinearLayout)findViewById(R.id.date_picker_layout);
		_datePickerLayout.setVisibility(View.GONE);
		_datePicker.init(
				_datePicker.getYear(), 
				_datePicker.getMonth(), 
				_datePicker.getDayOfMonth(), 
				new DatePicker.OnDateChangedListener() {
			
			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear,
					int dayOfMonth) {
				
				System.out.println("DateChanged");
				handleOnDateChanged(year, monthOfYear, dayOfMonth);
			}
		});
		
		super.onCreate(savedInstanceState);
	}

	@Override
	void handleUserDownloaded(ApiObject user) {
		
		ApiObject refinedUser = ApiObjectHandler.refineObject(user, "user");
		String birthday = ApiObjectHandler.UserObject.getBirthday(refinedUser);
		
		TextView changeTextView = (TextView)findViewById(R.id.change_text_view);
		
		if(birthday == null){
			changeTextView.setText("");
			return;
		}
		
		if(birthday.equals("null")){
			changeTextView.setText("");
			return;
		}
		
		else changeTextView.setText(birthday);
	}

	@Override
	void handleChangeButtonTapped() {
		System.out.println("ChangeButtontapped");
		
		// - If it is visible already
		if(_datePickerVisible){
			_datePickerLayout.setVisibility(View.GONE);
			_datePickerVisible = false;
		}
		// - If it is hidden
		else{
			_datePickerLayout.setVisibility(View.VISIBLE);
			_datePickerVisible = true;
		}
	}

	@Override
	void handleConfirmButtonTapped() {

		if(_birthdayString == null || 
				_birthdayString.equals("null") || 
				_birthdayString.equals("") ||
				_birthdayString.length() == 0){
			
			return;
		}
		
		ApiPost apiPost = new ApiPost();
		apiPost.addParameter("birthday", _birthdayString);
		apiPost.startPostDataTo("user/update/birthday/" + _birthdayString, new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				System.out.println("New birthday posted! Response: " + response);
			}
		});
		
	}
	
	
	private String _birthdayString;
	private void handleOnDateChanged(int year, int monthOfYear, int dayOfMonth){
		
		// - Assign the textView we want to change
		TextView textView = (TextView)findViewById(R.id.change_text_view);
		
		Date selectedDate = new Date(year - 1900, monthOfYear, dayOfMonth);
		
		textView.setText(selectedDate.toString());
		_birthdayString = selectedDate.toString();
	}
	
	

}
