package com.example.android.navigationdrawerexample;

import java.util.ArrayList;
import java.util.HashMap;

import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.CategoryHandler;
import com.android.minglr.models.ImageDownload;
import com.android.minglr.models.ImageDownloadInterface;
import com.android.minglr.models.MinglrConfig;
import com.android.minglr.models.MinglrConfig.kConfigSetting;
import com.android.minglr.views.ClusterPopup;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;

public class ClusterMapActivity extends FragmentActivity {
	
	//
	// - Instance var of the map
	protected GoogleMap _map;
	protected ArrayList <ApiObject> _locations;
	
	//
	// - This map contains an arraylist of locations (apiobjects) for ever
	// - key: category_id
	protected HashMap <String, ArrayList <ApiObject>> _categorizedLocations;
	
	//
	// - A hashmap with the marker as a key to a location object
	protected HashMap <Marker, ApiObject> _markerLocationMap = new HashMap<Marker, ApiObject>();
	
	//
	// - A HashMap with marker as a key to a user object
	protected HashMap <Marker, ApiObject> _markerUserMap = new HashMap<Marker, ApiObject>();
	
	//
	// - A list of all the users
	protected ArrayList <ApiObject> _userList = new ArrayList <ApiObject>();
	
	//
	// - A HashMap with image addresses for the specific map icon
	// - for a givet category_id
	protected HashMap <String, String> _categoryImageStringFor = new HashMap<String, String>();
	
	//
	// - A HashMap with a drawable for a specific category_id
	protected HashMap <String, Drawable> _categoryPictureFor = new HashMap <String, Drawable>();
	
	//
	// - This hashMap will have a value for every category_id which have had
	// - their category_picture download start
	protected HashMap <String, String> _downloadStartedFor = new HashMap<String, String>();

	public ClusterMapActivity() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		// - Init the _categorizedLocations
		_categorizedLocations = new HashMap<String, ArrayList <ApiObject>>();
		//setUpClusterManager();
	}
	
	
	/**
	 * This function is an offload/helper function for when the locations has been
	 * downloaded to insert the location into the correct array of locations
	 * based on its category
	 * @param location
	 */
	protected void insertLocationInMap(ApiObject location){
		
		String category_id = ApiObjectHandler.CategoryObject.getId(location);
		
		//
		// - This is the array that contains the locations for the specified category_id
		ArrayList <ApiObject> categoryList = _categorizedLocations.get(category_id);
		
		//
		// - If that list is null, we create a new one and insert it to the hashmap
		if(categoryList == null){
			categoryList = new ArrayList <ApiObject> ();
		}
		
		// - Add the locations
		if(!categoryList.contains(location)){
			categoryList.add(location);
		}
		
		// - And put it back in the hash map
		_categorizedLocations.put(category_id, categoryList);
	}
	
	/**
	 * Insert a user api object to the list of available users on the map
	 * @param user
	 */
	protected void insertUserInList(ApiObject user){
		_userList.add(user);
	}
	
	/**
	 * Will take a newly downloaded location and insert into their respective arrays in the map
	 * and also start download for the category picture
	 * @param object is the location we just downloaded
	 */
	protected void handleDownloadedLocation(ApiObject object){
		//
		// - We first need to put the location in the correct array of locations
		// - based on categories
		insertLocationInMap(object);
		
		//
		// - We want to be sure if we should add the location
		// - since we have categories
		String category_id = ApiObjectHandler.CategoryObject.getId(object);
		
		//
		// - We also need to add the category_picture string to the
		// - relevant hashmap with category_id as key
		String category_picture = object.getObject().get("category_picture");
		_categoryImageStringFor.put(category_id, category_picture);
		
		//
		// - Download category picture. After the download is finished, the function
		// - will add their markers to the map
		downloadCategoryPicture(category_picture, category_id);
	}
	
	
	// ================================================================================================ //
	// ********************************** Categorizing and pictures *********************************** //
	// ================================================================================================ // 
	/**
	 * This function downloads the category picture for a given url, 
	 * @param categoryPictureString
	 * @param category_id
	 */
	protected void downloadCategoryPicture(String categoryPictureString, final String category_id){
		
		//
		// - First we check if the download hasn't started already
		if(_downloadStartedFor.get(category_id) != null){
			return;
		}
		else{
			//
			// - If the download hasn't yet started, we add a value for 
			// - the parameter category_id, since we are actually starting the
			// - download now
			_downloadStartedFor.put(category_id, "started");
		}
		
		ImageDownload imageDownload = new ImageDownload();
		imageDownload.downloadImage(categoryPictureString, new ImageDownloadInterface() {
			
			@Override
			public void imageListFinished(ArrayList<Drawable> imageList) {
				
				//
				// - Allocate and assign a drawable
				Drawable categoryPicture = imageList.get(0);
				
				//
				// - And then put it into the hashmap where the categoryPicture
				// - is value for a key; category_id
				_categoryPictureFor.put(category_id, categoryPicture);
				
				//
				// - Show them on the map
				showMarkersForCategory(category_id);
			}
			
		});
	}
	
	/**
	 * Adds markers which has the same category id as the parameter
	 * This function will only add the markers for a category_id which is set as selected
	 * in the CategoryHandler. 'Selected' meaning that they should show 
	 * @param category_id for the markers (locations) which we will add to the map
	 */
	protected void showMarkersForCategory(String category_id){
		
		//
		// - If the category is not selected, we return
		if(!CategoryHandler.isCategorySelected(category_id, this)){
			return;
		}
		
		//
		// - If map is null, we return
		if(_map == null)
			return;
		
		//
		// - Get the locations for the parameter category_id
		ArrayList <ApiObject> locations = _categorizedLocations.get(category_id);
		
		//
		// - Fetch the drawable category picture
		//Drawable categoryPicture = _categoryPictureFor.get(category_id);
		BitmapDrawable categoryPicture = (BitmapDrawable) _categoryPictureFor.get(category_id);
		Bitmap categoryIcon = categoryPicture.getBitmap();
		
		//
		// - Loop thru them
		for(ApiObject object : locations){
			
			//
			// - First we check if the setting showOnlyOpen == true
			if(MinglrConfig.getInstance().isShowOpenOnlyOn()){
				String isOpenString = object.getObject().get("is_open");
				Integer isOpen = Integer.parseInt(isOpenString);
				
				// - If isOpen == 0, we continue in the for loop, and don't add this marker
				if(isOpen == 0)
					continue;
			}
			
			//
			// - Allocate and assign variables
			String name = ApiObjectHandler.UserObject.getName(object);
			LatLng coordinate = ApiObjectHandler.UserObject.getCoordinate(object);
			
			//
			// - Create MarkerOptions
			MarkerOptions markerOptions = new MarkerOptions().position(coordinate).title(name);
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(categoryIcon));
			//markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_base_small));
			
			//
			// - Create a key marker from the markerOptions and also adding it to the map
			//Marker markerKey = _map.addMarker(markerOptions);
			LocationClusterItem clusterItem = new LocationClusterItem(coordinate, object);
			_clusterManager.addItem(clusterItem);
			
			//
			// - We have to add it to a HashMap so we can relate
			// - it later when the user taps the info view
			//_markerLocationMap.put(markerKey, object);
		}
	}
	
	/**
	 * Call this function when we want to add the users to the map.
	 * Users won't be added if showAllOn == false in MinglrConfig
	 */
	protected void showMarkersForUsers(){
		
		//
		// - we also have to check if the "showAll" option
		// - in categoryHandler is set to ON
		// - If showAllOn == false, we have don't want to add any user markers
		if(!MinglrConfig.getInstance().isShowAllOn())
			return;
		
		for(ApiObject object : _userList){
			LatLng coordinate = ApiObjectHandler.UserObject.getCoordinate(object);
			String name = ApiObjectHandler.UserObject.getName(object);
			
			//
			// - Add the marker
			MarkerOptions marker = new MarkerOptions().position(coordinate).title(name);
			
			Marker markerKey = _map.addMarker(marker);
			
			//
			// - We have to add it to a HashMap so we can relate
			// - it later when the user taps the info view
			_markerUserMap.put(markerKey, object);
			
		}
	}
	
	/**
	 * Private class LocationClusterItem is the item which
	 * acts a cluster on the map
	 * @author Robert
	 *
	 */
	private class LocationClusterItem implements ClusterItem{
		
		private final LatLng _position;
		private final ApiObject _apiObject;
		
		public LocationClusterItem(double lat, double lng, ApiObject apiObject){
			_position = new LatLng(lat, lng);
			_apiObject = apiObject;
		}
		
		public LocationClusterItem(LatLng coord, ApiObject apiObject){
			_position = coord;
			_apiObject = apiObject;
		}
		
		@Override 
		public LatLng getPosition(){
			return _position;
		}
		
		public ApiObject getLocationObject(){
			return _apiObject;
		}
	}
	
	//
	// - EXP
	// - Will return an array of locations based on which category_id
	// - is the paramater
	private ArrayList<ApiObject> getLocationsForCategory(String category_id){
		
		ArrayList <ApiObject> locationList = _categorizedLocations.get(category_id);
		return locationList;
		
	}
	
	// ================================================================================================ //
	// ********************************** Categorizing and pictures *********************************** //
	// ================================================================================================ //
	
	
	// ================================================================================================ //
	// **************************************** CLUSTERING ******************************************** //
	// ================================================================================================ //
	/**
	 * Cluster manager for the map
	 */
	protected ClusterManager<LocationClusterItem> _clusterManager;
	
	/**
	 * Simply sets up the cluster manager
	 */
	protected void setUpClusterManager(){
		
		//
		// - initialize the manager witht he context and the map.
		// - (Activity extends context, so we can pass 'this' in the constructor.)
		_clusterManager = new ClusterManager<ClusterMapActivity.LocationClusterItem>(this, _map);
		_clusterManager.setRenderer(new LocationRenderer());
		
		//
		// - Point the map's listeners at the listeners implemented by 
		// - the cluster manager
		_map.setOnCameraChangeListener(_clusterManager);
		
		//
		// - We have to set the onMarkerClickListener to _clusterManager so
		// - that the cluster manager can handle the onClusterClick and
		// - onClusterItemClicked
		_map.setOnMarkerClickListener(_clusterManager);
		
		//
		// - Set on cluster click
		_clusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener
				<ClusterMapActivity.LocationClusterItem>(){

			@Override
			public boolean onClusterClick(Cluster<LocationClusterItem> cluster) {
				
				System.out.println("Cluster size: " + cluster.getItems().size());
				//
				// Create a list of location api objects
				ArrayList <LocationClusterItem> list = (ArrayList<LocationClusterItem>) cluster.getItems();
				ArrayList <ApiObject> locationObjectList = new ArrayList<ApiObject>();
				
				//
				// - Add all the ApiObjects for each cluster item to the arrayList
				for(LocationClusterItem item : list){
					locationObjectList.add(item.getLocationObject());
				}
				
				//
				// - And show popup
				//showClusterPopup(locationObjectList);
				showClusterAlert(locationObjectList);
				
				return false;
			}
		});
		
		//
		// - Set on cluster ITEM click, that means when a single item is clicked
		_clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener
				<ClusterMapActivity.LocationClusterItem>() {

			@Override
			public boolean onClusterItemClick(LocationClusterItem item) {
				System.out.println("Cluster item click");
				return false;
			}
		});
	}
	
	
	private void showClusterPopup(ArrayList <ApiObject> locationList){
		
		//
		// - Declare, allocate and assign a inflater
		LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
		
		//
		// - Parent for the popup
		View parentView = inflater.inflate(R.layout.popup_cluster_list, null);
		parentView.setBackgroundColor(Color.BLACK);
		
		//
		// - Declare the popup
		ClusterPopup popupWindow = new ClusterPopup(
				parentView, 
				LayoutParams.WRAP_CONTENT, 
				LayoutParams.WRAP_CONTENT, 
				true,
				locationList,
				this,
				inflater);
		
		View view = findViewById(android.R.id.content);
		popupWindow.setContentView(parentView);
		popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
	}
	
	private void showClusterAlert(final ArrayList <ApiObject> locationList){
		
		//
		// _ Declare, allocate and assign a new alertDialog
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		
		// - Get inflater
		LayoutInflater inflater = getLayoutInflater();
        
		//
		// - Create the convert view
        View convertView = (View) inflater.inflate(R.layout.popup_cluster_list, null);
        
        // - Assign values to alertDialog
        alertDialog.setView(convertView);
        alertDialog.setTitle("Clustered Locations");
        
        // - Assign listView
        ListView lv = (ListView) convertView.findViewById(R.id.listView1);
        
        
        //
        // - Set on item click listener
        lv.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				
				// - param arg2 is the location of the object
				ApiObject selectedObject = locationList.get(arg2);
				
				//
				// - Create a new intent
				Intent intent = new Intent(getApplicationContext(), ActivityLocationProfile.class);
				intent.putExtra("location_id", ApiObjectHandler.LocationObject.getId(selectedObject));
				startActivity(intent);
			}
        	
		});
        
        // - Adapter
        ArrayAdapterCheckInList adapter = new ArrayAdapterCheckInList(
        		this, 
        		android.R.layout.simple_list_item_1, 
        		locationList);
        
        // - Set adapter for listView
        lv.setAdapter(adapter);
        
        alertDialog.show();
	}
	
	private class LocationRenderer extends DefaultClusterRenderer<LocationClusterItem>{


		public LocationRenderer(){
			super(getApplicationContext(), _map, _clusterManager);
			
			//
			// - Dimensions for the base pin resource
			//_iconBaseDimension = (int) getResources().getDimensionPixelOffset(R.drawable.pin_base_small);
			
			//
			// - Init the _iconBasePin
			//_iconBasePin = new ImageView(getApplicationContext());
			//_iconBasePin.setLayoutParams(new ViewGroup.LayoutParams(_iconBaseDimension, _iconBaseDimension));
			//_iconGenerator.setContentView(_iconBasePin);
			
		}
		
		/**
		 * When a single "marker" is drawn
		 */
		@Override
		protected void onBeforeClusterItemRendered(LocationClusterItem locationItem, MarkerOptions markerOptions){
			
			//
			// - Get the object and it's category_id
			ApiObject locationObject = locationItem.getLocationObject();
			String category_id = locationObject.getObject().get("category_id");
			
			//
			// - Declare and assign bitmap for markerOptions.icon
			Bitmap categoryIcon = createCategoryBitmap(category_id);
			
			// 
			// - Set icon
			markerOptions.icon(BitmapDescriptorFactory.fromBitmap(categoryIcon));
			markerOptions.title(locationItem._apiObject.getObject().get("name"));
			_markerLocationMap.put(markerOptions., locationItem);
		}
		
		private Bitmap createCategoryBitmap(String category_id){
			
			// - The actual base bitmap, the pin which we 'imagine' we 'draw' on
			Bitmap baseBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.pin_base_small);
						
			// - The mutable bitmap which we draw everything on
			Bitmap returnBitmap = Bitmap.createBitmap(
					baseBitmap.getWidth(), 
					baseBitmap.getHeight(), 
					Bitmap.Config.ARGB_8888);
			
			// - The specifc icon for the category_id (OR location_id)
			BitmapDrawable categoryPicture = (BitmapDrawable) _categoryPictureFor.get(category_id);
			Bitmap categoryIcon = categoryPicture.getBitmap();
			
			//
			// - Create a canvas and construct it with the bitmap which we draw onto
			Canvas comboCanvas = new Canvas(returnBitmap);
			
			// - Draw
			comboCanvas.drawBitmap(baseBitmap, 0, 0, null);
			comboCanvas.drawBitmap(
					categoryIcon, 
					(baseBitmap.getWidth() / 2) - (categoryIcon.getWidth() / 2), 
					(baseBitmap.getHeight() / 2) - ((categoryIcon.getHeight() / 2) + 8), 
					null);
			
			// - Save
			comboCanvas.save();
			
			return returnBitmap;
		}
		
	}
	// ================================================================================================ //
	// **************************************** CLUSTERING ******************************************** //
	// ================================================================================================ //	
		
		

}
