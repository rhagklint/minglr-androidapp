package com.example.android.navigationdrawerexample;

import android.app.Activity;
import android.os.Bundle;

public class ActivitySelectFriends extends Activity {

	public ActivitySelectFriends() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(Bundle savedInstance){

		// - Super call
		super.onCreate(savedInstance);
		
		// - Content view
		setContentView(R.layout.activity_select_friends);
		
		/**
		 * OBS. This class uses the Fragment_FriendList as it's front-end fragment.
		 * This is set in the xml file for this class
		 */
		
	}

}
