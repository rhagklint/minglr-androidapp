package com.example.android.navigationdrawerexample;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;

public class ActivityUpdateUserAlias extends ActivityUpdateUser {

	public ActivityUpdateUserAlias() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * We have this so that when we download the user name, we can save it
	 * so that the user don't hit "change name" when the name hasn't been changed 
	 */
	private String _userName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		setContentView(R.layout.activity_update_user_alias);
		
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, R.id.change_layout);
		setDrawableResourceForLayoutId(R.drawable.button_yellow_clean, R.id.change_button);
		
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		mainLayout.setBackgroundResource(R.drawable.clean_background);
		
		EditText changeTextView = (EditText)findViewById(R.id.change_edit_text);
		changeTextView.setTextColor(Color.WHITE);
		changeTextView.setPadding(25, 0, 0, 0);
		changeTextView.clearFocus();
		
		Button changeButton = (Button)findViewById(R.id.change_button);
		changeButton.setTextColor(Color.WHITE);
		changeButton.setText("Change name");
		
		super.onCreate(savedInstanceState);
	}

	@Override
	void handleUserDownloaded(ApiObject user) {
		
		ApiObject refinedUser = ApiObjectHandler.refineObject(user, "user");
		String name = ApiObjectHandler.UserObject.getName(refinedUser);
		
		EditText changeTextView = (EditText)findViewById(R.id.change_edit_text);
		changeTextView.setText(name);
		
		_userName = name;
	}

	@Override
	void handleChangeButtonTapped() {
		System.out.println("Change button tapped");
		EditText changeTextView = (EditText)findViewById(R.id.change_edit_text);
		
		if(changeTextView.getText().length() == 0 ||
		   changeTextView.getText().toString().equals(_userName)){
			System.out.println("This is already your name");
			return;
		}
		
		ApiPost apiPost = new ApiPost();
		apiPost.addParameter("name", changeTextView.getText().toString());
		apiPost.startPostDataTo("user/update", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				System.out.println("Response: " + response);	
			}
		});
		
	}

	@Override
	void handleConfirmButtonTapped() {
		System.out.println("Confirm button tapped");

	}

}
