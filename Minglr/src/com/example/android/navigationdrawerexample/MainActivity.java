/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.navigationdrawerexample;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;


import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.fragments.Fragment_Feed;
import com.android.minglr.fragments.Fragment_ProfileView;
import com.android.minglr.models.ActivityStore;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.MinglrConfig;
import com.android.minglr.models.FileManager;
import com.android.minglr.models.FlashDrive;
import com.android.minglr.models.FragmentStore;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.android.navigationdrawerexample.StatusUpdateActivity;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.model.GraphUser;
/**
 * This example illustrates a common usage of the DrawerLayout widget
 * in the Android support library.
 * <p/>
 * <p>When a navigation (left) drawer is present, the host activity should detect presses of
 * the action bar's Up affordance as a signal to open and close the navigation drawer. The
 * ActionBarDrawerToggle facilitates this behavior.
 * Items within the drawer should fall into one of two categories:</p>
 * <p/>
 * <ul>
 * <li><strong>View switches</strong>. A view switch follows the same basic policies as
 * list or tab navigation in that a view switch does not create navigation history.
 * This pattern should only be used at the root activity of a task, leaving some form
 * of Up navigation active for activities further down the navigation hierarchy.</li>
 * <li><strong>Selective Up</strong>. The drawer allows the user to choose an alternate
 * parent for Up navigation. This allows a user to jump across an app's navigation
 * hierarchy at will. The application should treat this as it treats Up navigation from
 * a different task, replacing the current task stack using TaskStackBuilder or similar.
 * This is the only form of navigation drawer that should be used outside of the root
 * activity of a task.</li>
 * </ul>
 * <p/>
 * <p>Right side drawers should be used for actions, not navigation. This follows the pattern
 * established by the Action Bar that navigation should be to the left and actions to the right.
 * An action should be an operation performed on the current contents of the window,
 * for example enabling or disabling a data overlay on top of the current content.</p>
 */
public class MainActivity extends StatusUpdateActivity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
    
    private boolean _isSignedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	
    	// - Init the flashdrive
    	FlashDrive.initInstance(getApplicationContext());
    	System.out.println("Credentials: " + FlashDrive.getInstance().getCredentials().toString());
    	
        super.onCreate(savedInstanceState);
        
     // ==================================== DEBUG ======================================== //
        String signed_in = FlashDrive.getInstance().getString("fb_signed_in");
        System.out.println("FB_SIGNED_IN: " + signed_in);
        
        String m_signed_in = FlashDrive.getInstance().getString("minglr_signed_in");
        System.out.println("MINGLR_SIGNED_IN: " + m_signed_in);
     // ==================================== DEBUG ======================================== //
        
           
     // ==================================== LOGIN ======================================== //
        //
        // - First we check if we are checked in via facebook
        if(FlashDrive.getInstance().getString("fb_signed_in").equals("yes")){
        	signedInStart(savedInstanceState);
        }
        //
        // - If we are not signed into Facebook, we also check if we are signed in via Minglr directly
        else if(FlashDrive.getInstance().getString("minglr_signed_in").equals("yes")){
        	signedInStart(savedInstanceState);
        }
        //
        // - If neither of these are true, we start the app as unsigned
        else{
        	unsignedInStart(savedInstanceState);
        }
     // ==================================== LOGIN ======================================== //
        
     // - When the comment/nice count label is clicked in the ArrayAdapter
      	// - we want to display a listview
      	LocalBroadcastManager.getInstance(this).registerReceiver(
      			mTappedNiceLabelReceiver, 
      			new IntentFilter("broadcast_nice_list_tapped")
      			);
      	
      	displayNotificationList(false);
     		
    }
    
    @Override
    protected void onResume(){
    	super.onResume();
    	System.out.println("Resume");
    }
    
    /**
     * Will start the app in the usual manner if the user is already signed in
     * @param savedInstanceState the bundle from onCreate(...) method
     */
    private void signedInStart(Bundle savedInstanceState){
    	System.out.println("Signed in start");
    	
    	setContentView(R.layout.activity_main);
    	_isSignedIn = true;

    	requiredStart(savedInstanceState);
        
        this.selectItem(3);
        
        //FlashDrive.getInstance().saveString("685", "KEY_USER_ID");
        
        //
        // - Download user
        ApiDownload userDownload = new ApiDownload();
        userDownload.startDownload("user/get/me", "whatever", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				// TODO Auto-generated method stub
				if(result == null)
					return;
				
				System.out.println("Complete dict: " + result.getObject().toString());
				System.out.println("User: " + result.getObject().get("user").toString());

				try {
					JSONObject jsonObject = new JSONObject(result.getObject().get("user"));
					ApiObject userApiObject = new ApiObject(jsonObject);
					
					FileManager.writeApiObjectToFile(userApiObject, "user_map", getApplicationContext());
					System.out.println("Wrote user to file");
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				// TODO Auto-generated method stub
				
			}
		});
    }
    
    /**
     * Will start the app by showing the sign in screen
     * @param savedInstanceState
     */
    private void unsignedInStart(Bundle savedInstanceState){
    	System.out.println("Unsigned start");
    	// - Set signed in to false
    	_isSignedIn = false;

    	// - Set content view
    	setContentView(R.layout.activity_main);
    	
    	// - Required for start
    	requiredStart(savedInstanceState);
    	
    	// - Declare intent and start it
    	Intent homeIntent = new Intent(this, HomeActivity.class);
    	startActivity(homeIntent);
    }
    
    /**
     * Will init/allocate/assign all the variables we need on the startup
     * @param savedInstanceState bundle
     */
    private void requiredStart(Bundle savedInstanceState){
    	// - drawer title
    	mTitle = mDrawerTitle = getTitle();
        // - mPlanetTitles is now an array of minglr_menu_options
        mPlanetTitles = getResources().getStringArray(R.array.minglr_menu_options);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mPlanetTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(3);
        }
        
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
    	super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    /*
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }
    */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	//
    	// - IF we have clicked the notification button in the top right
    	if(item.getTitle().equals("notifications")){
    		displayNotificationList(true);
    	}
    	
         // The action bar home/up action should open or close the drawer.
         // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        
        //
        // - Open post status update
     // - Send a message to any listenting receiver
		//Intent intent = new Intent("broadcast_status_update");
		
		// - Send
		// - This will be receiver by fragment_feed
		//LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        return true;
        //return false;
        // Handle action buttons
        /*
        switch(item.getItemId()) {
        case R.id.action_websearch:
            // create intent to perform web search for this planet
            Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, getActionBar().getTitle());
            // catch event that there's no activity to handle intent
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
            }
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
        */
    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {

    	if(position == 1){
    		Intent mapIntent = new Intent(this, MapActivity.class);
    		startActivity(mapIntent);
    		
    		return;
    	}
    	
    	// - User pressed logout
    	if(position == 7){
    		Intent logoutIntent = new Intent(this, HomeActivity.class);
    		startActivity(logoutIntent);
    		closeOptionsMenu();
    		return;
    	}
    	//
        // update the main content by replacing fragments
    	Fragment fragment = FragmentStore.getFragmentForRow(position);
    	if(position == 0){
    		Fragment_ProfileView frag = (Fragment_ProfileView) fragment;
    		frag.setUserID("685");
    		// - TODO fix the hardcoded userID
    	}
    	
    	FragmentManager fragmentManager = getFragmentManager();
    	fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
    	
    	mDrawerList.setItemChecked(position, true);
    	setTitle(mPlanetTitles[position]);
    	mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if(_isSignedIn)
        	mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * Fragment that appears in the "content_frame", shows a planet
     */
    public static class PlanetFragment extends Fragment {
        public static final String ARG_PLANET_NUMBER = "planet_number";

        public PlanetFragment() {
            // Empty constructor required for fragment subclasses
        	System.out.println("Constructor PlanetFragment");
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
        	
        	System.out.println("onCreateView PlanetFragment");
            View rootView = inflater.inflate(R.layout.fragment_planet, container, false);
            int i = getArguments().getInt(ARG_PLANET_NUMBER);
            String planet = getResources().getStringArray(R.array.minglr_menu_options)[i];

            int imageId = getResources().getIdentifier(planet.toLowerCase(Locale.getDefault()),
                            "drawable", getActivity().getPackageName());
            ((ImageView) rootView.findViewById(R.id.image)).setImageResource(imageId);
            getActivity().setTitle(planet);
            return rootView;
        }
    }
    
    //
    // - Broadcast listeners and methods which help them
    // - ATM we have the showNiceListPopup function here, which shows a popup listview
    // - with the names of people who like a certain post
    // ======================================================================================================== //
    // ****************************************** BROADCAST LISTENING ***************************************** //
    // ======================================================================================================== //
    
    /**
     * A broadcast receiver which listenes to when the user wants to see the nice list
     * which happens when they press the nice and comment count label in a cell view (received in
     * ArrayAdapterApiObject). This function will acknowledge the intent, and show a popup for that
     * status_id
     */
    private BroadcastReceiver mTappedNiceLabelReceiver = new BroadcastReceiver(){
		
		@Override 
		public void onReceive(Context context, Intent intent){
			
			String status_id = intent.getExtras().getString("status_id");
			showNiceListPopup(status_id);
		}
	};
    
	/**
	 * Downloads a list for nices for a particular status_id and then displays
	 * that information in a dialog, which has a ListView as contentView
	 * @param status_id
	 */
	private void showNiceListPopup(String status_id){

		final Context context = this;
		
		//
		// - init the dialog
		final Dialog dialog = new Dialog(this);
		dialog.setTitle("List of nices");
		dialog.setCancelable(true);
		
		LayoutInflater li = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final ListView view = (ListView) li.inflate(R.layout.nice_list_layout, null, false);
		
		// - Assign a new instance for api download
		ApiDownload apiDownload = new ApiDownload();
		apiDownload.startDownload("feed/status/" + status_id, "nices", false, new ApiDownloadInterface() {
			
			// - NOT USED
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
				// - This arraylist will add all the names from the nice list we received
				ArrayList<String> userNames = new ArrayList<String>();
				
				// - Loop thru the nice list
				for(ApiObject object : result){
					String name = ApiObjectHandler.UserObject.getName(object);
					userNames.add(name);
				}
				
				// - Create an arrayAdapter for the listView
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						context, 
						android.R.layout.simple_list_item_1, 
						userNames);
				
				// - Set adapter for listView
				view.setAdapter(adapter);
				// - Set listView as contentView for dialog
				dialog.setContentView(view);
				// - Show dialog
				dialog.show();
			}
		});
	}
    // ======================================================================================================== //
    // ****************************************** BROADCAST LISTENING ***************************************** //
    // ======================================================================================================== //
	
	// ======================================================================================================== //
    // ****************************************** NOTIFICATION LIST ******************************************* //
    // ======================================================================================================== //
	
	// - Implemented in superclass StatusUpdateActivity
	
	// ======================================================================================================== //
    // ****************************************** NOTIFICATION LIST ******************************************* //
    // ======================================================================================================== //
	
}