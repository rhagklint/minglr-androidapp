package com.example.android.navigationdrawerexample;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.http.message.BasicNameValuePair;

import com.android.minglr.adapters.ArrayAdapterChat;
import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.android.minglr.tasks.SimpleJsonFetcher;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ChatActivity extends Activity implements ApiPostInterface, ApiDownloadInterface{
	
	
	/********* INSTANCE VARIABLES ************/
	//
	// - The list view which we will assign onCreate(...) with findViewById
	ListView _listView;
	
	//
	// - Conversation_id of the currently shown conversation
	String _conversation_id;
	
	//
	// - The list of messages
	ArrayList <ApiObject> _data = new ArrayList <ApiObject> (); 
	/********* INSTANCE VARIABLES ************/

	public ChatActivity() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstance){
		super.onCreate(savedInstance);
		
		//
		// - Set the contentView
		this.setContentView(R.layout.activity_chat);
		
		//
		// - Assign the listView property by findViewById
		// - to make it easily accessible throughout this class
		_listView = (ListView) findViewById(R.id.chat_list);
		
		//
		// - Assign the _conversation_id
		_conversation_id = getIntent().getExtras().getString("id");
		
		/***************** DOWNLOAD MESSAGES **************/
		startDownloadTask(_conversation_id);
		/***************** DOWNLOAD MESSAGES **************/
		
		//
		// - Add listener to the post button
		Button postButton = (Button) findViewById(R.id.postButton);
		postButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText postField = (EditText) findViewById(R.id.postField);
				postChatMessage(postField.getText().toString());
			}
		});
		
		//
		// - Set background resource
		findViewById(R.id.chat_layout).setBackgroundResource(R.drawable.clean_background);
	}
	
	// - This function starts a task to download the messages of this conversation, with conversation_id
	// - the result will be posted to GetDataTask::onPostExecute()
	// - @params 
	// - String conversation_id (the conversation you want to download the messages to)
	private void startDownloadTask(String conversation_id){
		//
		// - Allocate a new task to download the messages for the conversation
		ApiDownload downloadInstance = new ApiDownload();
		downloadInstance.startDownload("conversation/get/" + conversation_id, "messages", new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
				//
				// - Reverse the list (new objects in chat should show at bottom)
				Collections.reverse(result);
				
				_data = result;
				//
				// - update the listview
				updateListView(result);
			}
		});
	}

	//
	// - UpdateListView function which is used within the private class "GetDataTask" to update the listView
	// - with the newly downloaded content
	private void updateListView(ArrayList <ApiObject> result){
		
		//
		// Create a new adapter for the list view
		ArrayAdapterChat adapter = new ArrayAdapterChat(
				getApplicationContext(), 
				android.R.layout.simple_expandable_list_item_1, 
				result);
		
		adapter.setContext(getApplicationContext());
		
		_listView.setAdapter(adapter);
		_listView.setSelection(_listView.getCount() - 1);
	}
	
	private void postChatMessage(String message){
		
		ApiPost postInstance = new ApiPost();
		postInstance.addParameter("message", message);
		postInstance.addParameter("conversation_id", _conversation_id);
		postInstance.startPostDataTo("conversation/send", this);
		
		//
		// - Clean the postField
		EditText postView = (EditText) findViewById(R.id.postField);
		postView.setText("");
		
		// - We DON'T want to hide the keyboard
		ApiObject newObject = new ApiObject();
		newObject.getObject().put("text", message);
		newObject.getObject().put("user_name", "Robert Ribert Hagklint");
		newObject.getObject().put("conversation_id", _conversation_id);
		
		ArrayList <ApiObject> temp = new ArrayList <ApiObject>();
		temp.addAll(_data); // - Add the old list
		temp.add(newObject); // - Add the new message to the bottom of the array
		
		_data = temp;
		
		//updateListView(_data);

		//
		// - Will add the object to the arrayAdapter, notify it that data has changed
		// - and then scroll down to the bottom
		addNewObject(newObject);
	}
	
	/**
	 * Add object to adapter, notify about new data, and then scroll to bottom
	 * @param newObject
	 */
	private void addNewObject(ApiObject newObject){
		ArrayAdapterChat thisAdapter = (ArrayAdapterChat)_listView.getAdapter();
		thisAdapter.add(newObject);
		thisAdapter.notifyDataSetChanged();
		_listView.setSelection(_listView.getCount() - 1);
	}

	//
	// - DOCS
	// - Api post interface call
	@Override
	public void postDataComplete(String response) {
		// TODO Auto-generated method stub
		
	}

	//
	// - DOCS
	// - Api download interface call
	@Override
	public void arrayFetchComplete(ArrayList<ApiObject> result,
			String variableName) {
		// TODO Auto-generated method stub
		_data = result;
		updateListView(result);
		
		for(ApiObject object : result){
			System.out.println("Chat apiObject: " + object.getObject().toString());
		}
		
	}

	@Override
	public void objectFetchComplete(ApiObject result, String variableName) {
		// TODO Auto-generated method stub
		
	}
	
}
