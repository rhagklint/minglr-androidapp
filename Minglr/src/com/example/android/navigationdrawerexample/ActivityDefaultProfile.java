package com.example.android.navigationdrawerexample;

import com.android.minglr.fragments.Fragment_ProfileView;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class ActivityDefaultProfile extends Activity {

	public ActivityDefaultProfile() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		//
		// - This creates a frame layout as the new contentView
		setContentView(R.layout.activity_profile_default);
		
		// - Create the fragment
		Fragment_ProfileView profileFragment = (Fragment_ProfileView)getFragmentManager().findFragmentById(R.id.profile_fragment);
		String user_id = getIntent().getExtras().getString("user_id");
		profileFragment.setUserID(user_id);
		
		/*
		Fragment_ProfileView profileFragment = new Fragment_ProfileView();
		
		// - Set user id for the fragment to download correct user
		String user_id = getIntent().getExtras().getString("user_id");
		profileFragment.setUserID(user_id);
		*/
		
		// - Inflate
		
		/*
		LinearLayout fragContainerLayout = (LinearLayout) findViewById(R.id.profile_fragment_container);
		
		LinearLayout linearLayout = new LinearLayout(this);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		
		linearLayout.setId(12345);
		
		// - Now if the savedInstanceState == null
		if(savedInstanceState == null){
			
		}
		*/
	}

}
