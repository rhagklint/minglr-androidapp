package com.example.android.navigationdrawerexample;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.minglr.fragments.Fragment_LocationFeed;
import com.android.minglr.fragments.Fragment_LocationVisitors;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.FragmentStore;
import com.android.minglr.models.ImageDownload;
import com.android.minglr.models.ImageDownloadInterface;
import com.android.minglr.models.LocationCheckInHandler;
import com.facebook.widget.LoginButton;
import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ActivityLocationProfile extends Activity {

	private String _locationID;
	
	//
	// - We need coordinates of this locaiton if the user
	// - wants to find the route to this location
	private LatLng _locationCoordinate;
	
	public ActivityLocationProfile() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		//
		// _ Set content view
		setContentView(R.layout.activity_location_profile);
		
		// - Get the location id
		_locationID = getIntent().getExtras().getString("location_id");
		
		LocationCheckInHandler.getInstance().downloadCheckInLocation(null);
		
		//
		// - Start the download of the location
		ApiDownload apiDownload = new ApiDownload();
		String url =  "location/get/" + _locationID + "?include_feed=0&limit=5";
		apiDownload.startDownload(url, "whatever", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				
				//
				// - Get the refined location object
				ApiObject locationObject = refineApiObject(result, "location");
				handleLocationDownloaded(locationObject);
				
				System.out.println("Location object: " + locationObject.getObject().toString());
				
				
				//
				// - Set text and other variables for the
				// - textViews in the scrollView
				// ==========================================================================================
				
				// - NOTE
				// - textView3 = information
				// - textView2 = opentimes text
				// - textView1 = offers/chalkboard
				
				String offers = locationObject.getObject().get("offers");
				String information = locationObject.getObject().get("information");
				String openText = locationObject.getObject().get("open_text");
				
				setTextForResourceId(information, R.id.textView3);
				setTextForResourceId(openText, R.id.textView2);
				setTextForResourceId(offers, R.id.text_view);
				
				setDrawableResourceForLayoutId(R.drawable.info_box, R.id.textView3);
				setDrawableResourceForLayoutId(R.drawable.info_box, R.id.textView2);
				setDrawableResourceForLayoutId(R.drawable.info_box, R.id.text_view);
				
				TextView tv3 = (TextView)findViewById(R.id.textView3);
				tv3.setTextColor(Color.WHITE);
				
				TextView tv2 = (TextView)findViewById(R.id.textView2);
				tv2.setTextColor(Color.WHITE);
				
				TextView tv1 = (TextView)findViewById(R.id.text_view);
				tv1.setTextColor(Color.WHITE);
				
				// ==========================================================================================
				
				//
				// - Set the profile picture for the location
				// ==========================================================================================
				
				String profileImageURLString = ApiObjectHandler.LocationObject.getProfilePicture(locationObject, "original");
				downloadAndSetProfileImage(profileImageURLString);
				
				// ==========================================================================================
				
				//
				// - Coordinate
				// ==========================================================================================
				
				String latitude = locationObject.getObject().get("latitude");
				String longitude = locationObject.getObject().get("longitude");
				
				_locationCoordinate = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
				
				// ==========================================================================================
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result, String variableName) {
			}
		});
		
		//
		// - Fetch the "tab" button
		Button infoButton = (Button)findViewById(R.id.tab_info);
		Button feedButton = (Button)findViewById(R.id.tab_feed);
		Button visitorsButton = (Button)findViewById(R.id.tab_visitors);
		
		//
		// - Add the listeners
		infoButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tappedInfoTabButton();
			}
		});
		feedButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tappedFeedTabButton();
			}
		});
		visitorsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				tappedVisitorsTabButton();
			}
		});
		
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		mainLayout.setBackgroundResource(R.drawable.clean_background);
	}
	
	/**
	 * Will update all the textviews and imageviews and other content in the main
	 * profile view
	 * @param locationObject contains all the content
	 */
	private void handleLocationDownloaded(ApiObject locationObject){
		
		String locationName = ApiObjectHandler.LocationObject.getName(locationObject);
		setTextForResourceId(locationName, R.id.name_text_view);
		
		setTextForResourceId("Find route", R.id.button_find_route);
		setTextForResourceId("Check-in", R.id.button_check_in);
		
		String profilePictureString = ApiObjectHandler.Images.getProfileOriginalImageString(locationObject);
		System.out.println("Profile picture string: " + profilePictureString);
		
		String openString = locationObject.getObject().get("open_text");
		System.out.println("Open string: " + openString);
		
		//
		// - Assign pointers
		Button routeButton = (Button)findViewById(R.id.button_find_route);
		Button checkButton = (Button)findViewById(R.id.button_check_in);
		
		//
		// - Set on click listeners 
		routeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clickedFindRouteButton();
			}
		});
		checkButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clickedCheckInButton();
			}
		});
		
		if(LocationCheckInHandler.getInstance().isCheckedInto(_locationID)){
			checkButton.setText("Check out");
		}
	}
	
	/**
	 * This function will take an ApiObject and return another ApiObject from inside the original 
	 * based on the variable name
	 * @param rawObject the full object which contains other objects in it
	 * @param variableName the variable name of the value you want returned
	 * @return a new ApiObject
	 */
	private ApiObject refineApiObject(ApiObject rawObject, String variableName){
		
		String jsonString = rawObject.getObject().get(variableName);
		ApiObject refinedObject = null;
		
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			refinedObject = new ApiObject(jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return refinedObject;
	}

	/**
	 * Simple help function to set text for a resource id
	 * @param text
	 * @param rid
	 */
	private void setTextForResourceId(String text, int rid){
		TextView textView = (TextView) findViewById(rid);
		textView.setText(text);
	}
	
	/**
	 * Simple help function to set drawable backround resource for layout/view id
	 * @param drawableResource
	 * @param layoutId
	 */
	private void setDrawableResourceForLayoutId(int drawableResource, int layoutId){
		View view = (View)findViewById(layoutId);
		view.setBackgroundResource(drawableResource);
		
		BitmapDrawable background = (BitmapDrawable)view.getBackground(); // assuming you have bg_tile as background.
		BitmapDrawable newBackground = new BitmapDrawable(background.getBitmap()) {
		    @Override
		    public int getMinimumWidth() {
		        return 0;
		    }

		    @Override
		    public int getMinimumHeight() {
		        return 0;
		    }
		};
		newBackground.setTileModeXY(background.getTileModeX(), background.getTileModeY());
		view.setBackgroundDrawable(newBackground);
	}
	
	//
	// - Listeners for the tab buttons
	// ================================================================================================ //
	// *************************************** BUTTON LISTENERS *************************************** //
	// ================================================================================================ //
	
	/**
	 * Tapped the info tab button (first button)
	 */
	private void tappedInfoTabButton(){
		System.out.println("Info");
		defaultToActivity();
	}
	
	/**
	 * Tapped the feed tab button (middle button)
	 */
	private void tappedFeedTabButton(){
		System.out.println("Feed");
		showFeedFragment();
	}
	
	/**
	 * Tapped the visitors tab button (right most button)
	 */
	private void tappedVisitorsTabButton(){
		System.out.println("Visitors");
		showVisitorsFragment();
	}
	
	// ================================================================================================ //
	// *************************************** BUTTON LISTENERS *************************************** //
	// ================================================================================================ //
	
	
	//
	// - Family of functions which handle the fragment handling when the
	// - user taps any of the "tab" buttons
	// ================================================================================================ //
	// *************************************** HANDLE FRAGMENTS *************************************** //
	// ================================================================================================ //
	
	/**
	 * Properties for the fragments Fragment_Feed and Fragment_Visitors
	 * These are needed so we can identify if they are allocated/assigned or not
	 * and so that we can remove them
	 */
	private Fragment_LocationFeed _feedFragment;
	private Fragment_LocationVisitors _visitorsFragment;
	
	/**
	 * This function defaults removes all views/fragments covering the main acitivty
	 * at the moment, and shows the default "info view"
	 */
	private void defaultToActivity(){
		
		// - Assign a fragment manager
		FragmentManager fm = getFragmentManager();
		
		//
		// - Identify if there is a Fragment showing
		if(_feedFragment != null){
			fm.beginTransaction().remove(_feedFragment).commit();
			_feedFragment = null;
		}
		else if(_visitorsFragment != null){
			fm.beginTransaction().remove(_visitorsFragment).commit();
			_visitorsFragment = null;
		}
		
		//
		// - Set the visibility of the top layout to visible
		LinearLayout topLayout = (LinearLayout)findViewById(R.id.top_layout);
		topLayout.setVisibility(View.VISIBLE);
		
		LinearLayout scrollLayout = (LinearLayout)findViewById(R.id.scroll_layout);
		scrollLayout.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Opens and inflates the feed fragment for this location
	 */
	private void showFeedFragment(){
		
		// - If this is already the visible fragment
		// - we have no interest continuing
		if(_feedFragment != null)
			return;
		
		//
		// - First we remove any added fragment
		defaultToActivity();
		
		// 
		// - Assign the fragment and set location_id
		_feedFragment = new Fragment_LocationFeed();
		_feedFragment.setLocationID(_locationID);
		
		//
		// - Assign a new fragment manager 
		FragmentManager fragmentManager = getFragmentManager();
		
		// - Hide the top layout and remove its borders
		// - for drawing purposes 
		// - Without this code the top_layout will still
		// - be visible and "push down" the feed fragment
		LinearLayout topLayout = (LinearLayout)findViewById(R.id.top_layout);
		topLayout.setVisibility(View.GONE);
		
		LinearLayout scrollLayout = (LinearLayout)findViewById(R.id.scroll_layout);
		scrollLayout.setVisibility(View.GONE);
		
		// - And perform the transaction
		fragmentManager.beginTransaction().replace(R.id.main_content_layout, _feedFragment).commit();
	}
	
	/**
	 * Opens and inflates the visitors fragment for this location
	 */
	private void showVisitorsFragment(){
		
		//
		// - If this is already the visible fragment
		// - We have no interest continuing
		if(_visitorsFragment != null)
			return;
		
		//
		// - Remove any added fragemnt
		defaultToActivity();
		
		//
		// - Assign the fragment and set location_id
		_visitorsFragment = new Fragment_LocationVisitors();
		_visitorsFragment.setLocationID(_locationID);
		
		//
		//- Assign a new fragment manager
		FragmentManager fragmentManager = getFragmentManager();
		
		// - Hide the top layout and remove its borders
		// - for drawing purposes 
		// - Without this code the top_layout will still
		// - be visible and "push down" the feed fragment
		LinearLayout topLayout = (LinearLayout)findViewById(R.id.top_layout);
		topLayout.setVisibility(View.GONE);
		
		LinearLayout scrollLayout = (LinearLayout) findViewById(R.id.scroll_layout);
		scrollLayout.setVisibility(View.GONE);
		
		// - And perform the transaction
		fragmentManager.beginTransaction().replace(R.id.main_content_layout, _visitorsFragment).commit();
	}
	
	
	// ================================================================================================ //
	// *************************************** HANDLE FRAGMENTS *************************************** //
	// ================================================================================================ //
	
	//
	// - Functions for handling and setting the text/images in the scroll view
	// ================================================================================================ //
	// ************************************ SCROLL VIEW HANDLING ************************************** //
	// ================================================================================================ //
	

	
	// ================================================================================================ //
	// ************************************ SCROLL VIEW HANDLING ************************************** //
	// ================================================================================================ //
	
	
	// 
	// - Functions dedicated to download api data or other stuff (such as images)
	// - to be presented
	// ================================================================================================ //
	// ****************************************** DOWNLOADS ******************************************* //
	// ================================================================================================ //
	
	/**
	 * Download and set profile image for the location
	 * with url
	 * @param url
	 */
	private void downloadAndSetProfileImage(String url){
		
		// - Assign and start the download
		ImageDownload imageDownload = new ImageDownload();
		imageDownload.downloadImage(url, new ImageDownloadInterface() {
			
			@Override
			public void imageListFinished(ArrayList<Drawable> imageList) {
				
				// - If we have no size of the arrayList we return
				if(imageList.size() < 0)
					return;
				
				// - Else we assign the (first) image
				Drawable image = imageList.get(0);
				
				// - Assign a pointer to the ImageView
				ImageView profileView = (ImageView) findViewById(R.id.location_profile_image_view);
				
				// - And set the image
				profileView.setImageDrawable(image);
				
			}
		});
	}
	
	// ================================================================================================ //
	// ****************************************** DOWNLOADS ******************************************* //
	// ================================================================================================ //
	
	// ================================================================================================ //
	// *************************************** CLICK EVENTS ******************************************* //
	// ================================================================================================ //
	
	private void clickedFindRouteButton(){
		
		//
		// - Create a new activity
		Intent routeIntent = new Intent(this, GAPIRoutableMapActivity.class);
		
		// - Put the locations coordinate as the destination of the routable map activity
		if(_locationCoordinate != null)
			routeIntent.putExtra("destination", _locationCoordinate);
		
		// - Users coordinate
		LatLng startCoordinate = new LatLng(59.337135, 18.062725);
		routeIntent.putExtra("start", startCoordinate);
		
		startActivity(routeIntent);
	}
	
	private void clickedCheckInButton(){
		
		LocationCheckInHandler.getInstance().postCheckinToLocationId(_locationID, null);
		
		Button checkinButton = (Button)findViewById(R.id.button_check_in);
		checkinButton.setText("Check out");
		
	}
	
	// ================================================================================================ //
	// *************************************** CLICK EVENTS ******************************************* //
	// ================================================================================================ //
}
