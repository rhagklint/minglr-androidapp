package com.example.android.navigationdrawerexample;

import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Set;

import com.android.minglr.fragments.Fragment_MapSearch;
import com.android.minglr.fragments.Fragment_MapSettings;
import com.android.minglr.fragments.Fragment_NearbyLocations;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.MinglrConfig;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;


public class MapActivity extends ClusterMapActivity implements ApiDownloadInterface {
	
	
	

	public MapActivity() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_google_map);
		_map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		setUpClusterManager();
		
		//
		// - Create an instance of the ApiDownload class
		// - and start the download with the get url, array variable (variable which contains the 
		// - data array we want
		// - and lastly we also send "this" as param, because this is the instance which should
		// - be calling arrayFetchComplete(...) from the ApiDownload class
		ApiDownload userDownloader= new ApiDownload();
		userDownloader.startDownload(
				"http://api-test.minglr.se/user/friends?include_gender=1&include_location=1", 
				"friends",
				this);
		
		//
		// - Also download locations
		ApiDownload locationDownloader = new ApiDownload();
		locationDownloader.startDownload(
				"http://api-test.minglr.se/location/list?category_id=", 
				"locations", 
				this);
		
		//
		// - Set map button graphics
		// R.id.statusButton // -  Zooms to user
		// R.id.button_check_in // - Search the map content
		// R.id.button_change_mail // - Show close by places
		// R.id.button_change_age // - Settings/filer
		setDrawableResourceForLayoutId(R.drawable.mb_coord_inactive, R.id.statusButton);
		setDrawableResourceForLayoutId(R.drawable.mb_search_inactive, R.id.button_check_in);
		setDrawableResourceForLayoutId(R.drawable.mb_list_inactive, R.id.button_change_mail);
		setDrawableResourceForLayoutId(R.drawable.mb_filter_inactive, R.id.button_change_age);
		
		//
		// - Download the map settings so we have them when the settings view 
		// - is opened
		MinglrConfig.getInstance().downloadMapSettings();

		
	}
	
	@Override 
	protected void onStart(){
		super.onStart();
		
		//
		// - EXP
		// - Add listeners for the buttons
		ImageButton button1 = (ImageButton) findViewById(R.id.statusButton);
		ImageButton button2 = (ImageButton) findViewById(R.id.button_check_in);
		ImageButton button3 = (ImageButton) findViewById(R.id.button_change_mail);
		ImageButton button4 = (ImageButton) findViewById(R.id.button_change_age);
		
		button1.setOnClickListener(new MapBarButtonClickListener());
		button2.setOnClickListener(new MapBarButtonClickListener());
		button3.setOnClickListener(new MapBarButtonClickListener());
		button4.setOnClickListener(new MapBarButtonClickListener());
		
		//
		// - EXP
		// - start locating user
		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		
		// - TODO - param LocationManager.NETWORK_PROVIDER does not work with emulator
		// - but will be best practice for release
		// - for the time being we will go with GPS_PROVIDER
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, new _LocationListener());
		
		//
		// - Set onInfoWindowClickListener
		// - FOR WHAT? WHAT DOES THIS DO?
		_map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
			
			@Override
			public void onInfoWindowClick(Marker arg0) {
				System.out.println("OnInfoWindowClick: " + arg0.getClass());
				//
				// - First we check if this is a location that has been clicked, by looking
				// - if the marker (as key) has a value in the _markerLocationMap
				ApiObject object = _markerLocationMap.get(arg0);
				if(object != null){
					//
					// - If it exists, we start the locationProfileIntent
					startLocationProfileIntent(object);
				}
				else{
					//
					// - Else this is most likley a user object
					object = _markerUserMap.get(arg0);
					startUserProfileIntent(object);
				}
			}
		});
		/*
		_map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker arg0) {
				// TODO Auto-generated method stub
				System.out.println("Marker click: " + arg0.getClass().toString());
				return false;
			}
		});
		*/
		
	}
	
	@Override
	protected void onResume(){
		System.out.println("On resume");
		super.onResume();
	}
	
	@Override
	protected void onPause(){
		System.out.println("On pause");
		//((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap().clear();
		super.onPause();
	}
	
	@Override
	public void onLowMemory(){
		System.out.println("On low memory");
		super.onLowMemory();
	}
	
	
	
	@Override
	public void arrayFetchComplete(ArrayList<ApiObject> result, String variableName) {
		// TODO Auto-generated method stub
		
		//GoogleMap map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		
		for(ApiObject object : result){
			
			// - If variable name is locations
			if(variableName == "locations"){
				//
				// - Handle downloaded location will insert the location object into a map, which contains
				// - an arraylist for every individual and unique category_id
				// - it will also start the download for the category_picture, and once that is done
				// - the marker (if category_id is "selected") will be added to the map
				handleDownloadedLocation(object);
			}
			
			// - Get the coordinate of the user
			LatLng coordinate = ApiObjectHandler.UserObject.getCoordinate(object);

			// - If there is no coordinate we take another lap in the loop
			if(coordinate == null)
				continue;
			
			//
			// - If map is not null we add it (may be so if we remove this intent/activity 
			// - before the download is complete)
			if(_map != null){
				
				//
				// - But we only add it the the map if this is a "locations" variable
				// - fetch
				if(variableName != "locations"){	
					insertUserInList(object);
					showMarkersForUsers();
				}
			}
		}
		
		if(variableName == "locations"){
			//
			// - This is now the _locationList
			_locations = new ArrayList<ApiObject>(result);
			
			//
			// - IF the searchFragment is in memory and on show
			// - we need to update it
			if(_searchFragment != null){
				if(!_searchFragment.isLocationListPopulated()){
					_searchFragment.setLocationList(_locations);
				}
			}
		}
	}

	@Override
	public void objectFetchComplete(ApiObject result, String variableName) {
		// TODO Auto-generated method stub
		// unused
	}
	
	//
	// *************** BUTTON ON CLICK LISTENERS ************* //
	// - EXP
	// - This class handles the click of the buttons on the toolbar
	// - they will then call the appopriate function in 
	// - ACTIONS FOR TOOLBAR BUTTON CLICKS category located directly below
	private class MapBarButtonClickListener implements OnClickListener{

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			// - Cast
			ImageButton clickedButton = (ImageButton) v;
			
			// - Tag
			int clickedButtonTag = getButtonTag(clickedButton);
			
			// - Switch the tag
			switch (clickedButtonTag) {
			
			case 1:
				handleButton1Clicked();
				break;
			case 2:
				handleButton2Clicked();
				break;
			case 3: 
				handleButton3Clicked();
				break;
			case 4:
				handleButton4Clicked();
				break;

			default:
				break;
			}
		}
		
		//
		// - Returns which of the four buttons was pressed
		private int getButtonTag(ImageButton button){
			
			switch (button.getId()) {
			case R.id.statusButton:
				return 1;
			case R.id.button_check_in:
				return 2;
			case R.id.button_change_mail:
				return 3;
			case R.id.button_change_age:
				return 4;
				
			default:
				break;
			}
			
			
			return 0;
		}
		
	}
	
	// *************** BUTTON ON CLICK LISTENERS ************* //
	//
	
	//
	//  *************** ACTIONS FOR TOOLBAR BUTTON CLICKS ************ //
	// - EXP 
	// - this set of functions will only open/add/replace views/fragments/layouts
	// - to the activity based on the button press on the map toolbar
	// - no explanation of the function names needed... ;)
	
	// - DOCS
	// - Zoom to user location
	private void handleButton1Clicked(){
		
		//
		// - If map is null we return
		if(_map == null)
			return;
		
		//
		// - TODO 
		// - Get the actual location of the user, but the emulator can't/won't handle
		// - location updates atm, so we have to make due with fixed coords for user location
		LatLng _userCoordinate = new LatLng(59.337135, 18.062725);
		
		// - TODO
		// - Zoom map to user location
		_map.animateCamera(CameraUpdateFactory.newLatLngZoom(_userCoordinate, 16));
	}
	
	// - DOCS
	// - Search the map
	// - variables which we need
	private Fragment_MapSearch _searchFragment;
	private void handleButton2Clicked(){
		
		//
		// - Replace fragment with Fragment list
		if(_searchFragment == null){
			
			//
			// - Layout of the activity, which contains the fragment
			//LinearLayout layout = (LinearLayout) findViewById(R.id.map_layout);
			
			// - Create the new fragment
			_searchFragment = new Fragment_MapSearch();
			if(_locations != null){
				_searchFragment.setLocationList(_locations);
			}
			
			// - Begin transaction
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.replace(R.id.map, _searchFragment);
			ft.commit();
			
		}
		//
		// - remove _searchFragment
		else{
			getFragmentManager().beginTransaction().remove(_searchFragment).commit();
			_searchFragment = null;
		}
		
	}
	
	// - DOCS
	// - List of locations close by
	// - And variables we need 
	private Fragment_NearbyLocations _locationsFragment;
	private void handleButton3Clicked(){
		
		//
		// Replace current fragment with _locationsFragment
		if(_locationsFragment == null){
			
			// - Create the new fragment
			_locationsFragment = new Fragment_NearbyLocations();
			
			// - Begin transaction
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.replace(R.id.map, _locationsFragment);
			ft.commit();
		}
		//
		// - Remove the _locationsFragment
		else{
			getFragmentManager().beginTransaction().remove(_locationsFragment).commit();
			_locationsFragment = null;
		}
	}
	
	// - DOCS
	// - Map settings
	private Fragment_MapSettings _mapSettingsFragment;
	private void handleButton4Clicked(){
		
		//
		// - Replace current fragment with this fragment
		if(_mapSettingsFragment == null){
			
			// - Create
			_mapSettingsFragment = new Fragment_MapSettings();
			
			// - Begin transaction
			FragmentTransaction ft = getFragmentManager().beginTransaction();
			ft.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.replace(R.id.map, _mapSettingsFragment);
			ft.commit();
			mapDidDisappear();
		}
		else{
			getFragmentManager().beginTransaction().remove(_mapSettingsFragment).commit();
			_mapSettingsFragment = null;
			mapDidReappear(); // - Only do map did reappear and disappear for the 
			// - map settings button. Because they are only relevant if we have changed settings
		}
	}
	
	/**
	 * Call this everytime this activity removes a fragment
	 * Place logic which whould be performed on reappearance
	 */
	private void mapDidReappear(){
		
		//
		// - All markers for "selected" categories
		// - showMarkersForCategory will filter out and won't add markers
		// - for categories which are not set as selected
		System.out.println("Map did reappear");
		showMarkersForAllCategories();
		showMarkersForUsers();
	}
	
	/**
	 * Call this everytime the activity adds a fragment ontop
	 * of this map
	 */
	private void mapDidDisappear(){
		//
		// - Remove all items from the map
		System.out.println("Map did disappear");
		_map.clear();
		_clusterManager.clearItems();
	}
	
	/**
	 * Helper function to show all the markers for each individual category_id
	 * Will only display marker for a particular category if they are "selected" in the 
	 * CategoryHandler
	 */
	private void showMarkersForAllCategories(){
		Set <String> keySet = _categorizedLocations.keySet();
		for(String category_id: keySet){
			showMarkersForCategory(category_id);
		}
	}
	
	//
	// - DOCS
	// - Zoom map to coordinate with zoomLevel
	public void zoomMapToCoordinate(LatLng coordinate, int zoomLevel){
		_map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinate, zoomLevel));
	}
	//
	// - DOCS
	// - remove the Search fragment
	public void removeSearchFragment(){
		getFragmentManager().beginTransaction().remove(_searchFragment).commit();
		_searchFragment = null;
	}
	
	//  *************** ACTIONS FOR TOOLBAR BUTTON CLICKS ************ //
	//
	
	//
	// ****************** LOCATION LISTENER ****************//
	// - EXP
	// - Class which implements LocationListener and all its function inside of it
	// - pretty clearcut what this is
	private class _LocationListener implements android.location.LocationListener{

		@Override
		public void onLocationChanged(Location arg0) {
			// TODO Auto-generated method stub
			System.out.println("onLocationChanged");
			
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub
			
		}
		
	}
	// ****************** LOCATION LISTENER ****************//
	// 
	
	/**
	 * Will start an intent to open the location profile
	 * @param locationObject is the apiObject which we use to 
	 * start the intent
	 */
	private void startLocationProfileIntent(ApiObject locationObject){
		System.out.println("StartLocationProfileIntent");
		
		//
		// - location_id
		String location_id = ApiObjectHandler.LocationObject.getId(locationObject);
		
		//
		// - Create a new intent
		Intent locationIntent = new Intent(this, ActivityLocationProfile.class);
		locationIntent.putExtra("location_id", location_id);
		
		//
		// - Start the activity
		startActivity(locationIntent);
		
	}
	
	/**
	 * Will start an intent to open the user profile
	 * @param userObject is the apiObject which we use to
	 * start the intent
	 */
	private void startUserProfileIntent(ApiObject userObject){
		System.out.println("StartUserProfileIntent");
		//
		// - user_id
		String user_id = ApiObjectHandler.UserObject.getId(userObject);
		
		//
		// - Create a new intent
		Intent profileIntent = new Intent(this, ActivityDefaultProfile.class);
		profileIntent.putExtra("user_id", user_id);

		//
		// - Start the new activity
		startActivity(profileIntent);
	}

	
	/**
	 * Simple help function to set drawable backround resource for layout/view id
	 * @param drawableResource
	 * @param layoutId
	 */
	protected void setDrawableResourceForLayoutId(int drawableResource, int layoutId){
		View view = findViewById(layoutId);
		view.setBackgroundResource(drawableResource);
		
		BitmapDrawable background = (BitmapDrawable)view.getBackground(); // assuming you have bg_tile as background.
		BitmapDrawable newBackground = new BitmapDrawable(background.getBitmap()) {
		    @Override
		    public int getMinimumWidth() {
		        return 0;
		    }

		    @Override
		    public int getMinimumHeight() {
		        return 120;
		    }
		};
		newBackground.setTileModeXY(background.getTileModeX(), background.getTileModeY());
		view.setBackgroundDrawable(newBackground);
	}
}
