package com.example.android.navigationdrawerexample;

import com.android.minglr.models.LoginManager;

import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class ActivityCreateUser extends Activity {

	public ActivityCreateUser() {
		// TODO Auto-generated constructor stub
	}
	
	private EditText mailField;
	private EditText passField;
	private CheckBox boxMale;
	private CheckBox boxFemale;
	private CheckBox boxUnknown;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_user);
		
		// - Set background
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		mainLayout.setBackgroundResource(R.drawable.blurred_bg);
		
		// - Login box
		LinearLayout loginLayout = (LinearLayout)findViewById(R.id.login_layout);
		loginLayout.setBackgroundResource(R.drawable.login_box);
		
		mailField = (EditText)findViewById(R.id.mail_field);
		passField = (EditText)findViewById(R.id.pass_field);
		
		mailField.setTextColor(Color.WHITE);
		passField.setTextColor(Color.WHITE);
		
		mailField.setHint("E-mail");
		passField.setHint("Password");
		
		Button signupButton = (Button)findViewById(R.id.signup_button);
		signupButton.setTextColor(Color.WHITE);
		signupButton.setText("Done");
		signupButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				pressedDoneButton();
			}
		});
		
		System.out.println("OnCreate ActivityCreateUser");
		
		setupCheckBoxes();
	}
	
	private void setupCheckBoxes(){
		boxMale = (CheckBox)findViewById(R.id.box_male);
		boxFemale = (CheckBox)findViewById(R.id.box_female);
		boxUnknown = (CheckBox)findViewById(R.id.box_unknown);
		
		boxMale.setText("Man");
		boxFemale.setText("Woman");
		boxUnknown.setText("Blank");
		
		boxMale.setOnCheckedChangeListener(mCheckBoxListener);
		boxFemale.setOnCheckedChangeListener(mCheckBoxListener);
		boxUnknown.setOnCheckedChangeListener(mCheckBoxListener);
		
	}
	
	private CompoundButton.OnCheckedChangeListener mCheckBoxListener = new CompoundButton.OnCheckedChangeListener(){

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			
			if(buttonView.equals(boxMale) && isChecked){
				boxFemale.setChecked(false);
				boxUnknown.setChecked(false);
			}
			else if(buttonView.equals(boxFemale) && isChecked){
				boxMale.setChecked(false);
				boxUnknown.setChecked(false);
			}
			else if(buttonView.equals(boxUnknown) && isChecked){
				boxMale.setChecked(false);
				boxFemale.setChecked(false);
			}
		}
	};
	
	/**
	 * Simple function with returns true if ANY of the checkboxes are checked
	 * @return
	 */
	private boolean isCheckboxActive(){
		if(boxMale.isChecked() || boxFemale.isChecked() || boxUnknown.isChecked())
			return true;
		else return false;
	}
	
	private String getGenderString(){
		if(boxMale.isChecked()){
			return "m";
		}
		else if(boxFemale.isChecked()){
			return "f";
		}
		else return "";
	}
	
	private String getMailString(){
		return mailField.getText().toString();
	}
	
	private String getPasswordString(){
		return passField.getText().toString();
	}
	
	/**
	 * Simple function which returns true if the mail and passwords are filled
	 * with strings which has length
	 * @return
	 */
	private boolean isFormLegal(){
		if(mailField.length() > 0 && passField.length() > 0)
			return true;
		else return false;
	}

	
	private void pressedDoneButton(){
		if(!isCheckboxActive()){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			
			builder.setMessage("You have not set any gender").
			setPositiveButton("Ok", null).
			show();
			
			return;
		}
		
		else if(!isFormLegal()){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			
			builder.setMessage("You have not filled out mail and/or password").
			setPositiveButton("Ok", null).
			show();
			
			return;
		}
		
		//
		// - New user variables
		final String gender = getGenderString();
		final String mail   = getMailString();
		final String pass   = getPasswordString();
		// - Name is set in OnClick()
		
		final EditText input = new EditText(this);
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
		alertDialog.setTitle("Enter your name");
		
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
		                      LinearLayout.LayoutParams.MATCH_PARENT,
		                      LinearLayout.LayoutParams.MATCH_PARENT);
		
		input.setLayoutParams(lp);
		alertDialog.setView(input); // uncomment this line
		
		final ActivityCreateUser thisActivity = this;
		
		alertDialog.setPositiveButton("Finish", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				String name = input.getText().toString();
				LoginManager.getInstance().setNewUserEmail(mail);
				LoginManager.getInstance().setNewUserPassword(pass);
				LoginManager.getInstance().setNewUserGender(gender);
				LoginManager.getInstance().setNewUserName(name);
				
				thisActivity.finish();
			}
		});
		alertDialog.setNegativeButton("Cancel", null);
		
		alertDialog.show();
		input.requestFocus();
	}
}
