package com.example.android.navigationdrawerexample;

import java.util.ArrayList;

import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.FlashDrive;
import com.android.minglr.models.LoginManager;
import com.android.minglr.models.UserCreationInterface;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.Facebook;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * This activity will house the screen and logic for when the user is not signed in.
 * The user will have the choice between signup and login, either with mail or Facebook
 * Facebook tutorial told me to extend FragmentActivity instead of Activity.
 * @author Robert
 *
 */
public class HomeActivity extends FragmentActivity  {

	public HomeActivity() {
		// TODO Auto-generated constructor stub
	}
	
	private UiLifecycleHelper _uiHelper;
	
	@Override
	protected void onCreate(Bundle bundle){
		
		super.onCreate(bundle);
		setContentView(R.layout.activity_home);
		
		System.out.println("HomeActivity onCreate()");
		
		//
		// - Assign listeners so that we can get callBacks to the Session.StatusCallback
		// - instance variable can get the callbacks
		_uiHelper = new UiLifecycleHelper(this, _callBack);
		_uiHelper.onCreate(bundle);
		
		//
		// - Main layout background
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		mainLayout.setBackgroundResource(R.drawable.blurred_bg);
		
		
		//
		// - Assign buttons
		LoginButton signInFbButton = (LoginButton) findViewById(R.id.sign_in_fb);
		Button signInMailButton = (Button) findViewById(R.id.sign_in_mail);
		
		//
		// - Add on click listener to the (sign-in) mailButton
		signInMailButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mailSignInButtonPressed();
			}
		});
		
		//
		// - Logout everything, since when this activity is on display
		// - we have either logged out or never logged in anyway
		// - TODO "logout everything" logic
		Session.getActiveSession().closeAndClearTokenInformation();
		finishFacebookSignOnWithSuccess(false);
		
		//
		// - Add listeners to textfields
		final EditText mailField = (EditText) findViewById(R.id.mailField); 
		final EditText passwordField = (EditText) findViewById(R.id.passwordField);
		
		passwordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				
				// - The user pressed enter when on the passwordField
				if(actionId == 6){
					String username = mailField.getText().toString();
					String password = passwordField.getText().toString();
					
					// - Return if there are no length for either of the credential strings 
					if(username.length() == 0 || password.length() == 0)
						return false;
					
					signInWithCredentials(username, password, false);
				}
				
				return false;
			}
		});
		
		// -
		// - Signup/create new user
		Button signupButton = (Button)findViewById(R.id.sign_up_button);
		signupButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				signUpButtonPressed();
			}
		});
	}
	
	@Override
	public void onResume(){
		super.onResume();
		_uiHelper.onResume();
		
		//
		// _ Check if we have created a new user
		if(LoginManager.getInstance().newUserShouldBeCreated()){
			System.out.println("NEW USER SHOULD BE CREATED");
			System.out.println("Come on");
			
			//
			// - Create new user with "block"
			LoginManager.getInstance().createNewUser(new UserCreationInterface() {
				
				// - Upon completion
				@Override
				public void userCreationComplete(Boolean success, String user_id, String mail, String pass) {
					
					// - This statement will be true if the creation was successful
					if(success && user_id != null && mail != null && pass != null){
						System.out.println("User creation completed OK! :)");
						System.out.println("User_id: " + user_id);
						
						signInWithCredentials(mail, pass, false);
					}
					else{
						System.out.println("User creation completed FAILED! :(");
					}
				}
			});
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		super.onActivityResult(requestCode, resultCode, data);
		_uiHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onPause() {
	    super.onPause();
	    _uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    _uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    _uiHelper.onSaveInstanceState(outState);
	}

	/**
	 * Handles the event when a user presses the sign in with facebook button
	 * __DEPREACEATED because we use the facebook login button instead__ 
	 */
	private void facebookSignInButtonPressed(){
		System.out.println("Pressed sign in with Facebook button");
	}
	
	/**
	 * Handles the event when a user presses the sign in with mail button
	 */
	private void mailSignInButtonPressed(){
		System.out.println("Pressed sign in with mail button");
		
		EditText mailField = (EditText) findViewById(R.id.mailField); 
		EditText passField = (EditText) findViewById(R.id.passwordField);
		
		// - Assign the strings we would use as credentials
		String mail = mailField.getText().toString();
		String pass = passField.getText().toString(); 
		
		// - Both strings has to have length
		if(mail.length() > 0 && pass.length() > 0){
			signInWithCredentials(mail, pass, false);
		}
	}
	
	/**
	 * Handles the event when the user presses the signup button. It 
	 * will start a new intent
	 */
	private void signUpButtonPressed(){
		Intent signupIntent = new Intent(this, ActivityCreateUser.class);
		startActivity(signupIntent);
	}
	
	/**
	 * When facebook state changes, this function will be called and can delegate tasks based on state
	 * @param session the session which has had a state change
	 * @param state the new state
	 * @param e the exception if there is one
	 */
	private void onSessionStateChange(Session session, SessionState state, Exception e){
		if (state.isOpened()) {
			//
			// - this call will get the facebookAccess token from the session
			// - save it as credentials if there are any, and then make the api-call to
			// - verify the credentials 
			getFacebookAccessToken(session);
	    } else if (state.isClosed()) {
	    	//
	    	// -
	        System.out.println("State is closed");
	        finishFacebookSignOnWithSuccess(false);
	    }
	}
	
	/**
	 * This will handle the callback from the session state and move on to onSessionStateChange
	 */
	private Session.StatusCallback _callBack = new Session.StatusCallback() {
		
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};
	
	
	/**
	 * This function will get the accessToken from the session we just signed in with
	 * and then we try to sign in with Facebook access token as credentials
	 * @param session
	 */
	private void getFacebookAccessToken(final Session session){
		
		// - Get the access token from the session
		String accessToken = session.getAccessToken();

		// - Sign in with credentials
		signInWithCredentials("facebook_access_token", accessToken, true);
	}
	
	/**
	 * This function will take credentials and verify sign in
	 * @param username username variable in credentials
	 * @param password password variable in credentials
	 * @param FacebookSignIn if this is Facebook sign in or not
	 */
	private void signInWithCredentials(String username, String password, final boolean facebookSignIn){
		
		//
		// - Save credentials
		FlashDrive.getInstance().setCredentials(username, password);
		
		//
		// - And when the credentials are saved, we can make the api call
		ApiDownload apiDownload = new ApiDownload();
		apiDownload.startDownload("user/check_credentials", "status", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				
				if(result == null){
					//
					// Empty credentials
					FlashDrive.getInstance().setCredentials("", "");
					
					if(facebookSignIn)
						finishFacebookSignOnWithSuccess(false);
					else finishCredentialSignOnWithSuccess(false);
					
					return;
				}
				//
				// - Get the status message
				// - "ok" if the sign in is verified
				String status = result.getObject().get("status");
				if(status.equals("ok")){
					
					//
					// - If this is a facebook sign in
					if(facebookSignIn == true){
						finishFacebookSignOnWithSuccess(true);
					}
					else{
						finishCredentialSignOnWithSuccess(true);
					}
					
				}
				//
				// - Sign in verification failed
				else{
					//
					// Empty credentials
					FlashDrive.getInstance().setCredentials("", "");
					
					if(facebookSignIn)
						finishFacebookSignOnWithSuccess(false);
					else finishCredentialSignOnWithSuccess(false);
				}
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
			}
		});
	}
	
	/**
	 * This function will finish this activity if the facebook sign on was verified ok
	 * @param success 
	 */
	private void finishFacebookSignOnWithSuccess(Boolean success){
		String successString;
		if(success){
			successString = "yes";
		}
		else successString = "no";
		//
		// - Save fb_signed_in
		FlashDrive.getInstance().saveString(successString, "fb_signed_in");
		
		// - We also have to set minglr as not signed in
		FlashDrive.getInstance().saveString("no", "minglr_signed_in");
		
		// - Print
		System.out.println("Finishin home activity...");
		
		// - Finish
		if(success)
			finish();
	}
	
	private void finishCredentialSignOnWithSuccess(Boolean success){
		String successString;
		if(success)
			successString = "yes";
		else successString = "no";
		
		//
		// - Save minglr_signed_in
		FlashDrive.getInstance().saveString(successString, "minglr_signed_in");
		
		//
		// - We also have to set Facebook as not signed in
		FlashDrive.getInstance().saveString("no", "fb_signed_in");
		
		if(success)
			finish();

	}
}
