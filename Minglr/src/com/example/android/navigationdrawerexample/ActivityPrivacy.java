package com.example.android.navigationdrawerexample;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * This class shows the privacy document from the settings fragment
 * @author Robert
 *
 */
public class ActivityPrivacy extends Activity {

	public ActivityPrivacy() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle bundle){
		setContentView(R.layout.activity_plain_text_view);
		
		TextView textView = (TextView)findViewById(R.id.textView1);
		textView.setText(R.string.lorem_ipsum);
		
		super.onCreate(bundle);
	}

}
