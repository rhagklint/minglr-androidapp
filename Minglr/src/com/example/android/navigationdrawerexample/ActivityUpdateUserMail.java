package com.example.android.navigationdrawerexample;

import java.util.ArrayList;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;

public class ActivityUpdateUserMail extends ActivityUpdateUser {

	public ActivityUpdateUserMail() {
		// TODO Auto-generated constructor stub
	}

	private String _mail;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		
		setContentView(R.layout.activity_update_user_alias);
		
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, R.id.change_layout);
		setDrawableResourceForLayoutId(R.drawable.button_yellow_clean, R.id.change_button);
		
		RelativeLayout mainLayout = (RelativeLayout)findViewById(R.id.main_layout);
		mainLayout.setBackgroundResource(R.drawable.clean_background);
		
		EditText changeTextView = (EditText)findViewById(R.id.change_edit_text);
		changeTextView.setTextColor(Color.WHITE);
		changeTextView.setPadding(25, 0, 0, 0);
		changeTextView.clearFocus();
		
		Button changeButton = (Button)findViewById(R.id.change_button);
		changeButton.setTextColor(Color.WHITE);
		changeButton.setText("Change mail");
		
		//
		// - When getting user mail, we don't download the user, but the user settings
		ApiDownload apiDownload = new ApiDownload();
		apiDownload.startDownload("user/settings", "settings", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				handleMailDownloaded(result);
			}
			
			// - NOT USED
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
			}
		});
		
		super.onCreate(savedInstanceState);
	}
	
	private void handleMailDownloaded(ApiObject settings){
		System.out.println("Settings mail downloaded: " + settings.getObject().toString());
		
		ApiObject refinedSettings = ApiObjectHandler.refineObject(settings, "settings");
		String mail = refinedSettings.getObject().get("email");
		
		_mail = mail;
		
		if(mail != null && mail.length() > 0){
			EditText changeTextView = (EditText)findViewById(R.id.change_edit_text);
			changeTextView.setText(mail);
		}
	}

	@Override
	void handleUserDownloaded(ApiObject user) {
		
		// - Not used here
	}

	@Override
	void handleChangeButtonTapped() {
		System.out.println("Change button tapped");
		EditText changeTextView = (EditText)findViewById(R.id.change_edit_text);
		
		if(changeTextView.getText().length() == 0 ||
		   changeTextView.getText().toString().equals(_mail)){
			System.out.println("This is already your mail");
			return;
		}
		
		ApiPost apiPost = new ApiPost();
		apiPost.addParameter("email", changeTextView.getText().toString());
		apiPost.startPostDataTo("user/update", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				System.out.println("Response: " + response);	
			}
		});
		
	}

	@Override
	void handleConfirmButtonTapped() {
		// TODO Auto-generated method stub
		
	}

}
