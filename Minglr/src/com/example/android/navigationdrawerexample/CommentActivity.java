package com.example.android.navigationdrawerexample;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.adapters.ArrayAdapterChat;
import com.android.minglr.adapters.ArrayAdapterComments;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.android.minglr.models.FileManager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class CommentActivity extends Activity implements ApiDownloadInterface, ApiPostInterface {

	/********* INSTANCE VARIABLES ************/
	//
	// - The list view which we will assign onCreate(...) with findViewById
	ListView _listView;
	
	//
	// - Conversation_id of the currently shown conversation
	String _status_id;
	
	//
	// - The list of messages
	ArrayList <ApiObject> _data = new ArrayList <ApiObject> (); 
	
	//
	// - Original post
	ApiObject _originalPost;
	
	/********* INSTANCE VARIABLES ************/
	
	public CommentActivity() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_comments);
		
		_status_id = getIntent().getExtras().getString("status_id");
		
		//
		// - Assign the listView property by findViewById
		// - to make it easily accessible throughout this class
		_listView = (ListView) findViewById(R.id.comments_list);
		
		
		/***************** DOWNLOAD MESSAGES **************/
		startDownloadTask(_status_id);
		/***************** DOWNLOAD MESSAGES **************/
		
		//
		// - Add listener to the post button
		Button postButton = (Button) findViewById(R.id.postButton);
		postButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText postField = (EditText) findViewById(R.id.postField);
				if(postField.getText().length() > 0){
					// - Post
					postChatMessage(postField.getText().toString());
					// - Clean text
					postField.setText("");
					// - TODO hide the keyboard
				}
			}
		});
		
		//
		// - TODO
		// - Add like tapped broadcast receiver
		LocalBroadcastManager.getInstance(
				this).registerReceiver(
						mTappedNiceReceiver, 
						new IntentFilter("broadcast_nice_tapped")
						);
		
	}
	
	@Override

	public void onDestroy(){
		super.onDestroy();
		LocalBroadcastManager.getInstance(this).unregisterReceiver(mTappedNiceReceiver);
	}
	
	// - This function starts a task to download the messages of this conversation, with conversation_id
	// - the result will be posted to GetDataTask::onPostExecute()
	// - @params 
	// - String conversation_id (the conversation you want to download the messages to)
	private void startDownloadTask(String status_id){
		//
		// - Allocate a new task to download the messages for the conversation
		ApiDownload downloadInstance = new ApiDownload();
		downloadInstance.startDownload("feed/status/" + status_id, "whatever", true, this);
	}

	//
	// - UpdateListView function which is used within the private class "GetDataTask" to update the listView
	// - with the newly downloaded content
	private void updateListView(ArrayList <ApiObject> result){
		
		//
		// - We have to add the original post on top of this array
		ArrayList <ApiObject> objects = new ArrayList<ApiObject>();
		
		if(_originalPost != null)
			objects.add(_originalPost);
		
		objects.addAll(result);
		System.out.println("objects count: " + objects.size());
		System.out.println("result count:  " + result.size());
		
		ArrayAdapterComments adapter = new ArrayAdapterComments(
				this, 
				android.R.layout.simple_expandable_list_item_1, 
				objects);
		
		adapter.setContext(getApplicationContext());
		
		_listView.setAdapter(adapter);
	}
	
	//
	// - TODO
	private void postChatMessage(String message){
		
		ApiPost postInstance = new ApiPost();
		postInstance.addParameter("text", message);
		postInstance.addParameter("status_id", _status_id);
		
		postInstance.startPostDataTo("feed/add_status_comment", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
		//
		// - Get the user on the device
		HashMap <String, String> userMap = FileManager.readMapFromFile(FileManager.getAppendedFileName("user_map", this));
		ApiObject userObject = new ApiObject(userMap);
		
		//
		// - Create the new comment
		HashMap <String, String> comment = new HashMap<String, String>();
		comment.put("text", message);
		comment.put("status_id", _status_id);
		comment.put("user_id", ApiObjectHandler.UserObject.getId(userObject));
		comment.put("user_name", ApiObjectHandler.UserObject.getName(userObject));
		
		//
		// - Update the _data arrayList
		ApiObject commentObject = new ApiObject(comment);
		_data.add(commentObject);
		updateListView(_data);
		
	}

	//
	// - DOCS
	// - Api post interface call
	@Override
	public void postDataComplete(String response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void arrayFetchComplete(ArrayList<ApiObject> result,
			String variableName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void objectFetchComplete(ApiObject result, String variableName) {
		// TODO Auto-generated method stub
		//
		// - set original post
		JSONObject post = null;
		
		try {
			System.out.println("Printing: " + result.getObject().toString());
			post = new JSONObject(result.getObject().get("status"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		_originalPost = new ApiObject(post);
		
		
		// - Update list view
		updateListView(ApiObjectHandler.FeedObject.getCommentList(result));
		_data = ApiObjectHandler.FeedObject.getCommentList(result);
	}

	/**
	 * When the user taps the like button of the base post
	 */
	private BroadcastReceiver mTappedNiceReceiver = new BroadcastReceiver(){
		
		//
		// - This member will handle messages sent from the ArrayAdapterApiObject
		// - when the user taps the nice button
		@Override
		public void onReceive(Context context, Intent intent) {

			final int messageInt = intent.getIntExtra("message", 0);
			System.out.println("Received message from index: " + messageInt);
			
			// - Get the object for the row
			ApiObject object = _originalPost;
			String status_id = ApiObjectHandler.FeedObject.getId(object);
			
			
			ApiPost apiPost = new ApiPost();
			apiPost.addParameter("status_id", status_id);
			apiPost.startPostDataTo("feed/add_status_nice", new ApiPostInterface() {
				
				@Override
				public void postDataComplete(String response) {
					// TODO Auto-generated method stub
					//ArrayAdapterApiObject adapter = (ArrayAdapterApiObject) getListAdapter();
					//adapter.updateRow(messageInt, getListView());
				}
			});
		}
		
	};
}
