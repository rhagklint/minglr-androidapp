package com.example.android.navigationdrawerexample;

import java.util.ArrayList;

import com.android.minglr.models.ImageDownload;
import com.android.minglr.models.ImageDownloadInterface;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.ImageView;

/**
 * Class will display a clicked image on the screen
 * @author Robert
 *
 */
public class ImageViewActivity extends Activity {

	public ImageViewActivity() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		// - Super call
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_view);
		
		//
		// - Get the url_string for the image
		String url_string = getIntent().getExtras().getString("url_string");
		
		// - Allocate a download instance
		ImageDownload imageDownload = new ImageDownload();
		
		// - And start the download
		imageDownload.downloadImage(url_string, new ImageDownloadInterface() {
			
			@Override
			public void imageListFinished(ArrayList<Drawable> imageList) {
				
				//
				// - We know that there is max 1 image (if the download went correctly)
				if(imageList.size() > 0){
					Drawable imageDrawable = imageList.get(0);
					displayImage(imageDrawable);
				}
			}
		});
	}
	
	/**
	 * Will draw the Drawableon the imageView
	 * @param image is the drawable we will display
	 */
	private void displayImage(Drawable image){
		
		ImageView imageView = (ImageView) findViewById(R.id.image_view);
		imageView.setImageDrawable(image);
	}

}
