package com.example.android.navigationdrawerexample;

import java.util.ArrayList;

import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * This class will function as the abstract superclass for all activites where we "update" or "change"
 * variables for the user. These activities are accessed via the Settings fragment 
 * @author Robert
 *
 */
abstract class ActivityUpdateUser extends Activity {

	/**
	 * Refers to the button which has the title "change"
	 * and will begin the process of the user updating their value
	 * for a specific variable
	 */
	protected Button _changeButton;
	
	/**
	 * Refers to the button which will process the new information
	 * and send it to the server. This is the confirm button
	 */
	protected Button _confirmButton;
	
	public ActivityUpdateUser() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//
		// - Download the user
		ApiDownload apiDownload = new ApiDownload();
		apiDownload.startDownload("user/get/me", "", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				handleUserDownloaded(result);
			}
			
			// - NOT USED
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// - Assign the change button before we call super.onCreate()
		_changeButton = (Button)findViewById(R.id.change_button);
		_confirmButton = (Button)findViewById(R.id.confirm_button); 
		
		if(_changeButton == null)
			return;
		//
		//  - Add the listener to the changeButton
		_changeButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handleChangeButtonTapped();
			}
		});
		
		if(_confirmButton == null)
			return;
		
		_confirmButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handleConfirmButtonTapped();
			}
		});
		
	}
	
	/**
	 * This function will be called when the user is downloaded. That process (of downloading the user)
	 * is started in onCreateMethod in this abstract superclass
	 * @param user
	 */
	abstract void handleUserDownloaded(ApiObject user);

	/**
	 * This function will be called when the user taps the "change button"
	 */
	abstract void handleChangeButtonTapped();
	
	/**
	 * This function will be called when the user taps the "confirm button"
	 */
	abstract void handleConfirmButtonTapped();
	
	/**
	 * Simple help function to set drawable backround resource for layout/view id
	 * @param drawableResource
	 * @param layoutId
	 */
	protected void setDrawableResourceForLayoutId(int drawableResource, int layoutId){
		View view = (View)findViewById(layoutId);
		view.setBackgroundResource(drawableResource);
		
		BitmapDrawable background = (BitmapDrawable)view.getBackground(); // assuming you have bg_tile as background.
		BitmapDrawable newBackground = new BitmapDrawable(background.getBitmap()) {
		    @Override
		    public int getMinimumWidth() {
		        return 0;
		    }

		    @Override
		    public int getMinimumHeight() {
		        return 0;
		    }
		};
		newBackground.setTileModeXY(background.getTileModeX(), background.getTileModeY());
		view.setBackgroundDrawable(newBackground);
	}
}
