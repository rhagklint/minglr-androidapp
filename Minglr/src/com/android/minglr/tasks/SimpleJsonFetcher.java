package com.android.minglr.tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.FlashDrive;

import android.R.string;
import android.content.Context;
import android.net.Credentials;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;


/** Task **/
public class SimpleJsonFetcher extends AsyncTask<String, Integer, ArrayList<ApiObject>> {
	
	static String API_ROOT = "http://api-test.minglr.se/";
 	static String REST_API_KEY = "2udn1gvz9h6f3dl22yjep8qta75a83pvhnwqumyp";
 	static String METHOD_POST = "POST";
 	static String METHOD_GET = "GET";
 	
 	//
 	// _ Every subclass defines it's own url
 	// - depending on wheter get or post is allocated
 	// - we change performer
 	protected String get_request_url;
 	protected String post_request_url;
 	protected String array_variable; // - the variable from which we make the array (eg. "feed" or "user")
 	
 	protected Context _context;
 	
 	public void setGetRequestURL(String url){
 		get_request_url = url;
 	}
 	
 	public void setPostRequestURL(String url){
 		post_request_url = url;
 	}
 	
 	public void setArrayVariable(String variable){
 		array_variable = variable;
 	}
 
	// - Boilerplate
	protected HttpClient getClient(){
		HttpClient client = new DefaultHttpClient();
		return client;
	}
	
	// - Boilerplate add headers
	protected void setDefaultHeaders (HttpRequestBase request){
		// - For authentication
		//String userName = "facebook_access_token";
		//String password = "CAATtCwwtyr4BALZAZAjm9hgZCNZC93oZAjaQfoOp44Tc27DVED1g1pnNG8Sg5kShd8H9WSx17gLbD7aZBnlWGadMNYTFlQAZCZCSZAtKBkYyJ0byH4tZCueZBu5D5x1jJZBWarKld6ng3RESe1MrL0vPrB3avdfrcMkTLFU5ahHifVCoMpY0BGTAAZBdZA";
		
		System.out.println("Context: " + _context);
		
		String userName = FlashDrive.getInstance().getCredentials().get("username");
		String password = FlashDrive.getInstance().getCredentials().get("password");
		
		System.out.println("userName: " + userName);
		System.out.println("password: " + password); 

		// - Credentials
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(userName, password);
		Header authHeader = BasicScheme.authenticate(credentials, "UTF-8", false);
		request.addHeader(authHeader);
		System.out.println("AuthHeader: " + authHeader.toString());
		request.addHeader("ACCEPT-LANGUAGE", "en");
		// - X-API-KEY
		request.addHeader("X-API-KEY", REST_API_KEY);		
		System.out.println("Request: " + request.toString());
		System.out.println("UrlString: " + get_request_url);
	}
	
	// - Boilerplate - sets up a getRequest - in this case with specific data and 
	protected HttpGet getRequest(String requestString) throws UnsupportedEncodingException{
		
		// - Allocate request
		HttpGet getRequest = new HttpGet(requestString);
		setDefaultHeaders(getRequest);
		return getRequest;
	}
	
	// - Boilerplate
	protected HttpPost getPostRequest(){
		
		// - Allocate request
		HttpPost postRequest = new HttpPost();
		setDefaultHeaders(postRequest);
		return postRequest;
	}
	
	@Override
    protected void onPreExecute() {
		// - TODO
    }
     
    @Override
    protected ArrayList<ApiObject> doInBackground(String... url) {
    	
    	// - TODO
    	HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response;
        String responseString = null;
    	
        try {
        	//response = httpclient.execute(this.getRequest("http://api-test.minglr.se/feed/userwall?limit=5"));
        	response = httpclient.execute(this.getRequest(get_request_url));
            StatusLine statusLine = response.getStatusLine();
            
            System.out.println("Status: " + statusLine.toString());
            System.out.println("StatusCode: " + statusLine.getStatusCode());
            if(statusLine.getStatusCode() == HttpStatus.SC_OK){
            	
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                responseString = out.toString();
                
            } else{
            	
                //Closes the connection.
                response.getEntity().getContent().close();
                throw new IOException(statusLine.getReasonPhrase());
                
            }
            
        } catch (ClientProtocolException e) {
        	
            //TODO Handle problems..
        	System.out.println("Error client");
        	
        } catch (IOException e) {
        	
            //TODO Handle problems..
        	System.out.println("Error exception: " + e.getMessage());
        	Log.e("Error exception", e.toString());
        	
        }
        
        // - Here we will store the result
    	ArrayList<ApiObject> dataList = new ArrayList <ApiObject>();
    	
        try {
        	
        	//System.out.println("Response string: " + responseString);
        	
        	// - Get the raw object
        	if(responseString == null){
        		return dataList;
        	}
        	JSONObject rawObject = new JSONObject(responseString);
        	//System.out.println("Response string: " + responseString);
        	
        	// - Get the feed value
        	JSONArray jsonArray = rawObject.getJSONArray(array_variable);
        	
        	// - Loop thru them all
        	for(int i = 0; i < jsonArray.length(); i++){
        		
        		// - Current jsonObject
        		JSONObject jsonObject = (JSONObject) jsonArray.get(i);
        		
        		// - Converted to apiObject
        		ApiObject apiObject = new ApiObject(jsonObject);
        		
        		dataList.add(apiObject);
        		
        		//System.out.println("New Object: " + jsonObject);
        		//System.out.println("-----");
        	}
			
		} catch (JSONException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("JsonObject could not be created");
		}
    	
    	return dataList;
    }
     
    @Override
    protected void onProgressUpdate(Integer... progress) {
    	// - TODO
    	System.out.println("Progress " + progress);
    }
     
    @Override
    protected void onPostExecute(ArrayList<ApiObject> result) {
    	// Publish result
   }
}