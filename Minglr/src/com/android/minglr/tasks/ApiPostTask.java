package com.android.minglr.tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.FlashDrive;

import android.os.AsyncTask;

/*
 * This will function pretty similar to the SimpleJsonFetcher
 * Except that it will post instead of download in the doInBackground method
 * and have different class of result and parameters
 */
public class ApiPostTask extends AsyncTask<String, Integer, String> {

	public ApiPostTask() {
		// TODO Auto-generated constructor stub
	}
	static String API_ROOT = "http://api-test.minglr.se/";
 	static String REST_API_KEY = "2udn1gvz9h6f3dl22yjep8qta75a83pvhnwqumyp";
 	static String METHOD_POST = "POST";
 	static String METHOD_GET = "GET";
 	
 	//
 	// _ Every subclass defines it's own url
 	// - depending on wheter get or post is allocated
 	// - we change performer
 	protected String post_request_url;
 	
 	/**
 	 * Only set the default url, parameters should be set seperatly
 	 * @param url
 	 */
 	public void setPostRequestURL(String url){
 		post_request_url = url;
 	}
 	
 	//
 	// - variable for all the paramters 
 	protected ArrayList <BasicNameValuePair> _parameterList;
 	
 	/**
 	 * Adds a new parameter to the pending task to post
 	 * @param param
 	 */
 	public void addParameter(BasicNameValuePair param){
 		if(_parameterList == null){
 			_parameterList = new ArrayList<BasicNameValuePair>();
 		}
 		
 		_parameterList.add(param);
 	}
 
	// - Boilerplate
	protected HttpClient getClient(){
		HttpClient client = new DefaultHttpClient();
		return client;
	}
	
	// - Boilerplate add headers
	protected void setDefaultHeaders (HttpRequestBase request){
		// - For authentication
		//String userName = "facebook_access_token";
		//String password = "CAATtCwwtyr4BALZAZAjm9hgZCNZC93oZAjaQfoOp44Tc27DVED1g1pnNG8Sg5kShd8H9WSx17gLbD7aZBnlWGadMNYTFlQAZCZCSZAtKBkYyJ0byH4tZCueZBu5D5x1jJZBWarKld6ng3RESe1MrL0vPrB3avdfrcMkTLFU5ahHifVCoMpY0BGTAAZBdZA";
		String userName = FlashDrive.getInstance().getCredentials().get("username");
		String password = FlashDrive.getInstance().getCredentials().get("password");
		
		// - Credentials
		UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(userName, password);
		Header authHeader = BasicScheme.authenticate(credentials, "UTF-8", false);
		request.addHeader(authHeader);
		
		// - X-API-KEY
		request.addHeader("X-API-KEY", REST_API_KEY);
	}
	
	
	// - Boilerplate
	protected HttpPost getPostRequest(String requestString){
		
		// - Allocate request
		HttpPost postRequest = new HttpPost(requestString);
		setDefaultHeaders(postRequest);
		HttpURLConnection connection;
		return postRequest;
	}
	
	@Override
	protected String doInBackground(String... url){
		
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		String responseString = null;
		
		try {
			HttpPost post = this.getPostRequest(post_request_url);
			post.setEntity(new UrlEncodedFormEntity(_parameterList));
			
			response = client.execute(post);
			StatusLine statusLine = response.getStatusLine();
			
			
			if(statusLine.getStatusCode() == HttpStatus.SC_OK){
	        	
	            ByteArrayOutputStream out = new ByteArrayOutputStream();
	            response.getEntity().writeTo(out);
	            out.close();
	            responseString = out.toString();
	            
	        } else{
	        	
	            //Closes the connection.
	            response.getEntity().getContent().close();
	            throw new IOException(statusLine.getReasonPhrase());
	            
	        }
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return responseString;
	}
	
	@Override
    protected void onProgressUpdate(Integer... progress) {
    	// - TODO
    	System.out.println("Progress " + progress);
    }
     
    @Override
    protected void onPostExecute(String result) {
    	// Publish result
   }
}
