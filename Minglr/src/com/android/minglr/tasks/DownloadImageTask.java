package com.android.minglr.tasks;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

public class DownloadImageTask extends AsyncTask<String, Integer, Drawable> {

	//ImageView _handledImageView;
	String _imageURL;
	Context _context;
	
	// - Init with the imageView and url
	public DownloadImageTask(String url){
		//_handledImageView = imageView;
		_imageURL = url;
		//_context = context;
	}
	
	@Override
	protected Drawable doInBackground(String...url){
		return this.ImageOperations(_imageURL, _imageURL);
	}
	
	@Override
	protected void onPostExecute(Drawable drawableImage){
		// - LEGACY
		//_handledImageView.setImageDrawable(drawableImage);
	}
	
	// - Create the drawable
	private Drawable ImageOperations(/*Context ctx, */String url, String saveFilename) {
        try {
            InputStream is = (InputStream) this.fetch(url);
            Drawable drawable = Drawable.createFromStream(is, url);
            
            return drawable;
        } catch (MalformedURLException e) {
        	System.out.println("Drawable error");
            return null;
        } catch (IOException e) {
        	System.out.println("Drawable error");
            return null;
        }
    }
	
	// - Fetches the input stream
	private Object fetch(String address) throws MalformedURLException,IOException {
        URL url = new URL(address);
        Object content = url.getContent();
        return content;
    }

	// - Fetch the handled imageView
}
