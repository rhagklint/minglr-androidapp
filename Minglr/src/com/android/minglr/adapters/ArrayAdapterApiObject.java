package com.android.minglr.adapters;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.DateFormatter;
import com.android.minglr.models.ImageDownload;
import com.android.minglr.models.ImageDownloadInterface;
import com.android.minglr.tasks.DownloadImageTask;
import com.android.minglr.tasks.GetUserTask;
import com.example.android.navigationdrawerexample.R;
import com.google.android.gms.drive.internal.u;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout.Alignment;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.TextView;

// - An adapter class for the basic ApiObject. Will be used for all kinds of feeds. Might need to be sublclassed
public class ArrayAdapterApiObject extends ArrayAdapter<ApiObject> {
	
	// _ Context
	protected Context context;
	
	// - Array of users
	protected ArrayList <String> _user_ids = new ArrayList <String>();
	
	
	// ************************************************** //
	// ---------------- PROFILE IMAGE ------------------- //
	// ************************************************** //
	
	// - HashMap of user profile images
	HashMap <String, String> _profileImageStringForUserId = new HashMap<String, String>();
	void addProfileImageStringForUserId(String image, String user_id){
		_profileImageStringForUserId.put(user_id, image);
	}

	// - HashMap of Drawable (profilePicture) for user id
	HashMap<String, Drawable> _drawableForUserID = new HashMap<String, Drawable>();
	protected void addDrawableForUserID(String user_id, Drawable drawable){
		_drawableForUserID.put(user_id, drawable);
	}
	
	// - Returns status on existing profile picture for user_id
	Boolean profileImageExists(String user_id){
		
		if(_drawableForUserID == null){
			System.out.println("DrawableForUserID map: " + _drawableForUserID);
			return null;
		}
		
		return _drawableForUserID.containsKey(user_id);
	}
	
	// - Returns existing profile picture for user_id
	Drawable getProfileImage(String user_id){
		
		Drawable profileImage = _drawableForUserID.get(user_id);
		return profileImage;
		
	}
	
	/**
	 * Contains an array of image urls for every status id
	 * we need these when the user clicks on an imageView
	 */
	HashMap<String, ArrayList<String>> imageUrlsForStatusID;
	private ArrayList<String> getImageURLS(String status_id){
		return imageUrlsForStatusID.get(status_id);
	}
	
	/**
	 * Puts the arrayList as value for the key status_id in the 
	 * hashmap containing image urls for each and every status_id
	 * @param status_id
	 * @param urls
	 */
	private void setImageURLS(String status_id, ArrayList <String> urls){
		if(imageUrlsForStatusID == null){
			imageUrlsForStatusID = new HashMap<String, ArrayList<String>>();
		}
		imageUrlsForStatusID.put(status_id, urls);
	}
	
	// ************************************************** //
	// ---------------- PROFILE IMAGE ------------------- //
	// ************************************************** //
	
	
	// ------------------------------------------------ //
	
	
	// ************************************************** //
	// -------------------- IMAGES   ------------------- //
	// ************************************************* //
	
	// - HashMap with value of arrayList of drawables for each status_id
	HashMap <String, ArrayList<Drawable>> _drawablesForStatusID = new HashMap <String, ArrayList<Drawable>>();
	void addDrawableForStatusID(String status_id, Drawable drawable){
		
		// - First we check if there is any arrayList for this key atm
		if(!_drawablesForStatusID.containsKey(status_id)){
			ArrayList <Drawable> newArrayList = new ArrayList<Drawable>();
			_drawablesForStatusID.put(status_id, newArrayList);
		}
		
		// - Then we fetch the arrayList
		ArrayList<Drawable> drawablesForThisStatus = getImagesForStatusID(status_id);
		
		// - Add the drawable into the array
		drawablesForThisStatus.add(drawable);
		
		// - and put the updated array back into the hash map
		_drawablesForStatusID.put(status_id, drawablesForThisStatus);
	}

	// - Returns status on existing image for status id
	Boolean imagesExistsForStatus(String status_id){
		return _drawablesForStatusID.containsKey(status_id);
	}

	// - returns the existing images for this status id
	ArrayList<Drawable> getImagesForStatusID(String status_id){
		return _drawablesForStatusID.get(status_id);
	}
	
	// ************************************************** //
	// -------------------- IMAGES   ------------------- //
	// ************************************************* //
	
	
	// ------------------------------------------------ //
	
	
	// - Constructor
	public ArrayAdapterApiObject(
			Context context, 
			int textViewResourceId, 
			ArrayList<ApiObject> items) {
		
        super(context, textViewResourceId, items);
        this.context = context;
    }
	
	// - Get view
	// - RetrieveImageTask used here
	// - GetUserTaskPrivate used here
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View view = convertView;
		
		
		// - If view is null
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//view = inflater.inflate(R.layout.simple_list_item_1, null);
			view = inflater.inflate(R.layout.item_feed_cell, parent, false);
		}
		
		ApiObject item = getItem(position); // - getItem(position) gets the item from the arrayList which
		// - is set in the constructor (I guess)
		
		if(item == null)
			return view;
		
		TextView timestampView = (TextView)view.findViewById(R.id.timestamp);
		String timeStamp = item.getObject().get("timestamp");
		timestampView.setTextSize(12);
		
		try {
			Date timeStampDate = DateFormatter.dateFromString(timeStamp);
			Date now = new Date();
			
			timestampView.setText(DateFormatter.readableTimeDifference(timeStampDate, now));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// 
		// - Quick stuff, set message and name 
		String name_ = ApiObjectHandler.UserObject.getName(item);
		String message_ = ApiObjectHandler.FeedObject.getContentMessage(item);
		
		TextView itemView = (TextView)view.findViewById(R.id.nameTextView);
		if(name_ != null && itemView != null)
			itemView.setText(String.format("%s", name_));
		
		TextView mainTextView = (TextView)view.findViewById(R.id.mainTextView);
		if(message_ != null && mainTextView != null){
			mainTextView.setText(String.format("%s", message_));
		}
		
		// 
		// - Profile image setting
		this.handleProfileImage(item, view, R.id.profileImageView);
		
		//
		// - Nice count and comment count
		this.setCommentAndNiceCount(view, item);
		
		//
		// - Set images
		this.handleImages(item, view, position);
		
		// 
		// - Fetch the buttons
		Button commentButton = (Button)getViewWithID(R.id.commentButton, view);
		Button niceButton 	 = (Button)getViewWithID(R.id.niceButton, view);
		
		//  
		// - And set onClickListeners
		this.setClickListenterForCommentButton(commentButton, position);
		this.setClickListenerForNiceButton(niceButton, position);
		this.setClickListenerForNameLabel(itemView, ApiObjectHandler.UserObject.getId(item));
		
		//
		// - Set images and text color for the buttons
		commentButton.setBackgroundResource(R.drawable.button_gray);
		commentButton.setTextColor(Color.WHITE);
		
		niceButton.setBackgroundResource(R.drawable.button_gray);
		niceButton.setTextColor(Color.WHITE);
		
		
		//
		// - Set filler views hidden
		TextView fillerView1 = (TextView)getViewWithID(R.id.filler_tv_1, view);
		TextView fillerView2 = (TextView)getViewWithID(R.id.filler_tv_2, view);
		TextView fillerView3 = (TextView)getViewWithID(R.id.filler_tv_3, view); 
		
		fillerView1.setAlpha(0);
		fillerView2.setAlpha(0);
		fillerView3.setAlpha(0);
		
		
		return view;
	}
	
	/**
	 * Updates a given row in the listView
	 * @param position, the row position
	 */
	public void updateRow(int position, ViewGroup listView){
		View view = listView.getChildAt(position);
		getView(position, view, listView);
	}
	
	
	// ----------------------------------------------------------- //
	
	
	// ************************************************************ //
	// -------------------- SMALL HELPTER FUNCTIONS -------------- //
	// ********************************************************** //
	
	// - Function to get a string for nice and comment count (used in countTextView)
	private String getStringForCounts(int niceCount, int commentCount){
		String commentAndNiceCountString = String.valueOf(niceCount) + " nice | " + String.valueOf(commentCount) + " kommentarer";
		return commentAndNiceCountString;
	}
	
	// - Handle nice and comment count output for superview
	private void setCommentAndNiceCount(View superView, ApiObject item){
		
		// - Nice count
		int niceCount 	 = ApiObjectHandler.FeedObject.getNiceCount(item);
		
		// - Comment count
		int commentCount = ApiObjectHandler.FeedObject.getCommentCount(item);
		
		
		String commentAndNiceCountString = getStringForCounts(niceCount, commentCount);
		TextView commentAndNiceCountTextView = (TextView)getViewWithID(R.id.countTextView, superView);
		if(commentAndNiceCountTextView != null)
			commentAndNiceCountTextView.setText(String.format("%s", commentAndNiceCountString));
		
		// - Set tag so that the OnCLickListener can get the tag later
		// - witht he "view" parameter
		commentAndNiceCountTextView.setTag(ApiObjectHandler.FeedObject.getId(item));
		
		// - Set onClickListener
		commentAndNiceCountTextView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String status_id = v.getTag().toString();
				
				if(status_id != null)
					broadcastNiceListWantsDisplay(status_id);
			}
		});
	}
	
	/**
	 * Handle the profile imaging
	 * @param item the api object which contains the string for the image
	 * @param superView the view which contains the profile
	 * @param image_view_id the resource_id for the profile view
	 */
	protected void handleProfileImage(ApiObject item, View superView, int image_view_id){
		
		// - Get id
		String user_id = ApiObjectHandler.UserObject.getId(item);
		
		// - Fetch the view (the profile image view)
		ImageView imageView = (ImageView)superView.findViewById(image_view_id);
		
		
		// - If we have an existing profile picture for user id
		if(profileImageExists(user_id)){
			
			Drawable profileDrawable = getProfileImage(user_id);
			imageView.setImageDrawable(profileDrawable);
			
			return; // - RETURN
		}
		
		// - If we have no profile image for this user_id
		if(_profileImageStringForUserId.get(user_id) == null){
			
			// - Will initiate the userTask, so we can collect the profile image location in
			//  - onPostExecute  in GetUserTaskPrivate class
			GetUserTaskPrivate userTask = new GetUserTaskPrivate();
			userTask.setUserID(user_id);
			userTask.setArrayVariable("user");
			userTask.setWaitningImageView(imageView); // - This will ensure that we update the image view when
			// - we have downloaded the user info
			userTask.execute();
			
		}
		// - If we have a string for this one (if its longer than 1, since there will be 0 if there was no
		// - picture for the user_id)
		else if(_profileImageStringForUserId.get(user_id).length() > 1){
			
			// - TODO must implement cache of profile images
			
			// - Get the url
			String imageURL = _profileImageStringForUserId.get(user_id);
			
			
			// - Start the task which we have defined privatly in this class
			RetrieveProfileImageTask task = new RetrieveProfileImageTask(imageView, imageURL, user_id);
			task.execute();
		}
		// - The user has been downloaded, but no image for it was found
		else{
			
			imageView.setImageDrawable(null);
		}
	}
	
	// - Download images for the item and set the images to the correct views for the tableRow
	// - TODO must implement cache for object images
	protected void handleImages(ApiObject item, View superView, int position){
		
		// - TODO
		// - Save the images to memory, and reuse them (maybe an array of images/drawables for every
		// - status_id)
		String status_id = ApiObjectHandler.FeedObject.getId(item);
		
		
		// - ArrayList of relevant imageViews for the cell
		ArrayList <ImageView> imageViewArrayList = new ArrayList <ImageView> ();
		ImageView imageView1 = (ImageView) superView.findViewById(R.id.profile_image_view);
		ImageView imageView2 = (ImageView) superView.findViewById(R.id.ImageView2);
		ImageView imageView3 = (ImageView) superView.findViewById(R.id.ImageView3);
		
		imageViewArrayList.add(imageView1);
		imageViewArrayList.add(imageView2);
		imageViewArrayList.add(imageView3);
		// - Finished
		
		// - If we have images for this status_id already
		// - we fetch the drawable image from the cache
		if(imagesExistsForStatus(status_id)){
			
			//
			// - Set all the imageViews as visible
			for (ImageView imageView : imageViewArrayList) {
				imageView.setVisibility(View.VISIBLE);
			}
			
			// - Set images from cache
			Integer usedImageViewsInteger = setImagesFromCache(status_id, imageViewArrayList);
			
			// - we must also clear the image of unsused imageViews
			clearUnusedImageViews(usedImageViewsInteger, imageViewArrayList);
			
			// - Cleanup
			imageViewArrayList.clear();
			imageViewArrayList = null;
			
			return; // _ RETURN
		}
		
		//
		// - Instance of the thumb-imageArray (of strings)
		ArrayList <String> urlArrayList = ApiObjectHandler.FeedObject.Images.getImageThumbStringArray(item);
		//
		// - Add the strings to the hashMap so we
		// - can fetch them when the user clicks any imageView
		if(urlArrayList.size() > 0){
			setImageURLS(status_id, urlArrayList);
		}
				
		// - Variable for counting used imageViews
		// - Starts the retrieving of images, which is an asyncTask that updates the 
		// - imageView upon completion
		Integer usedImageViewsInteger =  startRetrieveImageTask(
				urlArrayList, 
				imageViewArrayList, 
				status_id, 
				position);
	
		//
		// - Detect and set visibility
		if(usedImageViewsInteger == 0){
			for (ImageView imageView : imageViewArrayList) {
				imageView.setVisibility(View.GONE);
			}
		}
		else{
			for(ImageView imageView : imageViewArrayList){
				imageView.setVisibility(View.VISIBLE);
			}
		}
		// -

		
		// - We must also clear the image of unused imageViews
		clearUnusedImageViews(usedImageViewsInteger, imageViewArrayList);
		
		// - Cleanup
		//urlArrayList.clear();
		//urlArrayList = null;
		
		imageViewArrayList.clear();
		imageViewArrayList = null;
	}
	
	// - get view by id
	private View getViewWithID(int id, View superView){
		return superView.findViewById(id);
	}
	
	// - Adds a click listener with handlers for comment button 
	private void setClickListenterForCommentButton(Button button, final int index){
		
		button.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View arg0) {
				
				System.out.println("Clicked commentButton at index: " + index);
				
				// - Send a message to any listenting receiver
				Intent intent = new Intent("broadcast_comment_tapped");
				intent.putExtra("message", index);
				
				// - Send
				// - This will be receiver by fragment_feed
				LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
				
			}
			
		});
	}
	
	private void setClickListenerForNameLabel(final TextView textView, final String user_id){
		
		textView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// - Send a message to any listening receiver
				Intent intent = new Intent("broadcast_name_tapped");
				intent.putExtra("user_id", user_id);
				
				//
				// _ Send
				LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
			}
			
		});
	}
	
	// - Adds a click listener with handlers for nice button
	private void setClickListenerForNiceButton(Button button, final int index){
		
		button.setOnClickListener(new Button.OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				System.out.println("Clicked niceButton at index " + index);
				
				// - Send a message to any listening reciever
				Intent intent = new Intent("broadcast_nice_tapped");
				intent.putExtra("message", index);
				
				// - Send
				// - This will be recieved by fragment feed
				LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
			}
			
		});
	}
	
	// - Clear the unused imageviews
	private void clearUnusedImageViews(int usedCount, ArrayList<ImageView> imageViewArrayList){
		
		Integer usedImageViewsIntegerIncrement = usedCount;
		while(usedImageViewsIntegerIncrement < imageViewArrayList.size()){
			
			// - Get unused imageView
			ImageView unusedImageView = imageViewArrayList.get(usedImageViewsIntegerIncrement);
			
			// - Clear the image resouce/content
			unusedImageView.setImageDrawable(null);
			unusedImageView.setImageResource(0);
			
			usedImageViewsIntegerIncrement++;
		} // -
		
	}
	
	// - Set images from cache for status_id
	private Integer setImagesFromCache(final String status_id, ArrayList<ImageView> imageViewArrayList){
		
		Integer usedImageViewsInteger = 0;
		
		// - Loop thru all the images
		for(int i = 0; i < getImagesForStatusID(status_id).size(); i++){
			
			// - instance of image
			Drawable imageDrawable = getImagesForStatusID(status_id).get(i);
			
			// - Instance of imageview
			if(imageViewArrayList.size() < i + 1)
				continue;

			ImageView imageView = imageViewArrayList.get(i);
			
			
			//setOnClickListenerForImageView(imageView, status_id);
			//
			// - Set on click listener
			imageView.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					// - Send a message to any listenting receiver
					System.out.println("Tapped image");
					
					Intent intent = new Intent("broadcast_image_tapped");
					
					ArrayList <String> urlArrayList = getImageURLS(status_id);
					
					if(urlArrayList.size() > 0){
						
						intent.putExtra("url_string", urlArrayList.get(0));
						// - Send
						// - This will be receiver by fragment_feed
						LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
					}
						
				}
			});
			
			imageView.setImageDrawable(imageDrawable);
			
			usedImageViewsInteger++;
		}
		
		return usedImageViewsInteger;
	}
	
	// - Start retrieveImageTask
	// - Returns an integer with the index of the first unused imageView
	/*
	private Integer startRetrieveImageTask(
			ArrayList<String> urlArrayList,
			ArrayList <ImageView> imageViewArrayList, 
			String status_id){
		
		Integer usedImageViewsInteger = 0;
		
		// - Loop thru all the strings
		for(int i = 0; i < urlArrayList.size(); i++){
			
			// - increment used imageviews integer
			usedImageViewsInteger++;
			
			// - Current image view
			ImageView imageView = imageViewArrayList.get(i);
			
			// - Instance of current string
			String currentURL = urlArrayList.get(i);
			
			// - Create an retrieve image task
			RetrieveImageTask task = new RetrieveItemImageTask(imageView, currentURL, status_id);
			task.execute();
		}
		
		return usedImageViewsInteger;
	}
	*/
	
	private void setOnClickListenerForImageView(ImageView imageView, final String status_id){
		//
		// - Set on click listener
		imageView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				// - Send a message to any listenting receiver
				System.out.println("Tapped image");
				
				Intent intent = new Intent("broadcast_image_tapped");
				
				ArrayList <String> urlArrayList = getImageURLS(status_id);
				
				if(urlArrayList.size() > 0){
					
					intent.putExtra("url_string", urlArrayList.get(0));
					// - Send
					// - This will be receiver by fragment_feed
					LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
				}
					
			}
		});
	}
	
	private Integer startRetrieveImageTask(
			ArrayList <String> urlArrayList, 
			ArrayList <ImageView> imageViewArrayList, 
			final String status_id, 
			int listPosition){
		
		Integer usedImageViewsInteger = 0;
		
		// - Loop thru all the strings
		for(int i = 0; i < urlArrayList.size(); i++){
			
			// - increment used imageviews integer
			usedImageViewsInteger++;
			
			// - Current image view
			final ImageView imageView = imageViewArrayList.get(i);
			
			// - Instance of current string
			String currentURL = urlArrayList.get(i);
			
			// - Create an retrieve image task
			ImageDownload imageDownload = new ImageDownload();
			imageDownload.downloadImage(currentURL, new ImageDownloadInterface() {
				
				@Override
				public void imageListFinished(ArrayList<Drawable> imageList) {
					// TODO Auto-generated method stub
					addDrawableForStatusID(status_id, imageList.get(0));
					imageView.setImageDrawable(imageList.get(0));
					setOnClickListenerForImageView(imageView, status_id);
				}
			});
		}
		
		return usedImageViewsInteger;
	}
	
	// ************************************************************ //
	// -------------------- SMALL HELPTER FUNCTIONS -------------- //
	// ********************************************************** //
	
	
	// ----------------------------------------------------------- //
	
	
	// ************************************************************ //
	// -------------------- PRIVATE CLASSES  --------------------- //
	// ********************************************************** //
		
	// - This extended class will download the image in background, and then update on main thread
	// - Will not be used
	private abstract class RetrieveImageTask extends DownloadImageTask{
		
		// - The imageView (here specifically the imageView to update)
		ImageView _handledImageView;
		
		public RetrieveImageTask(ImageView imageView, String url){
			super(url);
			_handledImageView = imageView;
		}
		
		@Override
		protected void onPostExecute(Drawable drawableImage){
			_handledImageView.setImageDrawable(drawableImage);
		}
	}
	
	// - Retrieve feed item image (extends RetrieveImageTask)
	// - So that it can save object images to memory
	private class RetrieveItemImageTask extends RetrieveImageTask{

		String status_id;
		
		// - Constructor
		public RetrieveItemImageTask(ImageView imageView, String url, String statusid) {
			
			super(imageView, url);
			status_id = statusid;
		}
		
		// - OnPostExecture
		@Override
		protected void onPostExecute(Drawable drawableImage) {
			super.onPostExecute(drawableImage);
			addDrawableForStatusID(status_id, drawableImage);
		}
	}
	
	// - Retrieve Profile image (extends RetrieveImageTask)
	// - So that it can save profile images to memory
	protected class RetrieveProfileImageTask extends RetrieveImageTask{

		String _user_id;
		
		public RetrieveProfileImageTask(ImageView imageView, String url, String user_id) {
			super(imageView, url);
			_user_id = user_id;
		}
		
		@Override
		protected void onPostExecute(Drawable drawableImage){
			super.onPostExecute(drawableImage);
			addDrawableForUserID(_user_id, drawableImage);
		}
		
	}
	
	
	/**
	 * Will send a broadcast to show the nice list for given status_id
	 * The broadcast will be recieved in the activity hosting any fragment (MainActivity)
	 * @param status_id
	 */
	private void broadcastNiceListWantsDisplay(String status_id){
		
		Intent intent = new Intent("broadcast_nice_list_tapped");
		intent.putExtra("status_id", status_id);
		
		LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
	}

	// - This extends getUserTask so we can handle the new User object (apiObject)
	// - which will update the allocated imageView with an image if that variable is set
	
	// - Get user
	private class GetUserTaskPrivate extends GetUserTask{
		
		ImageView _imageViewToUpdate; // - The imageView we want updated once we have finished
		// - downloading the user
		public void setWaitningImageView(ImageView imageView){
			_imageViewToUpdate = imageView;
		}
		
		@Override
		protected void onPostExecute(ApiObject result) {
	    	// Publish result
			//System.out.println("onPostExecute1: " + ApiObjectHandler.UserObject.getId(result));
			//System.out.println("onPostExecute2: " + ApiObjectHandler.UserObject.Images.getProfileImageHashMap(result));
			//System.out.println("onPostExecute3: " + ApiObjectHandler.UserObject.Images.getProfileOriginalImageString(result));
			
			
			// - Get the profile image string
			String profileImageString = ApiObjectHandler.Images.getProfileOriginalImageString(result);
			String user_id = ApiObjectHandler.UserObject.getId(result);
			
			
			// - if it exists we add it to the HashMap
			if(profileImageString != null && profileImageString.length() > 0){
				addProfileImageStringForUserId(profileImageString, user_id);
			
				// - Update the image on the imageView
				if(_imageViewToUpdate != null){
					RetrieveImageTask newTask = new RetrieveProfileImageTask(_imageViewToUpdate, profileImageString, user_id);
					newTask.execute();
				}
			}
			else{
				System.out.println("profileImageString in arrayAdapterApiObject is NULL");
				addProfileImageStringForUserId("0", user_id);
			}
	   }
	}

	
	// ************************************************************ //
	// -------------------- PRIVATE CLASSES  --------------------- //
	// ********************************************************** //
		
	/**
	 * Protected function to helpt with the basic setup and inflating of the view
	 * @param resource_id
	 * @param parent
	 * @param convertView
	 * @return
	 */
	protected View setupView(int resource_id, ViewGroup parent, View convertView){
		
		View view = convertView;
		
		// - If view is null
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(resource_id, parent, false);
		}		
		
		return view;
	}
		
	// ----------------------------------------------------------- //
	
	//
	// - HAS TO BE OVERRIDEN
	protected Context _context;
	public void setContext(Context context){
		_context = context;
	}
}
