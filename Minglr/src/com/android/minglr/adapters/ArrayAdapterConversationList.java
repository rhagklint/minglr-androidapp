package com.android.minglr.adapters;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.FlashDrive;
import com.example.android.navigationdrawerexample.MainActivity;
import com.example.android.navigationdrawerexample.R;

//
// - EXPLANATION
// - Inherits from the ArrayAdapterApiObject
// - So basically just override the getView method and the on click methods and so on
// - Used for displaying the fetched conversation list for the Fragment_ConversationList
public class ArrayAdapterConversationList extends ArrayAdapterApiObject {

	//
	// - Constructor
	public ArrayAdapterConversationList(
			Context context,
			int textViewResourceId, 
			ArrayList<ApiObject> items) 
	{
		
		//
		// - TODO -> save all the conversation id's
		
		
		//
		// - Super constructor will set this.context to the paramter (context)
		super(context, textViewResourceId, items);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View view = convertView;
		
		// - If view is null
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//view = inflater.inflate(R.layout.simple_list_item_1, null);
			view = inflater.inflate(android.R.layout.simple_list_item_1, null);
		}
		
		//
		// - Get the api object
		ApiObject item = getItem(position);
		if(item == null)
			return view;
		
		//
		// - Conversation id
		ArrayList<ApiObject> conversationMembers = ApiObjectHandler.ConversationObject.getMembers(item);
		String conversationName = appendConversationNames(conversationMembers);
		
		TextView itemView = (TextView)view.findViewById(android.R.id.text1);
		itemView.setText(conversationName);
		itemView.setFocusable(false);
		
		
		return view;
	}
	
	//
	// - Get the name of the conversation
	// - @params
	// - members: an array of members which should be appendend as the name of the conversation
	// - TODO -> leave out the logged in users name
	String appendConversationNames(ArrayList<ApiObject> members){

		//
		// - Where we append all the names of the conversation
		String appendedNames = "";
		
		//
		// - The stringBuilder which will append all the characters the appendedNames
		StringBuilder stringBuilder = new StringBuilder(appendedNames);
	
		//
		// - Loop thru all the members
		for (ApiObject member : members) {
			
			// - Member id
			String memberIDString = ApiObjectHandler.UserObject.getId(member);
			
			// - Check if the member id is equal to the logged in users id
			if(memberIDString.equals(FlashDrive.getInstance().getString("KEY_USER_ID"))){
				
				// - If it is - we continue the for loop
				continue;
			}

			// - Get the name from the JSONOBject
			String memberName = ApiObjectHandler.UserObject.getName(member);
			
			
			// - Append the memberName to the appendendNames variable
			stringBuilder.append(memberName);
			stringBuilder.append(", ");
			
		}
		
		// - Get the appendend string to appendedNames
		appendedNames = stringBuilder.toString();
		
		// - Return the appended names
		return appendedNames;
	}

}
