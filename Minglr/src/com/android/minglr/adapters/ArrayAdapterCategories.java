package com.android.minglr.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.minglr.adapters.ArrayAdapterApiObject.RetrieveProfileImageTask;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.CategoryHandler;
import com.android.minglr.models.ImageDownload;
import com.android.minglr.models.ImageDownloadInterface;
import com.example.android.navigationdrawerexample.R;

//
// - EXP
// - This class extends the ArrayAdapterImagedItem instead of the default ArrayAdapterApiObject
// - because it will work EXATLY the same, EXCEPT some variable names in the ApiObject being different
// - so this class will bridge the differences
public class ArrayAdapterCategories extends ArrayAdapterImagedItem {

	public ArrayAdapterCategories(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override 
	public View getView(int position, View convertView, ViewGroup parent){
		
		View view = convertView;
		
		if(view == null){
			
			// - Instance the inflater 
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			// - inflate the view
			view = inflater.inflate(R.layout.list_item_category, parent, false);
		}
		
		ApiObject item = getItem(position);
		if(item == null)
			return view;
		
		String name = ApiObjectHandler.LocationObject.getName(item);
		
		CheckBox checkBox = (CheckBox) view.findViewById(R.id.check_box_name);
		checkBox.setText(name);
		
		ImageView imageView = (ImageView) view.findViewById(R.id.category_image_view);
		handleProfileImage(item, imageView, position);
		
		// - First remove listener
		checkBox.setOnCheckedChangeListener(null);
		
		// - And set checked status for the category_id
		final String category_id = ApiObjectHandler.CategoryObject.getId(item);
		setCheckBoxStatusForCategoryID(checkBox, category_id);
		
		// - Then add on checked listener again
		checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				handleCheckedCategoryID(isChecked, category_id);
			}
		});
		
		
		//view.setBackgroundColor(CategoryHandler.getListItemColorForCategory(category_id, getContext()));
		
		return view;
	}
	
	protected void handleProfileImage(ApiObject item, final ImageView imageView, final int position){
		
		System.out.println("This: " + this.toString());
		
		// - Get location_id
		final String category_id = ApiObjectHandler.CategoryObject.getId(item);
		
		// - ImageView which should display the profile image
		ImageView profileView = imageView;
		
		// - If we have a picture for this location id already
		if(profileImageExists(category_id)){
			
			//
			// - Then we reuse that
			Drawable profileDrawable = getProfileImage(category_id);
			profileView.setImageDrawable(profileDrawable);
			System.out.println("Setting image drawable");
			
			return; // - RETURN
		}
		
		//
		// - Remove the current drawable because this is atm empty
		profileView.setImageDrawable(null);
		
		if(hasDownloadStartedForCategoryID(category_id)){
			return;
		}
		
		// - Get profile image string
		String profileImageString = ApiObjectHandler.Images.getCategoryImageString(item);
		
		if(profileImageString == null){
			return;
		}
		
		ImageDownload imageDownload = new ImageDownload();
		setDownloadStartedForCategoryID(category_id); // - Report that the download has started
		imageDownload.downloadImage(profileImageString, new ImageDownloadInterface() {
			
			@Override
			public void imageListFinished(ArrayList<Drawable> imageList) {
				//
				// - We know there is only one image
				Drawable imageDrawable = imageList.get(0); 
				System.out.println("ImageDrawable: " + imageDrawable.toString() + "for position: " + position);
				addDrawableForUserID(category_id, imageDrawable);
				
			}
		});
	}
	
	//
	// - Check box and related and lister handler	
	// ==================================================================================== //
	// ******************************** CHECK BOX ***************************************** //
	// ==================================================================================== //
	
	/**
	 * Sets the visible checked status for the checkBox depending on if the category_id
	 * is selected or not (in the CategoryHandler singleton)
	 * @param checkBox
	 * @param category_id
	 */
	private void setCheckBoxStatusForCategoryID(CheckBox checkBox, String category_id){
		if(CategoryHandler.isCategorySelected(category_id, _context))
			checkBox.setChecked(true);
		else
			checkBox.setChecked(false);
	}
	
	/**
	 * Handles the event when a checkbox is checked or unchecked. Called from the listener
	 * declared inline in getView(....)
	 * @param checked
	 * @param category_id
	 */
	private void handleCheckedCategoryID(Boolean checked, String category_id){
		//
		// - If the checkBox got checked and the category was not selected before
		if(checked && !CategoryHandler.isCategorySelected(category_id, _context)){
			CategoryHandler.setCategorySelected(category_id, _context);
		}
		//
		// - If the checkbox got unchecked and the category WAS sselected before
		else if(!checked && CategoryHandler.isCategorySelected(category_id, _context)){
			CategoryHandler.setCategoryUnselected(category_id, _context);
		}
	}
	
	// ==================================================================================== //
	// ******************************** CHECK BOX ***************************************** //
	// ==================================================================================== //
	
	// ==================================================================================== //
	// ********************************** DOWNLOADS *************************************** //
	// ==================================================================================== //
	/**
	 * Will contain the category_ids for which the image download has begun
	 */
	private HashMap<String, String> _downloadStartedForCategoryID = new HashMap<String, String>();
	
	private boolean hasDownloadStartedForCategoryID(String category_id){
		
		// - Will never happen since we assign and allocate it in class
		if(_downloadStartedForCategoryID == null){
			_downloadStartedForCategoryID = new HashMap<String, String>();
			return false;
		}
		
		else if(_downloadStartedForCategoryID.get(category_id) == null){
			return false;
		}
		
		else return true;
	}
	
	private void setDownloadStartedForCategoryID(String category_id){
		if(_downloadStartedForCategoryID == null){
			_downloadStartedForCategoryID = new HashMap<String, String>();
		}
		
		_downloadStartedForCategoryID.put(category_id, "started");
	}
	
	// ==================================================================================== //
	// ********************************** DOWNLOADS *************************************** //
	// ==================================================================================== //

}
