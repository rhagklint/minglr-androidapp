package com.android.minglr.adapters;

import java.util.ArrayList;

import android.R.integer;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.example.android.navigationdrawerexample.R;

public class ArrayAdapterComments extends ArrayAdapterApiObject {
	
	public ArrayAdapterComments(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}
	
	//
	// *************** OVERRIDES ***************** //
	@Override
	public int getViewTypeCount(){
		return 2;
	}

	@Override 
	public int getItemViewType(int position){
		if(position == 0){
			return 0;
		}
		else{
			return 1;
		}
	}
	// *************** OVERRIDES ***************** //
	//
	
	
	/**
	 * The standard getView for a item in a list
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		//
		// - the item for this given position
		ApiObject item = getItem(position);
		
		if(position == 0){
			return getFeedItemView(convertView, parent);
		}
		else{
			return getCommentView(convertView, parent, item);
		}
		
		//return getCommentView(convertView, parent, item);
	} 
	
	/**
	 * Will return a comment view for the list
	 * @param convertView, the view
	 * @param parent, container
	 * @param commentApiObject, the comment api object
	 * @return the comment view
	 */
	private View getCommentView(View convertView, ViewGroup parent, ApiObject commentApiObject){
		// - Take this view
		View view = convertView;
				
		// - If view is null
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//view = inflater.inflate(R.layout.simple_list_item_1, null);
			//view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
			view = inflater.inflate(R.layout.view_standard_content_list_item, parent, false);
		}
		
		//
		// - Get the api object
		System.out.println("Creating view in ArrayAdapterComments");
		ApiObject item = commentApiObject; //getItem(position);
		if(item == null)
			return view;
		
		String name = ApiObjectHandler.UserObject.getName(item);
		String text = ApiObjectHandler.FeedObject.getContentMessage(item);
		
		//TextView nameView = (TextView) view.findViewById(android.R.id.text1);
		//TextView detailView = (TextView) view.findViewById(android.R.id.text2);
		TextView nameView = (TextView)view.findViewById(R.id.user_name_label);
		TextView detailView = (TextView)view.findViewById(R.id.content_text_view);
		
		nameView.setText(name);
		detailView.setText(text);
		
		handleProfileImage(item, view, R.id.profile_image_view);
		
		System.out.println("Created view in ArrayAdapterComments");
		
		return view;		
	}
	
	/**
	 * This will return a view similar or equal to that of the feed, ergo
	 * it will look like the original you clicked "comment" on
	 * @param convertView, the view (may be null)
	 * @param parent, container
	 * @return usable view
	 */
	private View getFeedItemView(View convertView, ViewGroup parent){
		
		// - Assign
		View view = super.getView(0, convertView, parent);
		
		Button commentButton = (Button) view.findViewById(R.id.commentButton);
		commentButton.setVisibility(View.INVISIBLE);
		
		return view;
	}

	@Override
	protected void handleProfileImage(ApiObject object, View superView, int image_view_id){
		super.handleProfileImage(object, superView, image_view_id);
	}
}
