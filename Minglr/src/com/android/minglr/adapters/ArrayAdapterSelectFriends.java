package com.android.minglr.adapters;

import java.util.ArrayList;

import android.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;

public class ArrayAdapterSelectFriends extends ArrayAdapterApiObject {

	public ArrayAdapterSelectFriends(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View view = convertView;
		
		// - If view is null
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(android.R.layout.simple_list_item_1, null);
		}
		
		//
		// - Get the api object
		ApiObject item = getItem(position);
		if(item == null)
			return view;
		
		String name = ApiObjectHandler.UserObject.getName(item);
		TextView nameView = (TextView)view.findViewById(R.id.text1);
		nameView.setText(name);
		
		return view;
	}

}
