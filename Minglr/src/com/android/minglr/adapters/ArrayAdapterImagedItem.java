package com.android.minglr.adapters;

import java.util.ArrayList;

import android.R.integer;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.example.android.navigationdrawerexample.R;

public class ArrayAdapterImagedItem extends ArrayAdapterApiObject {

	public ArrayAdapterImagedItem(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		View view = convertView;
		
		if(view == null){
			
			// - Instance the inflater 
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			// - inflate the view
			view = inflater.inflate(R.layout.list_item_image, parent, false);
		}
		
		ApiObject item = getItem(position);
		if(item == null)
			return view;
		
		String name = ApiObjectHandler.LocationObject.getName(item);
		
		TextView textView = (TextView) view.findViewById(R.id.user_name_label);
		textView.setText(name);
		
		handleProfileImage(item, view, R.id.profile_image_view);
		
		return view;
	}
	
	@Override
	protected void handleProfileImage(ApiObject item, View view, int profile_id){
		
		// - Get location_id
		String location_id = ApiObjectHandler.LocationObject.getId(item);
		
		// - ImageView which should display the profile image
		ImageView profileView = (ImageView) view.findViewById(profile_id);
		
		// - If we have a picture for this location id already
		if(profileImageExists(location_id)){
			
			// - Then we reuse that
			Drawable profileDrawable = getProfileImage(location_id);
			profileView.setImageDrawable(profileDrawable);
			
			return; // - RETURN
		}
		
		//
		// - Remove the current drawable because this is atm empty
		profileView.setImageDrawable(null);
		
		// - Get profile image string
		String profileImageString = ApiObjectHandler.Images.getProfileOriginalImageString(item);
		
		if(profileImageString == null){
			return;
		}
		
		RetrieveProfileImageTask task = new RetrieveProfileImageTask(profileView, profileImageString, location_id);
		task.execute();
		
	}
	
}
