package com.android.minglr.adapters;

import java.util.ArrayList;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.example.android.navigationdrawerexample.R;

public class ArrayAdapterNotifications extends ArrayAdapterApiObject {

	public ArrayAdapterNotifications(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		
		
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}

	//
	// - GetView
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		// - Set view with resource id
		View view = setupView(android.R.layout.simple_list_item_2, parent, convertView);
		
		ApiObject item = getItem(position);
		
		if(item == null)
			return view;
				
		
		//
		// - set the text for the notification
		String text = item.getObject().get("text");
		TextView textView = (TextView)view.findViewById(android.R.id.text1);
		textView.setTextSize(12);
		textView.setText(text);
		
		//ApiObject metaData = ApiObjectHandler.refineObject(item, "metadata");
		//System.out.println("MetaData: " + metaData.getObject().toString());
		
		return view;
	}
}
