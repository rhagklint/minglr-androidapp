package com.android.minglr.adapters;

import java.util.ArrayList;

import android.R.color;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiObjectHeader;

public class ArrayAdapterFriendList extends ArrayAdapterApiObject {

	public ArrayAdapterFriendList(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}
	
	//
	// - GetView
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		// - Take this view
		View view = convertView;
		
		//
		// - Get the api object
		ApiObject item = getItem(position);
		
		//
		// - If this is header
		if(item != null){
			if(item instanceof ApiObjectHeader){
				view = getHeaderView(position, convertView, parent, (ApiObjectHeader)item);
				return view;
			}
		}
		
		// - If view is null
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//view = inflater.inflate(R.layout.simple_list_item_1, null);
			view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
		}
		
		
		if(item == null)
			return view;
		
		String text = ApiObjectHandler.UserObject.getName(item);
		
		TextView textView = (TextView) view.findViewById(android.R.id.text1);
		textView.setTextSize(16);
		textView.setTextColor(Color.BLACK);
		textView.setBackgroundColor(Color.TRANSPARENT);
		
		textView.setText(text);
		
		return view;
	}
	
	public View getHeaderView(int position, View convertView, ViewGroup parent, ApiObjectHeader header){
		
		View view = convertView;
		
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//view = inflater.inflate(R.layout.simple_list_item_1, null);
			view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
		}
		
		TextView textView = (TextView) view.findViewById(android.R.id.text1);
		textView.setTextSize(12);
		textView.setBackgroundColor(Color.GRAY);
		textView.setTextColor(Color.WHITE);
		textView.setText(header.headerText);
		
		return view;
	}
}
