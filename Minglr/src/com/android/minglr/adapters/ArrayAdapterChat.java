package com.android.minglr.adapters;

import java.util.ArrayList;

import android.R;
import android.R.color;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;

public class ArrayAdapterChat extends ArrayAdapterApiObject {

	//
	// - Constructor
	public ArrayAdapterChat(
			Context context,
			int textViewResourceId, 
			ArrayList<ApiObject> items) 
	{
		//
		// - Super constructor will set this.context to the paramter (context)
		super(context, textViewResourceId, items);
	}
	
	//
	// - GetView
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		// - Take this view
		View view = convertView;
		
		// - If view is null
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//view = inflater.inflate(R.layout.simple_list_item_1, null);
			view = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
		}
		
		//
		// - Get the api object
		ApiObject item = getItem(position);
		if(item == null)
			return view;
		
		
		String name = ApiObjectHandler.UserObject.getName(item);
		String text = ApiObjectHandler.FeedObject.getContentMessage(item);
		
		TextView textView = (TextView) view.findViewById(android.R.id.text1);
		textView.setTextColor(Color.BLACK);
		
		TextView detailView = (TextView) view.findViewById(android.R.id.text2);
		detailView.setTextColor(Color.BLACK);
		
		textView.setText(name);
		detailView.setText(text);
		
		return view;
	}

}
