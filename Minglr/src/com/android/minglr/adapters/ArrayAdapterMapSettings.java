package com.android.minglr.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.MinglrConfig;
import com.android.minglr.models.MinglrConfig.kConfigSetting;
import com.example.android.navigationdrawerexample.R;



public class ArrayAdapterMapSettings extends ArrayAdapterApiObject {
	
	
	public ArrayAdapterMapSettings(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		//
		// - Get the api object
		ApiObject item = getItem(position);
		if(item == null)
			return convertView;
		
		String name = ApiObjectHandler.UserObject.getName(item);
		View view;
		if(item.getObject().get("type").equals("check_box")){
			view = getCheckBoxView(position, convertView, parent, name);
		}
		else 
			view = getArrowView(position, convertView, parent, name);
		
		
		//TextView textView = (TextView) view.findViewById(android.R.id.text1);
		
		//textView.setText(name);
		
		return view;
	}
	
	/**
	 * Returns a standard plain views. In this particular ArrayAdapter they will have an arrow as
	 * accessory, and will lead to a new intent when clicked
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 */
	private View getArrowView(int position, View convertView, ViewGroup parent, String text){
		
		View view  = convertView;
		
		// - If view is null
		/*
		if(view == null){
			
		}
		*/
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.map_settings_cell_item, parent, false);
		
		TextView textView = (TextView)view.findViewById(R.id.text_view1);
		//textView.setTextSize(12);
		textView.setText(text);
		
		return view;
	}
	
	/**
	 * Will return a cell view with a check box. Selecting a particular check_box will
	 * in this case represent making a settings change
	 * @param position
	 * @param convertView
	 * @param parent
	 * @return
	 */
	private View getCheckBoxView(int position, View convertView, ViewGroup parent, String text){
		
		View view = convertView;
		
		// - Inflate if view is null
		/*
		if(view == null){
			
		}
		*/
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.checkin_cell_item, parent, false);
		
		TextView textView = (TextView)view.findViewById(R.id.text_view);
		textView.setText(text);
		
		CheckBox checkBox = (CheckBox)view.findViewById(R.id.check_box);
		checkBox.setText("");
		
		checkBox.setTag(position);
		
		//
		// - Set the checkbox isChecked status
		handleSettingForCheckBox(checkBox, position);
		
		
		//
		// - Set checkbox listener
		checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				handleCheckboxChecked(isChecked, Integer.parseInt(buttonView.getTag().toString()));
			}
		});
		
		return view;
	}
	
	/**
	 * Will update the logic for the different settings
	 * @param checked
	 * @param tag
	 */
	private void handleCheckboxChecked(boolean checked, int tag){
		//
		// - OBS. Cant seem to create a switch/case with Setting.SHOW_PEOPLE.ordinal()
		// - since it's not a constant. That means we have to make if statements instead
		
		// - Decide which setting was checked
		if(tag == kConfigSetting.SHOW_PEOPLE.getValue()){
			MinglrConfig.getInstance().setSettingOn(kConfigSetting.SHOW_PEOPLE, checked);
		}
		else if(tag == kConfigSetting.SHOW_HEATMAP.getValue()){
			MinglrConfig.getInstance().setSettingOn(kConfigSetting.SHOW_HEATMAP, checked);
		}
		else if(tag == kConfigSetting.SHOW_OPEN.getValue()){
			MinglrConfig.getInstance().setSettingOn(kConfigSetting.SHOW_OPEN, checked);
		}
		else if(tag == kConfigSetting.GEO_FRIENDS.getValue()){
			MinglrConfig.getInstance().setSettingOn(kConfigSetting.GEO_FRIENDS, checked);
		}
		
	}
	
	private void handleSettingForCheckBox(CheckBox checkBox, int position){
		
		// - Show all
		if(position == 0){
			checkBox.setChecked(MinglrConfig.getInstance().isShowAllOn());
		}
		// - Show heatmap
		else if(position == 1){
			checkBox.setChecked(MinglrConfig.getInstance().isShowHeatMapOn());
		}
		// - Show only open
		else if(position == 2){
			checkBox.setChecked(MinglrConfig.getInstance().isShowOpenOnlyOn());
		}
		else if(position == 3){
			// - Nothing, this is for categories, we will never arrive here
		}
		// - Geofriends
		else if(position == 4){
			checkBox.setChecked(MinglrConfig.getInstance().isGeoFriendsOn());
		}
		
	}

}
