package com.android.minglr.adapters;

import java.util.ArrayList;

import android.R.integer;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ImageDownload;
import com.android.minglr.models.ImageDownloadInterface;
import com.example.android.navigationdrawerexample.R;
import com.google.android.gms.plus.model.people.Person.Image;

public class ArrayAdapterUserProfile extends ArrayAdapterApiObject {
	
	Drawable _profileImage;
	ApiObject _userObject;
	
	public void setUserObject(ApiObject userObject){
		_userObject = userObject;
	}

	public ArrayAdapterUserProfile(Context context, int textViewResourceId,
			ArrayList<ApiObject> items) {
		super(context, textViewResourceId, items);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		//
		// - First we get the item
		ApiObject currentItem = getItem(position);
		
		String testString = currentItem.getObject().get("birthday");
		if(testString == null){
			return super.getView(position, convertView, parent);
		}
		else{
			return profileView(convertView, currentItem, parent);
		}
	}
	
	private View profileView(View convertView, ApiObject apiObject, ViewGroup parent){
		
		View view = convertView;
		
		if(view == null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//view = inflater.inflate(R.layout.simple_list_item_1, null);
			view = inflater.inflate(R.layout.view_profile_item, parent, false);
		}
		
		//
		// _ Set name
		TextView userNameLabelTextView = (TextView) view.findViewById(R.id.user_name_label);
		
		// - Handle profile image
		ImageView profileImageView = (ImageView) view.findViewById(R.id.profile_image_view);
		setProfileImage(apiObject, profileImageView);
		
		//
		// - Set profile info
		if(_userObject != null)
			setProfileInfo(_userObject, view);
		
		// - TODO
		// - Set the variables
		String nameString = ApiObjectHandler.UserObject.getName(apiObject);
		userNameLabelTextView.setText(nameString);
		
		return view;
	}
	
	private void setProfileImage(ApiObject userObject, ImageView imageView){
		
		//
		// - If we have the _profileImageView
		// - we set it directly and return
		if(_profileImage!= null){
			imageView.setImageDrawable(_profileImage);
			return;
		}
		//
		// - Get the url string
		String urlString = ApiObjectHandler.Images.getProfileOriginalImageString(userObject);
		System.out.println("URLSTRING: " + urlString);
		if(urlString == null){
			imageView.setImageResource(R.drawable.pin_base_small);
			return;
		}
		
		final ImageView updateView = imageView;
		
		//
		// - Else we have to download the drawable image
		ImageDownload imageDownload = new ImageDownload();
		imageDownload.downloadImage(urlString, new ImageDownloadInterface() {
			@Override
			public void imageListFinished(ArrayList<Drawable> imageList) {
				
				// TODO Auto-generated method stub
				System.out.println("ImageList count: " + imageList.size());
				
				if(imageList.size() > 0){
					_profileImage = imageList.get(0);
					updateView.setImageDrawable(imageList.get(0));
				}
				else{
					updateView.setImageResource(R.drawable.jupiter);
				}
			}
		});
	}
	
	private void setProfileInfo(ApiObject userObject, View view){
		//
		// - Assign variables from R.id
		TextView informationLabel = (TextView) view.findViewById(R.id.information_label);
		TextView ageLabel = (TextView) view.findViewById(R.id.age_label);
		TextView genderLabel = (TextView) view.findViewById(R.id.gender_label);
		TextView separatorLabel = (TextView) view.findViewById(R.id.separator_label);
		
		//
		// - Set text
		informationLabel.setText("INFORMATION");
		
		//
		// - Set age
		// - TODO
		String birthDay = ApiObjectHandler.UserObject.getBirthday(userObject);
		ageLabel.setText("Birthday: " + birthDay);
		
		//
		// - Set gender
		// - TODO
		String gender = ApiObjectHandler.UserObject.getGender(userObject);
		genderLabel.setText("Gender: " + gender);
		
		//
		// _ Seperator text
		separatorLabel.setText("TIMELIN");
	}
	
	//
	// *************** OVERRIDES ***************** //
	@Override
	public int getViewTypeCount(){
		return 2;
	}

	@Override 
	public int getItemViewType(int position){
		if(position == 0){
			return 0;
		}
		else{
			return 1;
		}
	}
	// *************** OVERRIDES ***************** //
	//
}
