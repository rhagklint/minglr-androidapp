package com.android.minglr.adapters;

import java.util.ArrayList;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.LocationCheckInHandler;
import com.example.android.navigationdrawerexample.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

public class ArrayAdapterCheckInList extends ArrayAdapterApiObject {
	
	//
	// - We have to save all the checkBoxes in a list, so that we can
	// - uncheck them whenever a new one is checked
	private ArrayList <CheckBox> _checkBoxList;
	
	//
	// - Constructor
	public ArrayAdapterCheckInList(
			Context context,
			int textViewResourceId, 
			ArrayList<ApiObject> items) 
	{
		
		//
		// - TODO -> save all the location ids id's
		
		
		//
		// - Super constructor will set this.context to the paramter (context)
		super(context, textViewResourceId, items);
	}
	
	
	//
	// - GetView
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		
		// - Take this view
		View view = setupView(R.layout.checkin_cell_item, parent, convertView);//convertView;
		
		// - Get the api object
		ApiObject item = getItem(position);
		if(item == null)
			return view;
		
		
		String name = ApiObjectHandler.LocationObject.getName(item);
		
		CheckBox checkBox = (CheckBox)view.findViewById(R.id.check_box);
		TextView textView = (TextView)view.findViewById(R.id.text_view);
		textView.setText(name);
		checkBox.setText("");
		checkBox.setTag(ApiObjectHandler.LocationObject.getId(item));
		
		if(_checkBoxList == null){
			_checkBoxList = new ArrayList<CheckBox>();
		}
		
		_checkBoxList.add(checkBox);
		
		//
		// - Since cells are most probably tiled, and reused, we want to remove the 
		// - on check listener, before we decide and set the checkBox checked or not,
		// - depending on check-in status on current location 
		checkBox.setOnCheckedChangeListener(null);
		
		// - Now we see if we are checked in here
		if(LocationCheckInHandler.getInstance().isCheckedInto(ApiObjectHandler.LocationObject.getId(item))){
			checkBox.setChecked(true);
		}
		else checkBox.setChecked(false);
		
		// - We have to do this, or else the onItemClick listener won't respond 
		// - in the list fragment
		checkBox.setFocusableInTouchMode(false);
		checkBox.setFocusable(false);
		
		//
		// - On check listener
		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				System.out.println("CheckBox with tag " + buttonView.getTag().toString() + "is checked: " + isChecked);
				
				if(isChecked == true){
					
					// - Update the other checkboxes
					checkBoxHasBeenChecked(buttonView.getTag().toString());
					
					// - Post checkin
					LocationCheckInHandler.getInstance().postCheckinToLocationId(buttonView.getTag().toString(),null);
					
				}
			}
		});
		
		return view;
	}
	
	/**
	 * This function is called whenever a check box has been checked to true
	 * It will uncheck all the checkboxes which doesn not have the same tag as 
	 * the checked checkbox
	 * @param tag
	 */
	private void checkBoxHasBeenChecked(String tag){
		for(CheckBox checkBox : _checkBoxList){
			if(!checkBox.getTag().toString().equals(tag)){
				checkBox.setChecked(false);
			}
		}
	}
}
