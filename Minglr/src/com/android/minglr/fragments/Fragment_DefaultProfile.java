package com.android.minglr.fragments;

import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;

import android.app.Fragment;

public class Fragment_DefaultProfile extends Fragment {
	
	//
	// - ENUM to set which type of profile this is
	public enum kProfileObjectType {
		kProfileObjectTypeUser, 
		kProfileObjectTypeLocation 
	}
	
	// - Constructor
	public Fragment_DefaultProfile() {
		// TODO Auto-generated constructor stub
	}

	//
	// - Class instance variables
	// - These are variables shared by all the profile views (either a user och a location)
	
	// - The api object on display  (either a location or user)
	protected ApiObject _displayedObject;
	public void setDisplayedObject(ApiObject displayedObject){ _displayedObject = displayedObject; }
	
	// - Type of profile object (this will help in getName and getID functions)
	protected kProfileObjectType _objectType;
	public void setProfileObjectType(kProfileObjectType type){ _objectType = type; }
	
	/***************** HELPER FUNCTION TO REDUCE AMOUNT OF CODE IN SUBCLASSES *************/
	//
	// - Get name of the displayed object
	protected String getObjectName(){
		if(_displayedObject == null){
			return "";
		}
		
		if(_objectType == kProfileObjectType.kProfileObjectTypeLocation){
			return ApiObjectHandler.LocationObject.getName(_displayedObject);
		}
		else return ApiObjectHandler.UserObject.getName(_displayedObject);
	}
	
	// - Get id of displayed object
	protected String getObjectID(){
		if(_displayedObject == null)
			return "";
		
		if(_objectType == kProfileObjectType.kProfileObjectTypeLocation){
			return ApiObjectHandler.LocationObject.getId(_displayedObject);
		}
		else return ApiObjectHandler.UserObject.getId(_displayedObject);
	}
	
	//
	/***************** HELPER FUNCTION TO REDUCE AMOUNT OF CODE IN SUBCLASSES *************/

}
