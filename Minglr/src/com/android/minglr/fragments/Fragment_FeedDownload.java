package com.android.minglr.fragments;

import java.util.ArrayList;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.EndlessScrollInterface;
import com.android.minglr.models.EndlessScrollListener;
import com.example.android.navigationdrawerexample.R;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

/**
 * This is the first class in the Fragment_Feed hierchy, it will handle all the downloading and
 * updating of the listView
 * @author Robert
 *
 */
public class Fragment_FeedDownload extends ListFragment {
	
	protected String _localURL;
	
	// - A string which contains the id of the last_id (the id for the post furthest down in
	// - the list. Is used upon pagination
	protected String _lastPostID;
	
	//
	// - A boolean which will be set to true when the app is downloading the feed
	// - And set to false when that data is downloaded
	// - UPDATE; not used for anything tho?
	protected boolean _isLoading;
	
	//
	// - Contains the data
	protected ArrayList<ApiObject> _data = new ArrayList<ApiObject>();
	protected ArrayList <ApiObject> _feed = new ArrayList <ApiObject>();	

	public Fragment_FeedDownload() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onViewCreated(View view, Bundle bundle){
		
		super.onViewCreated(view, bundle);
		
		//
		// - Add the on scroll listener to the listView
		// - and create a new interface
		getListView().setOnScrollListener(
				new EndlessScrollListener(
						new EndlessScrollInterface() {
			
			@Override
			public void doPaginate() {
				updateFeed(_lastPostID);
			}
			
			@Override
			public boolean isLoading(){
				return _isLoading;
			}
		}));
		
	}
	
	// =============================================================================================== //
	// *********************************** DOWNLOAD FEED ********************************************* //
	// =============================================================================================== //
	/**
	 * Downloads a need feed for the user, IF lastPostID is not null, we
	 * append the request with that info
	 * @param lastPostID
	 */
	protected void downloadFeed(final String lastPostID){
		//
		// - Flag _isLoading
		_isLoading = true;
		
		//
		// - Close keyboard
		//CloseKeyBoard(getActivity());
		
		//
		// - Set url
		String url = "feed/userwall?limit=10";
		
		// - If we have a lastPostID, we append it to the url string
		if(lastPostID != null && lastPostID.length() > 0){
			url = url + "&until_id=" + lastPostID;
		}
		
		ApiDownload apiDownload = new ApiDownload();
		apiDownload.setArrayVariable("feed");
		
		apiDownload.startDownload(url, "feed", new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
				// - IF we have lastPostID as a parameter, we want
				// - to updateListView, since it most likely means that the user
				// - has scrolled down
				if(lastPostID != null){
					updateListView(result);
				}
				
				// - If we on the other hand have no lastPostID
				// - it means we want a complete refresh
				// - so we use that function instead
				else{
					updateListView(result);//refreshListView(result);
				}
			}
		});
	}
	
	/**
	 * Ignores lastPostId and donwloads the feed fresh
	 */
	protected void refreshFeed(){
		downloadFeed(null);
	}
    
	/**
	 * Updates feed with lastPostID, this is basically for pagination
	 * @param lastPostID lastPostID of the last batch of data
	 */
	protected void updateFeed(String lastPostID){
		downloadFeed(lastPostID);
	}
	// =============================================================================================== //
	// *********************************** DOWNLOAD FEED ********************************************* //
	// =============================================================================================== //
	
	//
	// - Will contain functions such as updateListView which updates the data to the arrayAdapter
	// - and also updates the UI
	// =============================================================================================== //
	// ********************************** UI AND DATA UPDATE ***************************************** //
	// =============================================================================================== //
	protected LayoutInflater _inflater;
	/**
	 * Update listView
	 * @param result
	 */
	protected void updateListView(ArrayList<ApiObject> result){
		
		if(_feed == null){
			_feed = new ArrayList<ApiObject>();
		}
		
		if(result.size() == 0)
			return;
		
		ApiObject lastObject = result.get(result.size() - 1);
		_lastPostID = ApiObjectHandler.FeedObject.getId(lastObject);
		
		_feed.addAll(result);

		if(getListAdapter() == null){
			// - TODO This is reduntant code, this already exists in refreshListViewController
			ArrayList <ApiObject> tempList = new ArrayList<ApiObject>();
			tempList.addAll(_feed);
			
			// - Create the adapter
			ArrayAdapterApiObject adapter = new ArrayAdapterApiObject (
					_inflater.getContext(),  
					android.R.layout.simple_expandable_list_item_1,
					tempList);
			
			
			// - Set listAdapter
			this.setListAdapter(adapter); // - Set listAdapter
		}
		else{
			final ArrayAdapterApiObject adapter = (ArrayAdapterApiObject) getListAdapter();
			
			adapter.clear();
			
			if(_feed != null){
				
				for(ApiObject object : _feed){
					adapter.insert(object, adapter.getCount());
				}
			}
			
			Runnable updateThread = new Runnable() {
				@Override
				public void run() {
					System.out.println("Fragment_Feed: _feed.size(): " + _feed.size());
					System.out.println("Fragment_Feed: updateListView() - adapter.notifyDataSetChanged");
					adapter.notifyDataSetChanged();
				}
			};
			
			updateThread.run();
		}
		
		_data = _feed;
		
		//
		// - Flag isLoading
		_isLoading = false;
	}	
	
	/**
	 * Instead of just updating the list view, we refresh the entire thing
	 * guaranteeing a total restart of the adapter
	 * @param result
	 */
	protected void refreshListView(ArrayList <ApiObject> result){
		
		if(_feed == null){
			_feed = new ArrayList<ApiObject>();
		}
		
		if(result.size() == 0)
			return;
		
		// - Set _lastPostId that we need when 
		// - the user scrolls down
		ApiObject lastObject = result.get(result.size() - 1);
		_lastPostID = ApiObjectHandler.FeedObject.getId(lastObject);
		
		// - ArrayList
		ArrayList <ApiObject> tempList = new ArrayList<ApiObject>();
		tempList.addAll(_feed);
		
		// - Create the adapter
		ArrayAdapterApiObject adapter = new ArrayAdapterApiObject (
				_inflater.getContext(),  
				android.R.layout.simple_expandable_list_item_1,
				tempList);
		
		
		// - Set listAdapter
		this.setListAdapter(adapter); 
		
		_data = _feed;
		
		//
		// - Flag isLoading
		_isLoading = false;
	}
	
	// =============================================================================================== //
	// ********************************** UI AND DATA UPDATE ***************************************** //
	// =============================================================================================== //

}
