package com.android.minglr.fragments;

import java.io.FileNotFoundException;
import java.util.ArrayList;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.input.InputManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Layout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.android.minglr.models.EndlessScrollInterface;
import com.android.minglr.models.EndlessScrollListener;
import com.android.minglr.tasks.SimpleJsonFetcher;
import com.example.android.navigationdrawerexample.ActivityDefaultProfile;
import com.example.android.navigationdrawerexample.CommentActivity;
import com.example.android.navigationdrawerexample.ImageViewActivity;
import com.example.android.navigationdrawerexample.MainActivity;
import com.example.android.navigationdrawerexample.R;
import com.android.minglr.views.View_StatusUpdate;


public class Fragment_Feed extends Fragment_FeedImagePicker{
	
	// - Constructor
	public Fragment_Feed() {
	}
	
	
	// ================================================================================================== //
	// ********************************** LIFECYCLE OVERRIDE ******************************************** //
	// ================================================================================================== //
	
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		// - Save the inflater
		_inflater = inflater;
		
		// - Start the download for the feed
		refreshFeed();
		
		//
		// - Setup the broadcast recievers for nice, comment, image and name tapped
		setupBroadcastReceivers();
		
		super.onCreateView(inflater, container, savedStateInstance);
		
		View view = _inflater.inflate(R.layout.fragment_main_feed, container, false);
		
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstance){
		System.out.println("OnViewCreated");
		//showStatusUpdateView(view);
		super.onViewCreated(view, savedInstance);
		
		
		//
		// - Add listener to the image button
		Button imageButton = (Button) getView().findViewById(R.id.image_pick_button);
		imageButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				openImagePicker();
			}
		});
	
		// - Set the layout which carries any images the user wants to upload to hidden
		final LinearLayout imageLayout = (LinearLayout)getView().findViewById(R.id.image_layout);
		imageLayout.setVisibility(View.GONE);
		
		CloseKeyBoard(getActivity());
		
		// - Set background image for the listView
		getListView().setBackgroundResource(R.drawable.clean_background);
		
	}
	
	@Override 
	protected void downloadFeed(String lastPostID){
		super.downloadFeed(lastPostID);
		CloseKeyBoard(getActivity());
	}
	
	
	@Override
	public void onDestroyView(){
		
		// - Super
		super.onDestroyView();
		
		// - Remove as listener/receiver from the Broadcaster
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mTappedCommentReceiver);
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mTappedNiceReceiver);
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mStatusUpdateReceiver);
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mTappedNameReciever);
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mTappedImageReceiver);
	}
	
	
	// ================================================================================================== //
	// ********************************** LIFECYCLE OVERRIDE ******************************************** //
	// ================================================================================================== //
	
	
	private void setupBroadcastReceivers(){
		LocalBroadcastManager.getInstance(
				getActivity()).registerReceiver(
						mTappedCommentReceiver, 
						new IntentFilter("broadcast_comment_tapped")
						);
		
		// - When the nice button is tapped in the ArrayAdapter
		LocalBroadcastManager.getInstance(
				getActivity()).registerReceiver(
						mTappedNiceReceiver, 
						new IntentFilter("broadcast_nice_tapped")
						);
		
		
		// - When a name is tapped in the ArrayAdapter
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mTappedNameReciever, 
				new IntentFilter("broadcast_name_tapped")
				);
		
		// - When an image is tapped in the ArrayAdapter
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mTappedImageReceiver, 
				new IntentFilter("broadcast_image_tapped")
				);
	}
	
	// ================================================================================================== //
	// ********************************** BROADCAST RECEIVER ******************************************** //
	// ================================================================================================== //
	
	// - Will receive message when the nice and comment button is clicked on any item/row
	// - If we move the _data variable we can move this to another class/location
	protected BroadcastReceiver mTappedCommentReceiver = new BroadcastReceiver(){
		
		//
		// - This member will handle messages sent from the ArrayAdapterApiObject
		// - when the user taps the comment button
		
		@Override
		public void onReceive(Context context, Intent intent) {

			int messageInt = intent.getIntExtra("message", 0);
			
			System.out.println("Received message from index: " + messageInt);
			
			// - Get the data for this index
			ApiObject containedObject = _data.get(messageInt);
			String contentMessage = ApiObjectHandler.FeedObject.getContentMessage(containedObject);
			String status_id = ApiObjectHandler.FeedObject.getId(containedObject);
			
			Intent commentsIntent = new Intent(getActivity(), CommentActivity.class);
			commentsIntent.putExtra("status_id", status_id);
			
			startActivity(commentsIntent);
			
			System.out.println("Contained content: " + contentMessage + " with status_id: " + status_id);
		}
		
	};
	
	protected BroadcastReceiver mTappedNiceReceiver = new BroadcastReceiver(){
		
		//
		// - This member will handle messages sent from the ArrayAdapterApiObject
		// - when the user taps the nice button
		
		@Override
		public void onReceive(Context context, Intent intent) {

			final int messageInt = intent.getIntExtra("message", 0);
			System.out.println("Received message from index: " + messageInt);
			
			// - Get the object for the row
			ApiObject object = _data.get(messageInt);
			String status_id = ApiObjectHandler.FeedObject.getId(object);
			
			
			ApiPost apiPost = new ApiPost();
			apiPost.addParameter("status_id", status_id);
			apiPost.startPostDataTo("feed/add_status_nice", new ApiPostInterface() {
				
				@Override
				public void postDataComplete(String response) {
					// TODO Auto-generated method stub
					ArrayAdapterApiObject adapter = (ArrayAdapterApiObject) getListAdapter();
					adapter.updateRow(messageInt, getListView());
				}
			});
		}
		
	};

	protected BroadcastReceiver mTappedNameReciever = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			//
			// - user_id
			String user_id = intent.getExtras().getString("user_id");
			System.out.println("Starting intent user_id: " + user_id);
			
			// - Create a new intent
			Intent profileIntent = new Intent(getActivity(), ActivityDefaultProfile.class);
			profileIntent.putExtra("user_id", user_id);

			startActivity(profileIntent);
			
		}
	};
	
	protected BroadcastReceiver mTappedImageReceiver = new BroadcastReceiver(){
		
		@Override 
		public void onReceive(Context context, Intent intent){
			
			String urlString = intent.getExtras().getString("url_string");
			
			Intent imageViewIntent = new Intent(getActivity(), ImageViewActivity.class);
			imageViewIntent.putExtra("url_string", urlString);
			
			startActivity(imageViewIntent);
		}
	};
	
	// ================================================================================================== //
	// ********************************** BROADCAST RECEIVER ******************************************** //
	// ================================================================================================== //
	
	
	
	
	
}
