package com.android.minglr.fragments;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.adapters.ArrayAdapterConversationList;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.android.minglr.models.ConversationCreator;
import com.android.minglr.models.FlashDrive;
import com.android.minglr.tasks.SimpleJsonFetcher;
import com.example.android.navigationdrawerexample.ActivitySelectFriends;
import com.example.android.navigationdrawerexample.ChatActivity;
import com.example.android.navigationdrawerexample.R;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * This class will hold all the conversations currently available to the user
 * It will also have an option to create a new conversation. Thats why we check if there
 * has been a new conversation created on the onResume() function call. 
 * @author Robert
 *
 */
public class Fragment_ConversationList extends Fragment_DefaultList {
		
	public Fragment_ConversationList() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance)
	{
		
		// - Inflater is instanced by the superclass
		_inflater = inflater;
		
		//
		// - Url for the request
		setURLString("http://api-test.minglr.se/conversation/list");
		setArrayVariable("conversations");
		
		//
		// - Start get data task
		startGetDataTask();
		
		// - Return the rootView
		View rootView = inflater.inflate(R.layout.fragment_conversation_list, container, false);
		return rootView;
		//return super.onCreateView(inflater, container, savedStateInstance);
	}
	
	@Override 
	public void onViewCreated(View view, Bundle bundle){
		
		//
		// - Add listener to the create new button
		Button createNewButton = (Button)view.findViewById(R.id.button_create_new);
		createNewButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				clickedCreateNewButton();
			}
		});
		
		createNewButton.setText("Create new chat");
		super.onViewCreated(view, bundle);
	}
	
	@Override
	public void onResume(){
		System.out.println("On Resume");
		
		//
		// - If this is true, we have a new conversation
		if(ConversationCreator.getInstance().hasNewConversationMembers()){
			createNewConversation(ConversationCreator.getInstance().retrieveFriendsForNewConversation());
		}
		
		super.onResume();
	}
	
	//
	// - Update the listView
	// - This will be called from the superclass's GetDataTask::onPostExecute and this function
	// - will then send a call to perform the same function in the superclass to complete the
	// - update of the listView
	@Override
	protected void updateListView (ArrayList<ApiObject> result){
		
		// - Create the adapter
		ArrayAdapterConversationList adapter = new ArrayAdapterConversationList(
				_inflater.getContext(),  
				android.R.layout.simple_expandable_list_item_1,
				result);
		
		//
		// - Sets the adapter for the superclass
		setArrayAdapter(adapter);
		
		super.updateListView(result);
	}
	
	//
	// - Overrides the function onItemClick from the implementation of OnItemClickListener
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		//
		// - TODO open conversation_chat
		Intent chatActivity = new Intent(getActivity(), ChatActivity.class);
		
		//
		// - Get the selected object
		ApiObject selectedObject = (ApiObject) getListAdapter().getItem(position);
		
		//
		// - Then we get the conversation_id
		String conversation_id = ApiObjectHandler.ConversationObject.getId(selectedObject);
		
		//
		// - Intent put extra with the conversation_id
		chatActivity.putExtra("id", conversation_id);
		System.out.println("Conversation_id: " + conversation_id);
		
		
		startActivity(chatActivity);
	}
	
	/**
	 * Clicking the create new button will lead to a new activity
	 * where the user selects friends to create a new conversation with
	 */
	private void clickedCreateNewButton(){
		Intent newChatIntent = new Intent(getActivity(), ActivitySelectFriends.class);
		startActivity(newChatIntent);
	}
	
	
	/**
	 * Will take the members for a new conversation, send to api and create, and then 
	 * open the chat activity
	 * @param members ApiObjects as users for the new chat
	 */
	private void createNewConversation(ArrayList <ApiObject> members){
		System.out.println("Members size():" + members.size());
		
		ArrayList<String> userIdentifiers = new ArrayList<String>();
		for(ApiObject user : members){
			String user_id = ApiObjectHandler.UserObject.getId(user);
			userIdentifiers.add(user_id);
		}
		
		String commaSeparatedUsers = commaSeparatedString(userIdentifiers);
		
		//
		// _ Create the api post
		ApiPost apiPost = new ApiPost();
		apiPost.addParameter("users", commaSeparatedUsers);
		apiPost.startPostDataTo("conversation/add", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				System.out.println("Response: " + response);
				
				try {
					JSONObject responseObject = new JSONObject(response);
					//
					// - If the response has conversation_id as a parameter
					if(responseObject.getString("conversation_id") != null){
						String conversation_id = responseObject.getString("conversation_id");
						//
						// - We can open the chat activity
						Intent chatIntent = new Intent(getActivity(), ChatActivity.class);
						chatIntent.putExtra("id", conversation_id);
						startActivity(chatIntent);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
	}
	
	private String commaSeparatedString(ArrayList<String> arrayList){
		String listString = arrayList.toString();
		String csv = listString.substring(1, listString.length() - 1).replace(", ", ",");
		
		return csv;
	}
}
