package com.android.minglr.fragments;

import java.util.ArrayList;

import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.adapters.ArrayAdapterImagedItem;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.example.android.navigationdrawerexample.R;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

//
// - EXP
// - An instance of this class will download nearby locations and is therefore 
// - dependent on the LocationManagers updates

// - TODO
// - Implement a real LocationManager, can't do that yet because of emulator reasons
// - and shortcomings

// - TODO
// - Add images for the locations in the list
public class Fragment_NearbyLocations extends Fragment_DefaultList implements ApiDownloadInterface{

	public Fragment_NearbyLocations() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		
		// - Allocate
		_inflater = inflater;
		
		//
		// - Download instance
		// - TODO
		// - Fix the hardcoded variables
		ApiDownload download = new ApiDownload();
		double lat = 59.337135; // - Hardcoded userLocation
		double lon = 18.062725; // - Hardcoded userLocation
		String urlString = "http://api-test.minglr.se/location/list?latitude=" 
		+ lat 
		+ "&longitude=" 
		+ lon 
		+ "&distance=500"; // - Hardcoded distance
		 
		download.startDownload(urlString, "locations", this);
		System.out.println("urlstring: " + urlString);
		
		// - Return super
		// - because this is an indirect subclass of ListFragment
		// - without a layout
		return super.onCreateView(inflater, container, savedStateInstance);
	}
	
	@Override
	public void onStart(){
		getView().setBackgroundColor(Color.WHITE);
		super.onStart(); // - IF FORGET THIS EVERYTIME!!!
	}
	
	// **************************************************************** //
	// *********************** SUPER CLASS FUNCTIONS ****************** //
	// **************************************************************** //
	
	@Override
	protected void updateListView(ArrayList<ApiObject> result){
		
		//
		// - Set the adapter with the new data (result)
		ArrayAdapterImagedItem adapter = new ArrayAdapterImagedItem(
				_inflater.getContext(), 
				android.R.layout.simple_expandable_list_item_1, 
				result);
		
		setArrayAdapter(adapter);
		
		super.updateListView(result);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		
		ApiObject clickedItem = (ApiObject) getListAdapter().getItem(position);
		System.out.println("Clicked item: " + clickedItem.getObject().toString());
		
		// TODO - Check in function
		
		// TODO - Open location function
		
		super.onListItemClick(l, v, position, id);
	}	
	
	// **************************************************************** //
	// *********************** SUPER CLASS FUNCTIONS ****************** //
	// **************************************************************** //
	
	// **************************************************************** //
	// ********************** API DOWNLOAD INTERFACE ****************** //
	// **************************************************************** //
	
	@Override
	public void arrayFetchComplete(ArrayList<ApiObject> result,
			String variableName) {
		// TODO Auto-generated method stub
		
		updateListView(result);
		
	}

	@Override
	public void objectFetchComplete(ApiObject result, String variableName) {
		// TODO Auto-generated method stub
		
	}
	// **************************************************************** //
	// ********************** API DOWNLOAD INTERFACE ****************** //
	// **************************************************************** //
}
