package com.android.minglr.fragments;

import java.util.ArrayList;
import java.util.Locale;

import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.example.android.navigationdrawerexample.ActivityDefaultProfile;
import com.example.android.navigationdrawerexample.MapActivity;
import com.example.android.navigationdrawerexample.R;
import com.google.android.gms.maps.model.LatLng;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.SearchView;

public class Fragment_MapSearch extends Fragment_DefaultList implements ApiDownloadInterface {

	public Fragment_MapSearch() {
		// TODO Auto-generated constructor stub
	}
	
	
	//
	// - EXP
	// - Since there is a change that the location list is not ready upon creating 
	// - this instance, we need a way for the class which sends the list (atm MapActivity)
	// - to check if the list has been set or not
	private boolean _locationListPopulated = false;
	public boolean isLocationListPopulated(){
		return _locationListPopulated;
	}
	
	//
	// - EXP
	// - Locations for the queries
	private ArrayList <ApiObject> _locations;
	public void setLocationList(ArrayList<ApiObject> locationList){
		
		_locations = new ArrayList<ApiObject>(locationList);
		
		// - Set private instance var to true if the 
		// - location list is populated
		if(_locations.size() > 0){
			_locationListPopulated = true;
		}
	}

	// - Override
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
	
		//
		// - We need the inflater in updateListView
		_inflater = inflater;
		
		View rootView = inflater.inflate(R.layout.fragment_map_search, container, false);
		
		//
		// - we want an empty array at start
		ArrayList <ApiObject> emptyArrayList = new ArrayList <ApiObject> ();
		updateListView(emptyArrayList);
		
		//
		// - If _locations variable could not be set by the MapActivity
		// - we will have to register as a broadcast receiver
		// - since the download was then not yet complete
		
		return rootView;
	}
	
	@Override
	public void onStart(){
		super.onStart();
		
		//
		// - Set on search listener
		SearchView searchView = (SearchView) getView().findViewById(R.id.map_search_view);
		searchView.setOnQueryTextListener(new onTextChangeListener());
		getView().setBackgroundColor(Color.WHITE);
	}
	
	@Override
	//
	// - DOCS
	// - Is override because we want to remove the keyboard when the view is destroyed 
	// - (actually before)
	public void onDestroyView(){
		//
		// _ Remove the keyboard when we destroy this view and enter the map
		removeKeyboardFromSearchView();
		
		// - Super callback
		super.onDestroyView();
	}
	
	//
	// - DOCS 
	// - Simply remove the keyboard from the searchView
	private void removeKeyboardFromSearchView(){
		//
		// - Remove the keyboard from the view
		SearchView searchView = (SearchView) getView().findViewById(R.id.map_search_view);
		removeKeyboardFromView(searchView);
	}
	
	private void removeKeyboardFromView(View view){
		//
		// - Remove keyboard from param view
		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	//
	// - Method will be called when the data is fetched and the listView() should update
	@Override
	protected void updateListView(ArrayList<ApiObject> result){
		
		//
		// - IF the arrayAdapter has not been set yet
		if(_arrayAdapter == null){
			
			//
			// - Set the adapter with the new data (result)
			ArrayAdapterCheckInList adapter = new ArrayAdapterCheckInList(
					_inflater.getContext(), 
					android.R.layout.simple_expandable_list_item_1, 
					result);
			
			//
			// - Set the context for the adapter (it neeeds it)
			setArrayAdapter(adapter);
			
			// - Super call
			super.updateListView(result);
			
			_data = result;
		}
		//
		// - We probably come from downloading a query/search
		else{
			_arrayAdapter.clear();
			_arrayAdapter.addAll(result);
			_arrayAdapter.notifyDataSetChanged();
		}
	}	
	
	//
	// - On click
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		
		ApiObject clickedObject = (ApiObject) getListView().getItemAtPosition(position);
		
		//
		// _ Get the coordinat of the clicked object
		LatLng locationCoordinate = ApiObjectHandler.LocationObject.getCoordinate(clickedObject);
		
		System.out.println("Clicked coord: " + locationCoordinate.toString());
		
		//
		// - zoom on the map activity and remove this fragment
		MapActivity currentActivity = (MapActivity)getActivity();
		currentActivity.zoomMapToCoordinate(locationCoordinate, 16);
		currentActivity.removeSearchFragment();
	}
	
	
	//
	//******************* API DOWNLOAD INTERFACE ******************//
	@Override
	public void arrayFetchComplete(ArrayList<ApiObject> result, String variableName) {
		// TODO Auto-generated method stub
	}

	@Override
	public void objectFetchComplete(ApiObject result, String variableName) {
		// TODO Auto-generated method stub
		
	}
	//******************* API DOWNLOAD INTERFACE ******************//
	//
	
	
	//
	// ************ PRIVATE CLASS LISTENERS ************* //
	
	private class onTextChangeListener implements SearchView.OnQueryTextListener{

		@Override
		public boolean onQueryTextChange(String newText) {
			// TODO Auto-generated method stub
			
			System.out.println("OnQueryTextChange: " + newText);
			
			if(newText == null || newText.length() == 0){
				cancelSearchQuery();
			}
			
			return false;
		}

		@Override
		public boolean onQueryTextSubmit(String query) {
			// TODO Auto-generated method stub
			System.out.println("OnQueryTextSubmit");
			
			startSearchQuery(query);
			
			return false;
		}
		
	}
	
	//
	// - This function starts the query to the server
	// - with an apiDownload instance
	private void startSearchQuery(String text){
		
		if(!isLocationListPopulated()){
			return;
		}
		
		//
		// - Create the download instance
		//
		// - We will add all the locations matching in name to this list
		ArrayList <ApiObject> queryArrayList = new ArrayList<ApiObject>();
		
		if(_locations == null){
			System.out.println("LocationList is null");
		}
		
		for(ApiObject object : _locations){
			
			// - name of location
			String name = ApiObjectHandler.LocationObject.getName(object);
			
			// - if name contains the search text
			if(name.toLowerCase(Locale.getDefault()).contains(text.toLowerCase())){
				
				// - Add it to the results
				queryArrayList.add(object);
			}
		}
		
		//
		// - Now update the listView with this new array
		updateListView(queryArrayList);
	}
	
	//
	// - This function will reset the search, and display friends once again
	private void cancelSearchQuery(){
		//
		// - We clear the list
		ArrayList <ApiObject> emptyArrayList = new ArrayList <ApiObject> ();
		updateListView(emptyArrayList);
	}
	
	// ************ PRIVATE CLASS LISTENERS ************* //
	//
	
}
