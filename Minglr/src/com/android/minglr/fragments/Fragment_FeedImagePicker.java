package com.android.minglr.fragments;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.minglr.models.ApiMultipartPost;
import com.example.android.navigationdrawerexample.R;

public class Fragment_FeedImagePicker extends Fragment_FeedStatusUpdate {
	
	/**
	 * Will add a string everytime a image is added. That way we can know how many images 
	 * has been added. 
	 */
	private ArrayList<String> addedImages = new ArrayList<String>();
	
	private boolean _image1Set;
	private boolean _image2Set;
	private boolean _image3Set;

	public Fragment_FeedImagePicker() {
		// TODO Auto-generated constructor stub
		System.out.println("Created FeedImagePicker");
		
	}
	
	public void onViewCreated(View view, Bundle savedInstance){
		super.onViewCreated(view, savedInstance);
		
		addImageViewListeners();
		
	}
	
	/**
	 * Adds listeners to the imageViews which represent the images the user has previously selected
	 * to upload
	 */
	private void addImageViewListeners(){
		//
		// - Add listener to image view so that a click on it will remove it
		final ImageView imageView = (ImageView)getView().findViewById(R.id.category_image_view);
		final ImageView imageView2 = (ImageView)getView().findViewById(R.id.imageView2);
		final ImageView imageView3 = (ImageView)getView().findViewById(R.id.imageView3);
		
		final ArrayList<ImageView> imageViewList = new ArrayList<ImageView>();
		imageViewList.add(imageView);
		imageViewList.add(imageView2);
		imageViewList.add(imageView3);
		
		for(final ImageView iView : imageViewList){
			iView.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					iView.setImageBitmap(null);
					v.setVisibility(View.GONE);
					
					//
					// - Depening on which imageView, we have to
					// - set relevant flag to false
					if(v.equals(imageView))
						_image1Set = false;
					else if(v.equals(imageView2))
						_image2Set = false;
					else _image3Set = false;
					
					//
					// - If added images are 0, we remove the "selected images" view
					if(!_image1Set && !_image2Set && !_image3Set){
						LinearLayout imageLayout = (LinearLayout)getView().findViewById(R.id.image_layout);
						imageLayout.setVisibility(View.GONE);
					}
						
				}
			});
		}
	}
	
	/**
	 * Will open the image picker and delegate the result to onActivityResult	
	 */
	protected void openImagePicker(){
		Intent imageIntent = new Intent(
				Intent.ACTION_PICK, 
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(imageIntent, 0);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		System.out.println("OnActivityResult");
		
		ImageView imageView = (ImageView)getView().findViewById(R.id.category_image_view);
		ImageView imageView2 = (ImageView)getView().findViewById(R.id.imageView2);
		ImageView imageView3 = (ImageView)getView().findViewById(R.id.imageView3);
		
		if(resultCode == Activity.RESULT_OK){
			System.out.println("OK!");
			
			Uri targetUri = data.getData();
		    Bitmap bitmap;
		    try{
		    	bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(targetUri));
		    	
		    	if(bitmap == null)
		    		return;
		    	
		    	
		    	// - Set image somewhere
		    	if(!_image1Set){
		    		setDrawableForLayoutId(bitmap, R.id.category_image_view);
		    		_image1Set = true;
		    	}
		    	else if(!_image2Set){
		    		setDrawableForLayoutId(bitmap, R.id.imageView2);
		    		_image2Set = true;
		    	}
		    	else if(!_image3Set){
		    		setDrawableForLayoutId(bitmap, R.id.imageView3);
		    		_image3Set = true;
		    	}
		    	
		    	// - We also have to show the image layout
		    	LinearLayout layout = (LinearLayout)getView().findViewById(R.id.image_layout);
		    	layout.setVisibility(View.VISIBLE);
		    	
		    	
		    }catch(FileNotFoundException e){
		    	e.printStackTrace();
		    }
		}
	}

}
