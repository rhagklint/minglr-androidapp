package com.android.minglr.fragments;

import java.util.ArrayList;

import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.LocationCheckInHandler;
import com.android.minglr.tasks.SimpleJsonFetcher;
import com.example.android.navigationdrawerexample.ActivityLocationProfile;
import com.example.android.navigationdrawerexample.R;

import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Fragment_CheckIn extends Fragment_DefaultList {

	public Fragment_CheckIn() {
		// TODO Auto-generated constructor stub
	}
	
	LayoutInflater _inflater;
	ArrayList<ApiObject> _data;
	
	private String _checkedInLocationID;
	
	// - Override
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		//
		// - We need the inflater in updateListView
		_inflater = inflater;
		
		//
		// - Url for the request ( the rest gets taken care of in the super class)
		// - TODO
		// - Fix the hardcoded variables
		setURLString("http://api-test.minglr.se/location/list?latitude=59.3371355&longitude=18.0627258&distance=1000");
		setArrayVariable("locations");
		LocationCheckInHandler.getInstance().downloadCheckInLocation(null);
		
		//
		// - If we have no check-in location for the user
		if(LocationCheckInHandler.getInstance().checkedInIdentifier() == null){
			//
			// - If we have no record of checked-in location_id, we download it
			LocationCheckInHandler.getInstance().downloadCheckInLocation(new ApiDownloadInterface() {
				
				@Override
				public void objectFetchComplete(ApiObject result, String variableName) {
					//
					// - We KNOW that the LocationCheckInHandler has the checked in id now
					_checkedInLocationID = LocationCheckInHandler.getInstance().checkedInIdentifier();
					
					// - If we still have no checked in locaiton id, we return, since the user
					// - is not checked in anywhere
					if(_checkedInLocationID == null)
						return;
					
					//
					// - If we do on the other hand, we want to update the checked in list view
					// - We have to do so on the main UI thread, so that the re-draw is possible
					Handler mainHandler = new Handler(getView().getContext().getMainLooper());
					Runnable runnable = new Runnable() {
						
						@Override
						public void run() {
							
							// - Cast getListAdapter to correct class
							ArrayAdapterCheckInList currentAdapter = (ArrayAdapterCheckInList)getListAdapter();
							if(currentAdapter != null)
								currentAdapter.notifyDataSetChanged();
						}
					};
					
					mainHandler.post(runnable);
				}
				
				// - NOT USED
				@Override
				public void arrayFetchComplete(ArrayList<ApiObject> result,
						String variableName) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
		// - Return the rootView
		return super.onCreateView(inflater, container, savedStateInstance);
	}		
			
	//
	// - OnListItemClick
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		System.out.println("OnListItemClick");
		
		ApiObject locationObject = (ApiObject) getListAdapter().getItem(position);
		String location_id = ApiObjectHandler.LocationObject.getId(locationObject);
		
		Intent intent = new Intent(getActivity(), ActivityLocationProfile.class);
		intent.putExtra("location_id", location_id);
		
		startActivity(intent);
		
		super.onListItemClick(l, v, position, id);
	}			
			
	//
	// - Method will be called when the data is fetched and the listView() should update
	@Override
	protected void updateListView(ArrayList<ApiObject> result){
		
		//
		// - Set the adapter with the new data (result)
		ArrayAdapterCheckInList adapter = new ArrayAdapterCheckInList(
				_inflater.getContext(), 
				android.R.layout.simple_expandable_list_item_1, 
				result);
		
		setArrayAdapter(adapter);
		super.updateListView(result);
	}	
	
}
