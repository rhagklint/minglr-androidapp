package com.android.minglr.fragments;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.R.string;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.minglr.adapters.ArrayAdapterComments;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.example.android.navigationdrawerexample.R;

public class Fragment_Comments extends Fragment_DefaultList implements ApiDownloadInterface {

	//
	// - Default constructor
	public Fragment_Comments() {
		// TODO Auto-generated constructor stub
	}
	
	//
	// - We need a ApiObject which is the post all the comments are about
	// - this is that variable and it's set method
	// - this will be the first object in the array of data
	// - The instance will be downloaded via a ApiDownload. Ultimatly we would want to pass it from the 
	// - activity via getIntent().getExtras() but ApiObject is not Parcable atm
	// - That means we will also need a String status_id so we can download the status instead
	private ApiObject _originalPost;
	private String _status_id;
	public void setOriginalPost(ApiObject post){ 
		_originalPost = post; 
	}
	public void setStatusId(String status_id){
		_status_id = status_id;
	}
	
	
	//
	// - OVERRIDE onCreateView
	@Override 
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		
		_inflater = inflater;
	
		//
		// - Start download
		if(_status_id != null){
			ApiDownload downloadInstance = new ApiDownload();
			downloadInstance.startDownload(
					"http://api-test.minglr.se/feed/status/" + _status_id, 
					"status", 
					true,
					this); // - True is set so we know that this i a single return, meaning
						  // - we don't get an array in return - so that the objectFetchComplete
					      // - gets called instead
		}
		
		
		return super.onCreateView(inflater, container, savedStateInstance);
		
	}
	
	//
	// - updateListView override from Fragment_DefaultList
	// - altho this class does not atm adhere to the same implementation
	// - of the superclass with a private class of GetDataTask to fetch data. Instead it
	// - uses the ApiDownload implementation, which is a bit more generic and 
	// - easy to read.
	// - The superclass and the before created subclasses uses this method when it is called
	// - from the Fragment_DefaultList in onPostExecute(). In this class we call it from
	// - arrayFetchComplete, meaning NO interaction with superclass at all, other
	// - than simple design aesthetics
	@Override
	protected void updateListView(ArrayList<ApiObject> result) {
		
		//
		// - We have to add the original post on top of this array
		ArrayList <ApiObject> objects = new ArrayList<ApiObject>();
		
		if(_originalPost != null)
			objects.add(_originalPost);
		
		objects.addAll(result);
		System.out.println("objects count: " + objects.size());
		System.out.println("result count:  " + result.size());
		
		ArrayAdapterComments adapter = new ArrayAdapterComments(
				_inflater.getContext(), 
				android.R.layout.simple_expandable_list_item_1, 
				objects);
		
		System.out.println("Setting adapter");
		setArrayAdapter(adapter);
		super.updateListView(result);
	}
	
	
	//
	// ***************** API DOWNLOAD INTERFACE **************//
	
	//
	// - Is called when the ApiDownload instance finishes it's download
	@Override
	public void arrayFetchComplete(ArrayList<ApiObject> result, String variableName) {
		this.updateListView(result);
	}
	
	@Override
	public void objectFetchComplete(ApiObject result, String variableName) {
		// TODO Auto-generated method stub
		
		//
		// - set original post
		JSONObject post = null;
		
		try {
			System.out.println("Printing: " + result.getObject().toString());
			post = new JSONObject(result.getObject().get("status"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		_originalPost = new ApiObject(post);
		
		
		// - Update list view
		updateListView(ApiObjectHandler.FeedObject.getCommentList(result));
		
		
	}
	// ***************** API DOWNLOAD INTERFACE **************//
	//
	
	public void startDownload(String status_id){
		ApiDownload downloadInstance = new ApiDownload();
		_status_id = status_id;
		downloadInstance.startDownload(
				"http://api-test.minglr.se/feed/status/" + status_id, 
				"status", 
				true,
				this); 
	}
}
