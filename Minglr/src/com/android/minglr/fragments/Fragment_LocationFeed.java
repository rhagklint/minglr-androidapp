package com.android.minglr.fragments;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import android.widget.EditText;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.example.android.navigationdrawerexample.R;

public class Fragment_LocationFeed extends Fragment_Feed {

	public Fragment_LocationFeed() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * This will be the location_id provided by a set function
	 * which we will use to fetch the correct feed
	 */
	private String _locationID;
	
	/**
	 * This function will provide us with the currently displayed location id
	 * We can't use the Constructor for this, because android is wierd and
	 * don't want fragments initializing that way
	 * @param location_id will set private variable _locationID
	 */
	public void setLocationID(String location_id){
		_locationID = location_id;
	}
	
	
	//
	// - Overrides from the superclass Fragment_Feed
	// ========================================================================================== //
	// *************************************** OVERRIDES **************************************** //
	// ========================================================================================== //
	
	/**
	 * We need to override downloadFeed() since we are downloading a location
	 * object which will contain a feed, and we will not be usin the same 
	 * api call nor the same variables to fetch the list of status updates
	 */
	@Override
	protected void downloadFeed(final String lastPostId){
		
		//
		// - Flag is loading
		// - OBS, this is not used as of yet, but might
		// - be needed in the future
		_isLoading = true;
		
		//
		// - Close the keyboard
		CloseKeyBoard(getActivity());
		
		//
		// - Create the url string for the api call
		String url = "location/get/" + _locationID + "?include_feed=1&limit=5";
		
		// - We append the last post id variable if this is not null
		if(lastPostId != null && lastPostId.length() > 0){
			url = url + "&until_id=" + lastPostId;
		}
		
		// - Assign api download instance
		ApiDownload apiDownload = new ApiDownload();
		apiDownload.startDownload(url, "feed", false, new ApiDownloadInterface() {
			
			//
			// - Not used
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				//
				// - NOT USED
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
				updateListView(result);
			}
		});
	}
	
	/**
	 * We have to override this since we want to add another paramter to the 
	 * data being posted to the server, namely "location_id"
	 * This is not a "nice" solutions by any objective oriented standars, but it is
	 * a quick fix. Might want to redo this later and solve it much more elegantly. 
	 * The mentioned "error" is that we copy code from Fragment_Feed but maybe add one or two lines
	 * of code. BAD!
	 */
	@Override
	protected void postStatusUpdate(String message){
System.out.println("Status message to post: " + message);
    	
    	// - We need the array adapter
    	final ArrayAdapterApiObject arrayAdapter = (ArrayAdapterApiObject) getListAdapter();
    	
    	//
    	// - Post instance
    	ApiPost postInstance = new ApiPost();
    	
    	// - Add parameter
    	postInstance.addParameter("text", message);
    	postInstance.addParameter("location_id", _locationID);
    	
    	// - And then start the post, with a paremeterized interface 
    	postInstance.startPostDataTo("feed/add_status", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				
				System.out.println("Fragment_feed: postStatusUpdate() response: " + response);
				
				EditText textView = (EditText) getView().findViewById(R.id.statusField);
				textView.setText("");
				
				//
				// - Get the jsonObject
				try {
					// - Assign
					JSONObject responseJson = new JSONObject(response);
					
					// - Get the status_id
					String status_id = responseJson.getString("status_id");
					
					// -
					System.out.println("Fragment_Feed: status_id: " + status_id);
					
					// - Start the download of that status id
					ApiDownload apiDownload = new ApiDownload();
					apiDownload.startDownload("feed/status/" + status_id, "whatever", true, new ApiDownloadInterface() {
						
						@Override
						public void objectFetchComplete(ApiObject result, String variableName) {
							JSONObject post = null;
							
							try {
								System.out.println("Printing: " + result.getObject().toString());
								post = new JSONObject(result.getObject().get("status"));
								
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							// - Create the newly posted object
							ApiObject newPost = new ApiObject(post);
							
							// - Now get the insert that into the array adapter
							arrayAdapter.insert(newPost, 0);
							
							// - And notify changes
							arrayAdapter.notifyDataSetChanged();
						}
						
						@Override
						public void arrayFetchComplete(ArrayList<ApiObject> result,
								String variableName) {
						}
					});
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	// ========================================================================================== //
	// *************************************** OVERRIDES **************************************** //
	// ========================================================================================== //

}
