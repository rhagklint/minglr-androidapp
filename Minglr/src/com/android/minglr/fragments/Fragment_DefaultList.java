package com.android.minglr.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHeader;
import com.android.minglr.tasks.SimpleJsonFetcher;
import com.example.android.navigationdrawerexample.R;

public class Fragment_DefaultList extends ListFragment{

	//
	// - Instance variables
	
	// - The string which gets used when we want to fetch data for the list adapter
	String  _urlGetString;
	public void setURLString(String url){
		_urlGetString = url;
	}
	
	// - The variable which will contain the array that will be displayed in the data we fetched
	String _arrayVariable;
	public void setArrayVariable(String arrayVariable){
		_arrayVariable = arrayVariable;
	}
	
	// - LayoutInflater
	protected LayoutInflater _inflater;
	
	// - The data
	ArrayList<ApiObject> _data;
	
	// - The adapter (which will be set in the subclass)
	ArrayAdapterApiObject _arrayAdapter;
	protected void setArrayAdapter(ArrayAdapterApiObject arrayAdapter){
		_arrayAdapter = arrayAdapter;
	}
	
	// - The layout which contains all the relevant views to make a status update
	LinearLayout _statusUpdateLayout;
		
	// - Indicating wheter the status update layout is visible or not
	boolean _isUpdatingStatus = false;
	
	
	//
	// - Standard constructor
	public Fragment_DefaultList() {
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		// - Init the root view
		//View rootView = inflater.inflate(R.layout.fragment_feed, container, false);
		
		// - Find the listView
		//ListView listView = (ListView)rootView.findViewById(R.id.feed_listview);
		//
		// - We need the inflater in updateListView
		_inflater = inflater;
		
		startGetDataTask();
		
		// - Return the rootView
		return super.onCreateView(inflater, container, savedStateInstance);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceBundle){
		
		//
		// - Set background color
		//getListView().setBackgroundResource(R.drawable.clean_background);
		view.setBackgroundResource(R.drawable.clean_background);
		
		//
		// - Super call
		super.onViewCreated(view, savedInstanceBundle);
	}
	
	protected void startGetDataTask(){
		//
		// - Start the fetching of data
		GetDataTask task = new GetDataTask();
		
		//
		// - Url for the request
		if(_urlGetString != null && _arrayVariable != null){
			task.setGetRequestURL(_urlGetString);
			
			// 
			// - The "location" variable contains the list of locations
			task.setArrayVariable(_arrayVariable);
			task.execute();
		}
	}
	
	//
	// - Method will be called when the data is fetched and the listView() should update
	protected void updateListView(ArrayList<ApiObject> result){
		
		//
		// - Set the context for the adapter (it neeeds it)
		_arrayAdapter.setContext(getActivity().getApplicationContext());
		
		//
		// - Save the data
		_data = result;
		
		// - set list adatper
		this.setListAdapter(_arrayAdapter);
		
	}		
		
	
	/****************** PRIVATE CLASS *********************/
	//
	// - GetDataTask to fetch the places where the user can check-in
	private class GetDataTask extends SimpleJsonFetcher{
		
		@Override
		protected void onPostExecute(ArrayList<ApiObject> result) {
			
	    	// Publish result
			updateListView(result);
	   }
	}
	//
	// - 
	/****************** PRIVATE CLASS *********************/
	
	
	
	private BroadcastReceiver mStatusUpdateReceiver = new BroadcastReceiver(){
		
		//
		// - This member will handle messages sent from mainActivity
		// - when the user taps the action button in the action bar for 
		// - status updates
		
		@Override
		public void onReceive(Context context, Intent intent){
			if(!_isUpdatingStatus)
				showStatusUpdateView(getView());
			else
				hideStatusUpdateView(getView());
		}
	};
	
	//For open keyboard
	public void OpenKeyBoard(View focusView, Context mContext){
		InputMethodManager manager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		manager.showSoftInput(focusView, InputMethodManager.SHOW_FORCED);
	}
		
	//For close keyboard
	public void CloseKeyBoard(Context mContext){
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
    }
	
	//
	// - Show the view which the user updates their status with
	protected void showStatusUpdateView(View superView){
		
		// - Set boolean _isUpdatingStatus to true, so that the
		// - broadcastReceiver knows if the views has appeared and is visible
		_isUpdatingStatus = true;
		
		//LinearLayOut Setup
        _statusUpdateLayout = new LinearLayout(getActivity());
        _statusUpdateLayout.setOrientation(LinearLayout.HORIZONTAL);

        _statusUpdateLayout.setLayoutParams(new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));

        //ImageView Setup
        EditText textView = new EditText(getActivity());
        //setting image resource
        textView.setBackgroundColor(Color.GRAY);
        textView.setTextColor(Color.BLACK);
        
        //setting image position
        textView.setLayoutParams(new LayoutParams(
        		400,
        		200));

        // - Add on focus listener to the textView
        textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus)
					OpenKeyBoard(v, getActivity());
				else
					CloseKeyBoard(getActivity());
			}
		});

        // - Send button
        Button sendButton = new Button(getActivity());
        sendButton.setLayoutParams(new LayoutParams(200, 70));
        sendButton.setText("Update");
        sendButton.setTextSize(11);
        
        //
        // - Add on click listener to sendButton
        sendButton.setOnClickListener(new Button.OnClickListener(){
        	public void onClick(View v){
        		
        		// - TODO
        		// - clear textView text but seems it has to
        		// - be an instance variable for that to work
        		
        		//
        		// - post status update to server
        		postStatusUpdate("Can't get message from textView atm, needs to be instanceialzed?");
        		
        		//
        		// - Add the new item to the array ontop of the data array
        		addStatusUpdateToData("Can't get status update just yet");
        		
        		//
        		// - Hide the post status view
        		hideStatusUpdateView(getView());
        	}
        });

        //adding view to layout
        _statusUpdateLayout.addView(textView);
        _statusUpdateLayout.addView(sendButton);
        
        //make visible to program
        ViewGroup viewGroup = (ViewGroup) superView;
        viewGroup.addView(_statusUpdateLayout);
        
        //
        // - Also set focus for the EditText
        textView.requestFocus();
	}
	
	//
	// - hide the view from which the user updates their status
	protected void hideStatusUpdateView(View superView){
		
		// - Set boolean _isUpdatingStatus to false, so that the
		// - broadcastReceiver knows if the views has disappeared and is not visible
		_isUpdatingStatus = false;
				
		//
		// - Remove everything from the layout
		_statusUpdateLayout.removeAllViews();
		_statusUpdateLayout.removeAllViewsInLayout();
		
		//
		// - Get the viewGroup so we can remove the layout too
		ViewGroup viewGroup = (ViewGroup) superView;
		viewGroup.removeView(_statusUpdateLayout);
		
		// - set _statusUpdateLayout to null
		_statusUpdateLayout = null;
		
		//
		// - If keyboard is showing we also close that
		CloseKeyBoard(getActivity());
	}
	  
	// - Send update to server
    // - TODO
    private void postStatusUpdate (String message){
    	// - TODO
    	// - Send to server, and also need to create a feedItem class so that the paramater can 
    	// - be one of those
    }
    
    // - Add an item to the top of the data array
    // - TODO
    private void addStatusUpdateToData (String update){
    	// - TODO
    	// - need to create a FeedItem class
    }
    
    /**
	 * Creates an arrayList of apiObjects for a given variable name. The variable name is for an array within the
	 * object.
	 * @param array the array of objects which will either be friends, sent or pending
	 * @param variable will be the name of the header
	 * @return an array list of apiObjects with header on top
	 */
	protected ArrayList <ApiObject> createArrayListSegment(ApiObject result, String variable, String headerName){
		
		
		// - Assign the return argument
		ArrayList <ApiObject> returnArray = new ArrayList<ApiObject>();
		
		// - Create and add the header
		ApiObjectHeader header = new ApiObjectHeader();
		header.headerText = headerName;
		returnArray.add(header);
		
		try{
			JSONArray array = result.getJSON().getJSONArray(variable);
			for(int i = 0; i < array.length(); i++){
				
				// - Current jsonObject
        		JSONObject jsonObject = (JSONObject) array.get(i);
        		
        		// - Converted to apiObject
        		ApiObject apiObject = new ApiObject(jsonObject);
        		System.out.println("Variable: " + variable + " Object: " + apiObject.getObject().toString());
        		
        		// - Add
        		returnArray.add(apiObject);
			}
			
		}catch(JSONException e){
			e.printStackTrace();
		}
		
		//
		// - If this is only populated by the header, we remove the header
		// - so we return an empty array
		if(returnArray.size() == 1){
			returnArray.remove(0);
		}
		
		return returnArray;
	}

}
