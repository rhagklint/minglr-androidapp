package com.android.minglr.fragments;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.example.android.navigationdrawerexample.R;

import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * This class will act as a superclass to any Fragment_Feed that want to be able to publish status
 * updates
 * @author Robert
 *
 */
public class Fragment_FeedStatusUpdate extends Fragment_FeedDownload {
	
	// - The layout which contains all the relevant views to make a status update
	protected LinearLayout _statusUpdateLayout;
	
	// - Indicating wheter the status update layout is visible or not
	protected boolean _isUpdatingStatus = false;
	
	

	public Fragment_FeedStatusUpdate() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		// - When we want to publish a new status update
		LocalBroadcastManager.getInstance(
				getActivity()).registerReceiver(
						mStatusUpdateReceiver, 
						new IntentFilter("broadcast_status_update")
						);
		
		return super.onCreateView(inflater, container, savedStateInstance);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstance){
		
		super.onViewCreated(view, savedInstance);
		
		//
		// - Add listener to the status update button
		Button statusButton = (Button) getView().findViewById(R.id.statusButton);
		statusButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				EditText textField = (EditText) getView().findViewById(R.id.statusField);
				postStatusUpdate(textField.getText().toString());
			}
		});
		
		// - Set background color for status_layout (practically the "status bar")
		LinearLayout status_layout = (LinearLayout)getView().findViewById(R.id.status_layout);
		status_layout.setBackgroundColor(Color.GRAY);
		
		//
		// - Set image for post button
		//statusButton.setBackgroundResource(R.drawable.button_yellow_clean);
		setDrawableResourceForLayoutId(R.drawable.button_yellow_clean, R.id.statusButton);
		statusButton.setTextColor(Color.WHITE);
		
		//
		// - CLEAR FOCUS YEAH!
		EditText statusField = (EditText)getView().findViewById(R.id.statusField);
		statusField.clearFocus();
	}
	
	// =============================================================================================== //
	// ************************************* STATUS UPDATE ******************************************* //
	// =============================================================================================== //
	
	
	//
	// - Show the view which the user updates their status with
	protected void showStatusUpdateView(View superView){
		
		// - Set boolean _isUpdatingStatus to true, so that the
		// - broadcastReceiver knows if the views has appeared and is visible
		_isUpdatingStatus = true;
		
		//LinearLayOut Setup
        _statusUpdateLayout = new LinearLayout(getActivity());
        _statusUpdateLayout.setOrientation(LinearLayout.HORIZONTAL);

        _statusUpdateLayout.setLayoutParams(new LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));

        //ImageView Setup
        EditText textView = new EditText(getActivity());
        //setting image resource
        textView.setBackgroundColor(Color.GRAY);
        textView.setTextColor(Color.BLACK);
        
        //setting image position
        textView.setLayoutParams(new LayoutParams(
        		400,
        		200));

        // - Add on focus listener to the textView
        textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(hasFocus)
					OpenKeyBoard(v, getActivity());
				else
					CloseKeyBoard(getActivity());
			}
		});

        // - Send button
        Button sendButton = new Button(getActivity());
        sendButton.setLayoutParams(new LayoutParams(200, 70));
        sendButton.setText("Update");
        sendButton.setTextSize(11);
        
        //
        // - Add on click listener to sendButton
        sendButton.setOnClickListener(new Button.OnClickListener(){
        	public void onClick(View v){
        		
        		// - TODO
        		// - clear textView text but seems it has to
        		// - be an instance variable for that to work
        		
        		//
        		// - post status update to server
        		postStatusUpdate("Can't get message from textView atm, needs to be instanceialzed?");
        		
        		//
        		// - Add the new item to the array ontop of the data array
        		//addStatusUpdateToData("Can't get status update just yet");
        		
        		//
        		// - Hide the post status view
        		hideStatusUpdateView(getView());
        	}
        });

        //adding view to layout
        _statusUpdateLayout.addView(textView);
        _statusUpdateLayout.addView(sendButton);
        
        //make visible to program
        ViewGroup viewGroup = (ViewGroup) superView;
        viewGroup.addView(_statusUpdateLayout);
        
        //
        // - Also set focus for the EditText
        textView.requestFocus();
	}
	
	//
	// - hide the view from which the user updates their status
	protected void hideStatusUpdateView(View superView){
		
		// - Set boolean _isUpdatingStatus to false, so that the
		// - broadcastReceiver knows if the views has disappeared and is not visible
		_isUpdatingStatus = false;
				
		//
		// - Remove everything from the layout
		_statusUpdateLayout.removeAllViews();
		_statusUpdateLayout.removeAllViewsInLayout();
		
		//
		// - Get the viewGroup so we can remove the layout too
		ViewGroup viewGroup = (ViewGroup) superView;
		viewGroup.removeView(_statusUpdateLayout);
		
		// - set _statusUpdateLayout to null
		_statusUpdateLayout = null;
		
		//
		// - If keyboard is showing we also close that
		CloseKeyBoard(getActivity());
	}
	
	//For open keyboard

	

    // - Send update to server
    protected void postStatusUpdate (String message){
    	// - TODO
    	// - Send to server, and also need to create a feedItem class so that the paramater can 
    	// - be one of those
    	System.out.println("Status message to post: " + message);
    	
    	// - We need the array adapter
    	final ArrayAdapterApiObject arrayAdapter = (ArrayAdapterApiObject) getListAdapter();
    	
    	//
    	// - Post instance
    	ApiPost postInstance = new ApiPost();
    	
    	// - Add parameter
    	postInstance.addParameter("text", message);
    	
    	// - And then start the post, with a paremeterized interface 
    	postInstance.startPostDataTo("feed/add_status", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				
				System.out.println("Fragment_feed: postStatusUpdate() response: " + response);
				
				EditText textView = (EditText) getView().findViewById(R.id.statusField);
				textView.setText("");
				
				//
				// - Get the jsonObject
				try {
					// - Assign
					JSONObject responseJson = new JSONObject(response);
					
					// - Get the status_id
					String status_id = responseJson.getString("status_id");
					
					// -
					System.out.println("Fragment_Feed: status_id: " + status_id);
					
					// - Start the download of that status id
					ApiDownload apiDownload = new ApiDownload();
					apiDownload.startDownload("feed/status/" + status_id, "whatever", true, new ApiDownloadInterface() {
						
						@Override
						public void objectFetchComplete(ApiObject result, String variableName) {
							JSONObject post = null;
							
							try {
								System.out.println("Printing: " + result.getObject().toString());
								post = new JSONObject(result.getObject().get("status"));
								
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							// - Create the newly posted object
							ApiObject newPost = new ApiObject(post);
							
							// - Now get the insert that into the array adapter
							arrayAdapter.insert(newPost, 0);
							
							// - And notify changes
							arrayAdapter.notifyDataSetChanged();
						}
						
						@Override
						public void arrayFetchComplete(ArrayList<ApiObject> result,
								String variableName) {
						}
					});
					
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		});
    }
	
	// =============================================================================================== //
	// ************************************* STATUS UPDATE ******************************************* //
	// =============================================================================================== //
    
    
    // =============================================================================================== //
 	// ******************************** BROADCAST RECEIVER ******************************************* //
 	// =============================================================================================== //
    protected BroadcastReceiver mStatusUpdateReceiver = new BroadcastReceiver(){
		
		//
		// - This member will handle messages sent from mainActivity
		// - when the user taps the action button in the action bar for 
		// - status updates
		
		@Override
		public void onReceive(Context context, Intent intent){
			if(!_isUpdatingStatus)
				showStatusUpdateView(getView());
			else
				hideStatusUpdateView(getView());
		}
	};
	
	// =============================================================================================== //
	 // ******************************** BROADCAST RECEIVER ******************************************* //
	 // =============================================================================================== /

	// =============================================================================================== //
	// *************************************** UTILITY *********************************************** //
	// =============================================================================================== //
	
	/**
	 * Force open keyboard
	 * @param focusView
	 * @param mContext
	 */
	public void OpenKeyBoard(View focusView, Context mContext){
        //InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
		InputMethodManager manager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		manager.showSoftInput(focusView, InputMethodManager.SHOW_FORCED);
	}

	/**
	 * Close keyboard
	 * @param mContext
	 */
	public void CloseKeyBoard(Context mContext){
		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,0);
	}

	/**
	 * Simple help function to set drawable backround resource for layout/view id
	 * @param drawableResource
	 * @param layoutId
	 */
	protected void setDrawableResourceForLayoutId(int drawableResource, int layoutId){
		View view = (View)getView().findViewById(layoutId);
		view.setBackgroundResource(drawableResource);
		
		BitmapDrawable background = (BitmapDrawable)view.getBackground(); // assuming you have bg_tile as background.
		BitmapDrawable newBackground = new BitmapDrawable(background.getBitmap()) {
		    @Override
		    public int getMinimumWidth() {
		        return 0;
		    }

		    @Override
		    public int getMinimumHeight() {
		        return 0;
		    }
		};
		newBackground.setTileModeXY(background.getTileModeX(), background.getTileModeY());
		view.setBackgroundDrawable(newBackground);
	}
	
	protected void setDrawableForLayoutId(Bitmap bitmap, int lid){
		View view = (View)getView().findViewById(lid);
		view.setVisibility(View.VISIBLE);
		view.setBackgroundDrawable(new BitmapDrawable(getResources(), bitmap));
		
		BitmapDrawable background = (BitmapDrawable)view.getBackground(); // assuming you have bg_tile as background.
		BitmapDrawable newBackground = new BitmapDrawable(background.getBitmap()) {
		    @Override
		    public int getMinimumWidth() {
		        return 0;
		    }

		    @Override
		    public int getMinimumHeight() {
		        return 0;
		    }
		};
		newBackground.setTileModeXY(background.getTileModeX(), background.getTileModeY());
		view.setBackgroundDrawable(newBackground);
	}
	// =============================================================================================== //
	// *************************************** UTILITY *********************************************** //
	// =============================================================================================== //
}
