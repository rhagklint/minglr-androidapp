package com.android.minglr.fragments;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.adapters.ArrayAdapterFriendList;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiMultipartPost;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiObjectHeader;
import com.android.minglr.tasks.SimpleJsonFetcher;
import com.example.android.navigationdrawerexample.ActivityDefaultProfile;
import com.example.android.navigationdrawerexample.R;

import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

/**
 * Displays a list of friends divided by; 'friends', 'sent' and 'pending'
 * @author Robert
 *
 */
public class Fragment_FriendList extends Fragment_DefaultList {
	
	protected Boolean _searchViewEnabled = true;
	private String _friendListUrl = "http://api-test.minglr.se/user/friends?include_gender=1&include_location=0";
	
	public Fragment_FriendList() {
		// TODO Auto-generated constructor stub
	}
	
		// - Override
		@Override
		public View onCreateView(
				LayoutInflater inflater, 
				ViewGroup container, 
				Bundle savedStateInstance){
			
			//
			// - We need the inflater in updateListView
			_inflater = inflater;
			
			View rootView = inflater.inflate(R.layout.fragment_friend_list, container, false);
			
			startFriendListDownload();
			
			return rootView;

			// - Return the rootView
			//return super.onCreateView(inflater, container, savedStateInstance);
		}		
		
		@Override
		public void onStart(){
			super.onStart();
			
			//
			// - Set on search listener
			if(_searchViewEnabled){
				SearchView searchView = (SearchView) getView().findViewById(R.id.searchView1);
				searchView.setOnQueryTextListener(new onTextChangeListener());
			}
		}
		
		/**
		 * Will start download of the friends, and then sort that result, and after that
		 * it will publish the result on the UI thread with the ArrayAdapter
		 */
		private void startFriendListDownload(){
			
			ApiDownload friendListDownload = new ApiDownload();
			friendListDownload.startDownload(_friendListUrl, "", true, new ApiDownloadInterface() {
				
				@Override
				public void objectFetchComplete(ApiObject result, String variableName) {
					ArrayList <ApiObject> sortedResult = sortFriendList(result);
					updateListView(sortedResult);
				}
				
				// - Not used
				@Override
				public void arrayFetchComplete(ArrayList<ApiObject> result,
						String variableName) {
				}
			});
		}
		
		/**
		 * Takes the result from the get friends api call, and divides them into 3 sections;
		 * friends, sent and pending. For each of these we also add a ApiObjectHeader
		 * @param resul
		 * @return
		 */
		private ArrayList<ApiObject> sortFriendList(ApiObject result){
			
			ArrayList<ApiObject> sortedList = new ArrayList<ApiObject>();
			
			// - Add all the friends with a header on top
			sortedList.addAll(createArrayListSegment(result, "friends", "Friends"));
			sortedList.addAll(createArrayListSegment(result, "requests", "Requests"));
			sortedList.addAll(createArrayListSegment(result, "sent", "Sent"));
			
			
			return sortedList;
		}
		
		
		
		//
		// - Method will be called when the data is fetched and the listView() should update
		// - or when we have sorted the result of the friend response
		@Override
		protected void updateListView(ArrayList<ApiObject> result){
			
			
			//
			// - IF the arrayAdapter has not been set yet
			if(_arrayAdapter == null){
				
				//
				// - Set the adapter with the new data (result)
				ArrayAdapterFriendList adapter = new ArrayAdapterFriendList(
						_inflater.getContext(), 
						android.R.layout.simple_expandable_list_item_1, 
						result);
				
				//
				// - Set the context for the adapter (it neeeds it)
				setArrayAdapter(adapter);
				
				// - Super call
				super.updateListView(result);
				
				_data = result;
			}
			//
			// - We probably come from downloading a query/search
			else{
				_arrayAdapter.clear();
				_arrayAdapter.addAll(result);
				_arrayAdapter.notifyDataSetChanged();
			}
		}		
		
		//
		// - On click
		@Override
		public void onListItemClick(ListView l, View v, int position, long id) {
			// TODO Auto-generated method stub
			super.onListItemClick(l, v, position, id);
			
			ApiObject selectedObject = (ApiObject) getListAdapter().getItem(position);
			String user_id = ApiObjectHandler.UserObject.getId(selectedObject);
			
			// - if this is a header, we don't wanna do nuthin'
			if(selectedObject instanceof ApiObjectHeader){
				return;
			}
			
			Intent newIntent = new Intent(getActivity(), ActivityDefaultProfile.class);
			newIntent.putExtra("user_id", user_id);
			
			startActivity(newIntent);
		}
		
		
		//
		// ************ PRIVATE CLASS LISTENERS ************* //
		
		private class onTextChangeListener implements SearchView.OnQueryTextListener{

			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				
				System.out.println("OnQueryTextChange: " + newText);
				
				if(newText == null || newText.length() == 0){
					cancelSearchQuery();
				}
				
				return false;
			}

			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				System.out.println("OnQueryTextSubmit");
				
				startSearchQuery(query);
				
				return false;
			}
			
		}
		
		//
		// - This function starts the query to the server
		// - with an apiDownload instance
		private void startSearchQuery(String text){
			
			//
			// - Create the download instance
			ApiDownload searchDownload = new ApiDownload();
			searchDownload.startDownload("http://api-test.minglr.se/user/search?query=" + text, 
					"users", 
					new ApiDownloadInterface() {
				
				@Override
				public void objectFetchComplete(ApiObject result, String variableName) {
					
				}
				
				@Override
				public void arrayFetchComplete(ArrayList<ApiObject> result,
						String variableName) {
					updateListView(result);
					
				}
			});
		}
		
		//
		// - This function will reset the search, and display friends once again
		private void cancelSearchQuery(){
			//
			// - We do this by simply reusing the initial data
			// - which is saved in super.updateListView() as the variable _data
			// - the first time this instance sets up the array adapter (the initial download of friends)
			// - EDIT: simply reusing the _data would show an empty list, so we download the friends again
			// - Leaving a TODO here, so that we can fix and possibly reuse the _data variable afterall
			startFriendListDownload();
		}
		
		// ************ PRIVATE CLASS LISTENERS ************* //
		//
		

}
