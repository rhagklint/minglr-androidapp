package com.android.minglr.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.minglr.adapters.ArrayAdapterCategories;
import com.android.minglr.adapters.ArrayAdapterImagedItem;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.CategoryHandler;
import com.example.android.navigationdrawerexample.CategoriesActivity;
import com.example.android.navigationdrawerexample.ChatActivity;
import com.example.android.navigationdrawerexample.R;

/**
 * This class is marked up in the CategoriesActivity.xml
 * @author Robert
 *
 */
public class Fragment_Categories extends Fragment_DefaultList implements ApiDownloadInterface {

	public Fragment_Categories() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		//
		// - We need the inflater in updateListView
		_inflater = inflater;
		
		View rootView = inflater.inflate(R.layout.fragment_categories_list, container, false);
		
		ApiDownload downloadInstance = new ApiDownload();
		downloadInstance.startDownload(
				"http://api-test.minglr.se/location/categories", 
				"categories", 
				new ApiDownloadInterface() {
					
					// - Not used
					@Override
					public void objectFetchComplete(ApiObject result, String variableName) {
						
					}
					
					@Override
					public void arrayFetchComplete(ArrayList<ApiObject> result,
							String variableName) {
						updateListView(result);
					}
				});
		
		return rootView;
	}
	
	// ********************************************************* //
	// ************** FRAGMENT_DEFAULT OVERRIDE **************** //
	// ********************************************************* //
	
	@Override
	protected void updateListView(ArrayList<ApiObject> result){
		
		ArrayAdapterCategories adapter = new ArrayAdapterCategories(
				_inflater.getContext(), 
				R.layout.list_item_image, 
				result);
		
		setArrayAdapter(adapter);
		
		_data = new ArrayList<ApiObject> (result);
		
		super.updateListView(result);
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		ApiObject category = (ApiObject) getListAdapter().getItem(position);
		String categoryId = ApiObjectHandler.CategoryObject.getId(category);
		
		if(CategoryHandler.isCategorySelected(categoryId, getActivity())){
			
			CategoryHandler.setCategoryUnselected(categoryId, getActivity());
			
			v.setBackgroundColor(CategoryHandler.getListItemColorForCategory(categoryId, getActivity()));
		}
		else{
			
			CategoryHandler.setCategorySelected(categoryId, getActivity());
			
			v.setBackgroundColor(CategoryHandler.getListItemColorForCategory(categoryId, getActivity()));
		}
		
		
		super.onListItemClick(l, v, position, id);
	}
	
	
	// ********************************************************* //
	// ************** FRAGMENT_DEFAULT OVERRIDE **************** //
	// ********************************************************* //
	
	// ********************************************************* //
	// **************** API DOWNLOAD INTERFACE ***************** //
	// ********************************************************* //
	@Override
	public void arrayFetchComplete(ArrayList<ApiObject> result,
			String variableName) {
		// TODO Auto-generated method stub
		updateListView(result);
		
	}

	@Override
	public void objectFetchComplete(ApiObject result, String variableName) {
		// TODO Auto-generated method stub
		
	}	
	// ********************************************************* //
	// **************** API DOWNLOAD INTERFACE ***************** //
	// ********************************************************* //	
}
