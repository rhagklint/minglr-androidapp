package com.android.minglr.fragments;

import java.util.ArrayList;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.android.minglr.adapters.ArrayAdapterChat;
import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.adapters.ArrayAdapterConversationList;
import com.android.minglr.adapters.ArrayAdapterSelectFriends;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ConversationCreator;
import com.example.android.navigationdrawerexample.R;
import com.google.android.gms.common.api.Api;

/**
 * A simple "friend picker class" which can be subclassed. When the user presses the done button
 * the singleton ConversationCreator will add the users to its internal array of selected users.
 * Activities can then check in that Singleton instance if there are any users added there, and retrieve
 * them if there is. Upon retrieval, the array will be emptied
 * @author Robert
 *
 */
public class Fragment_SelectFriends extends Fragment_DefaultList {
	
	private String _friendListUrl = "http://api-test.minglr.se/user/friends?include_gender=1&include_location=0";
	/**
	 * Will contain any friend the user has selected
	 */
	private ArrayList <ApiObject> _selectedUsers = new ArrayList<ApiObject>();

	public Fragment_SelectFriends() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		
		//
		// - We need the inflater in updateListView
		_inflater = inflater;
		
		startFriendListDownload();
		
		View view = inflater.inflate(R.layout.fragment_select_friends, container, false);
		return view;
		
		//return super.onCreateView(inflater, container, savedStateInstance);
	}
	
	@Override
	public void onViewCreated(View view, Bundle bundle){
		
		//
		// - Set doneButton text and listener to doneButtonTapped()
		Button doneButton = (Button)view.findViewById(R.id.button_done);
		doneButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tappedDoneButton();
			}
		});
		doneButton.setText("Done");
		
		super.onViewCreated(view, bundle);
	}
	
	
	//
	// - Contains methods for data fetching from server and setting and updating list
	// - adapter
	// ============================================================================================ //
	// ******************************** DATA AND LISTVIEW RELATED ********************************* //
	// ============================================================================================ //
	/**
	 * Start the download and update the listView
	 */
	private void startFriendListDownload(){
		
		ApiDownload friendListDownload = new ApiDownload();
		friendListDownload.startDownload(_friendListUrl, "", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				
				//
				// - We will add all the friends here
				ArrayList<ApiObject> friends = createArrayListSegment(result, "friends", "Friends");
				updateListView(friends);
				
			}
			
			// - Not used
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
			}
		});
	}
	
	@Override
	protected void updateListView(ArrayList<ApiObject> result){
		
		//
		// - Create array adapter 
		ArrayAdapterSelectFriends adapter = new ArrayAdapterSelectFriends(
				getActivity(), 
				android.R.layout.simple_list_item_1, 
				result);
		
		setArrayAdapter(adapter);
		super.updateListView(result);
	}


	// ============================================================================================ //
	// ******************************** DATA AND LISTVIEW RELATED ********************************* //
	// ============================================================================================ //
	
	// ============================================================================================ //
	// ******************************* BUTTON AND LIST ITEM CLICKS ******************************** //
	// ============================================================================================ //
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		View tappedView = l.getChildAt(position);
		ApiObject selectedObject = (ApiObject)l.getItemAtPosition(position);
		
		//
		// - If it was already selected so we deselect it
		if(_selectedUsers.contains(selectedObject)){
			tappedView.setBackgroundColor(Color.TRANSPARENT);
			tappedView.setSelected(false);
			_selectedUsers.remove(selectedObject);
		}
		//
		// - It was NOT selected so we select it
		else{
			tappedView.setBackgroundColor(Color.WHITE);
			tappedView.setSelected(true);
			_selectedUsers.add(selectedObject);
		}
		
		System.out.println("Selected users size(): " + _selectedUsers.size());
	}
	
	private void tappedDoneButton(){
		
		//
		// - Add all the selected friends to the ConversationCreator singleton
		for(ApiObject selectedFriend : _selectedUsers){
			ConversationCreator.getInstance().insertFriendIntoConversation(selectedFriend);
		}
		
		getActivity().finish();
	}
	// ============================================================================================ //
	// ******************************* BUTTON AND LIST ITEM CLICKS ******************************** //
	// ============================================================================================ //
}
