package com.android.minglr.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.minglr.adapters.ArrayAdapterMapSettings;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.MinglrConfig;
import com.example.android.navigationdrawerexample.CategoriesActivity;
import com.example.android.navigationdrawerexample.R;

public class Fragment_MapSettings extends Fragment_DefaultList {

	public Fragment_MapSettings() {
		// TODO Auto-generated constructor stub
	}
	
	//
	// - Create the array for the menu options
	private void createMenuArray(){
		ApiObject option1 = new ApiObject();
		option1.getObject().put("name", "Show men/women");
		option1.getObject().put("type", "check_box");
		
		ApiObject option2 = new ApiObject();
		option2.getObject().put("name", "Show heatmap");
		option2.getObject().put("type", "check_box");
		
		ApiObject option3 = new ApiObject();
		option3.getObject().put("name", "Only show open");
		option3.getObject().put("type", "check_box");
		
		ApiObject option4 = new ApiObject();
		option4.getObject().put("name", "Categories");
		option4.getObject().put("type", "open_intent");
		
		ApiObject option5 = new ApiObject();
		option5.getObject().put("name", "Geo friends");
		option5.getObject().put("type", "check_box");
		
		_data = new ArrayList<ApiObject>();
		_data.add(option1);
		_data.add(option2);
		_data.add(option3);
		_data.add(option4);
		_data.add(option5);
	}
	
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		_inflater = inflater;
		
		return super.onCreateView(inflater, container, savedStateInstance);
	}
	
	@Override
	public void onStart(){
		
		
		createMenuArray();
		updateListView(_data);
		
		getView().setBackgroundColor(Color.WHITE);
		
		super.onStart();
	}
	
	@Override
	public void onViewCreated(View view, Bundle bundle){
		//
		// - Set background
		//getView().setBackgroundResource(R.drawable.clean_background);
		getListView().setBackgroundResource(R.drawable.clean_background);
		super.onViewCreated(view, bundle);
	}
	
	@Override
	protected void updateListView(ArrayList<ApiObject> result){
		ArrayAdapterMapSettings adapter = new ArrayAdapterMapSettings(
				_inflater.getContext(), 
				android.R.layout.simple_expandable_list_item_1, 
				_data);
		
		setArrayAdapter(adapter);
		
		super.updateListView(_data);
	}
	
	//
	// - Overrides the function onItemClick from the implementation of OnItemClickListener
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		
		//
		// - Categories list item
		if(position == 3){
			//
			// - TODO open conversation_chat
			Intent categoriesActivity = new Intent(getActivity(), CategoriesActivity.class);
		
		
			// - Start the activity
			startActivity(categoriesActivity);
		}
	}

}
