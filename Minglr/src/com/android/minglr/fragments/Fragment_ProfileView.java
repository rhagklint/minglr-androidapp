package com.android.minglr.fragments;

import java.util.ArrayList;

import com.android.minglr.adapters.ArrayAdapterCheckInList;
import com.android.minglr.adapters.ArrayAdapterUserProfile;
import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.ApiPost;
import com.android.minglr.models.ApiPostInterface;
import com.example.android.navigationdrawerexample.R;

import android.R.integer;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Fragment_ProfileView extends Fragment_DefaultProfile {
	
	//
	// - We need to be able to set the user_id
	private String _userID;
	public void setUserID(String user_id){
		_userID = user_id;
	}
	
	public Fragment_ProfileView() {
		setProfileObjectType(kProfileObjectType.kProfileObjectTypeUser);
	}
	
	//
	// - onCreateView we can start downloading everything
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		
		return inflater.inflate(R.layout.fragment_profile_view, container, false);
	}
	
	//
	// - Just to set background resource
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){
		
		view.setBackgroundResource(R.drawable.clean_background);
		
		super.onViewCreated(view, savedInstanceState);
	}
	
	@Override
	public void onStart(){
		
		// - Obligatory super call
		super.onStart();
		
		//
		// - Start the download
		ApiDownload downloadInstance = new ApiDownload();
		downloadInstance.startDownload(
				"user/get/" + _userID + "?include_feed=1&limit=5", 
				"whatever",
				true,
				new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(final ApiObject result, String variableName) {
				System.out.println("Fetched user in Fragment_ProfileView: " + result.getObject().toString());
				
				//
				// - Get the userObject
				ApiObject userObject = ApiObjectHandler.UserObject.getBaseUserObject(result);
				
				// - Create a temp arrayList
				ArrayList <ApiObject> tempList = new ArrayList<ApiObject>();
				tempList.add(userObject);
				tempList.addAll(ApiObjectHandler.FeedObject.getFeedList(result));
				
				//updateListView(ApiObjectHandler.FeedObject.getFeedList(result));
				updateListView(tempList, userObject);
				
				//
				// - SetupFrriendButtonsWithObject on MAIN THREAD
				Handler mainHandler = new Handler(getView().getContext().getMainLooper());
				Runnable runnable = new Runnable() {
					
					@Override
					public void run() {
						setupFriendButtonsWithObject(result);
					}
				};
				
				mainHandler.post(runnable);
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				// TODO Auto-generated method stub
			}
		});
	}
	
	/**
	 * Update the listView with an arrayList of apiObjects
	 * Here we create an ArrayAdapter which will populate the list
	 * @param result, the arrayList which will populate the ListView
	 * @param userObject, the userObject which we need to set the info
	 * in the ProfileView in the ArrayAdapter
	 */
	private void updateListView(ArrayList <ApiObject> result, ApiObject userObject){
		
		ListView lv = (ListView) getView().findViewById(R.id.combinedListView);
		
		ArrayAdapterUserProfile adapter = new ArrayAdapterUserProfile(
				getActivity(), 
				android.R.layout.simple_expandable_list_item_1, 
				result);
		
		adapter.setUserObject(userObject);
		lv.setAdapter(adapter);
	}
	
	/**
	 * Will handle the buttons and their visibility and actions depending on friend
	 * status with this visiting user
	 * @param result
	 */
	private void setupFriendButtonsWithObject(ApiObject result){
		
		//
		// - These buttons has layout: view_profile_item
		
		//
		// - Assign pointers to the buttons
		Button acceptButton = (Button)getView().findViewById(R.id.button_accept_request);
		Button declineButton = (Button)getView().findViewById(R.id.button_decline_request);
		
		//
		// - Also refine result to object user and get the user_id
		ApiObject user = ApiObjectHandler.refineObject(result, "user");
		final String user_id = ApiObjectHandler.UserObject.getId(user);
		
		//
		// - Remove the buttons and return if this is me or this is a friend
		if(result.getObject().get("is_me").equals("true") ||
		   result.getObject().get("is_friend").equals("true")){
			
			setupButtonsAsFriend(acceptButton, declineButton);
			
			return;
		}
		
		//
		// - if the user and this person is NOT friends and there has been no
		// - friend_request
		if(result.getObject().get("is_friend").equals("false") &&
		   result.getObject().get("friend_request").equals("false")){
			
			setupButtonsForFriendRequest(acceptButton, declineButton, user_id);
			
			return;
		}
		//
		// - Request is pending - waiting for device owners reply 
		else if(result.getObject().get("is_friend").equals("false") &&
				result.getObject().get("friend_request").equals("pending")){
			
			setupButtonsRequestPending(acceptButton, declineButton, user_id);
			
		}
		//
		// - Request has already been sent
		else if(result.getObject().get("is_friend").equals("false") &&
				result.getObject().get("friend_request").equals("sent")){
			
			setupButtonsWaitingForReply(acceptButton, declineButton);
		}

	}
	
	//
	// - Helper functions to avoid redudant code
	// - These functions will setup the buttons with differet types of layouts/listeners
	// ================================================================================= //
	// **************************** BUTTON LAYOUT TYPES ******************************** //
	// ================================================================================= //
	
	/**
	 * Sets the button as they should be if the profile is you or a friend
	 */
	private void setupButtonsAsFriend(Button acceptButton, Button declineButton){
		
		acceptButton.setVisibility(View.GONE);
		declineButton.setVisibility(View.GONE);
		
	}
	
	/**
	 * Make it possible to send friend request
	 * @param acceptButton
	 * @param declineButton
	 * @param user is the user we will send the request to
	 */
	private void setupButtonsForFriendRequest(Button acceptButton, Button declineButton, final String user_id){
		
		// - Make accept button the "request" button instead. By clicking that we
		// - will request friendship. 
		declineButton.setVisibility(View.GONE);
		
		//
		// - Setup acceptButton for sending a friend request
		acceptButton.setText("Request");
		acceptButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendFriendRequest(user_id);
			}
		});			
	}
	
	/**
	 * Setup the buttons so that the request button is visible. This will make it possible to accept 
	 * friend requests
	 * @param acceptButton
	 * @param declineButton
	 * @param user which we will accept the request from
	 */
	private void setupButtonsRequestPending(Button acceptButton, Button declineButton, final String user_id){
		//
		// - Set accept button listener
		acceptButton.setText("Accept");
		acceptButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				acceptFriendRequest(user_id);
			}
		});
		
		//
		// - Set decline button listener
		declineButton.setText("Decline");
		declineButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				declineFriendRequest(user_id);
			}
		});
	}
	
	/**
	 * Setup the buttons as the user has already sent a request. The buttons will be removed
	 * @param acceptButton
	 * @param declineButton
	 */
	private void setupButtonsWaitingForReply(Button acceptButton, Button declineButton){
		//
		// - Remove all the buttons
		setupButtonsAsFriend(acceptButton, declineButton);
		
		// - But also add label that request is sent
	}
	
	// ================================================================================= //
	// **************************** BUTTON LAYOUT TYPES ******************************** //
	// ================================================================================= //
	
	/**
	 * Show alert with text
	 * @param text
	 */
	private void showAlertWithText(String text){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(text);
		builder.setPositiveButton("Ok", null);
		
		AlertDialog dialog = builder.create();
		dialog.show();
		
	}
	
	//
	// - Api requests for sending - declining - accepting friend requests
	// ================================================================================= //
	// **************************** API REQUESTS *************************************** //
	// ================================================================================= //
	
	/**
	 * Send friend request to user id
	 * @param user_id
	 */
	private void sendFriendRequest(String user_id){
		System.out.println("Sending friend request to user id: " + user_id);
		
		ApiPost friendRequest = new ApiPost();
		friendRequest.addParameter("user_id", user_id);
		friendRequest.startPostDataTo("user/friend/add", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				showAlertWithText("Friend request sent!");
			}
		});
		
		Button acceptButton = (Button)getView().findViewById(R.id.button_accept_request);
		Button declineButton = (Button)getView().findViewById(R.id.button_decline_request);
		setupButtonsWaitingForReply(acceptButton, declineButton);
	}
	
	/**
	 * Decline friend request from user id
	 * @param user_id
	 */
	private void declineFriendRequest(String user_id){
		System.out.println("Declined friend request to user id: " + user_id);
		
		ApiPost declineRequest = new ApiPost();
		declineRequest.addParameter("user_id", user_id);
		declineRequest.startPostDataTo("user/friend/delete", new ApiPostInterface() {
			@Override
			public void postDataComplete(String response) {
				showAlertWithText("Friend request declined");
			}
		});
		
		Button acceptButton = (Button)getView().findViewById(R.id.button_accept_request);
		Button declineButton = (Button)getView().findViewById(R.id.button_decline_request);
		setupButtonsForFriendRequest(acceptButton, declineButton, user_id);

	}
	
	/**
	 * Accept friend request from user id
	 * @param user_id
	 */
	private void acceptFriendRequest(String user_id){
		System.out.println("Accepted friend request to user id: " + user_id);
		
		ApiPost acceptRequest = new ApiPost();
		acceptRequest.addParameter("user_id", user_id);
		acceptRequest.startPostDataTo("user/friend/confirm", new ApiPostInterface() {
			@Override
			public void postDataComplete(String response) {
				showAlertWithText("You are now friends!");
			}
		});
		
		Button acceptButton = (Button)getView().findViewById(R.id.button_accept_request);
		Button declineButton = (Button)getView().findViewById(R.id.button_decline_request);
		setupButtonsAsFriend(acceptButton, declineButton);

	}
	
	// ================================================================================= //
	// **************************** API REQUESTS *************************************** //
	// ================================================================================= //
}
