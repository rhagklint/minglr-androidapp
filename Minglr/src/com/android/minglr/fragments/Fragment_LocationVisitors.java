package com.android.minglr.fragments;

import java.util.ArrayList;

import org.json.JSONArray;

import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.example.android.navigationdrawerexample.R;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class Fragment_LocationVisitors extends Fragment_FriendList {
	
	//
	// - Location id
	private String _locationID;
	public void setLocationID(String location_id){
		_locationID = location_id;
	}

	
	// - We don't want super searchView to be enabled on start
	 
	public Fragment_LocationVisitors() {
		// TODO Auto-generated constructor stub
	}

	//
	// - Override on create view
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedInstanceState){
		
		_inflater = inflater;
		
		//
		// - Inflate rootView
		View rootView = inflater.inflate(R.layout.fragment_location_visitors, container, false);
		
		//
		// - Start fetching visitors
		showAllVisitors();
		
	
		return rootView;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){

		//
		// - Assign listeners to the buttons which
		// - decide which list the user sees
		Button showFriendsButton = (Button)getActivity().findViewById(R.id.button_show_friends);
		Button showAllButton = (Button)getActivity().findViewById(R.id.button_show_all);
		
		showFriendsButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showFriendVisitors();
				
			}
		});
		showAllButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showAllVisitors();
			}
		});
	
		super.onViewCreated(view, savedInstanceState);
	}
	
	@Override
	public void onStart(){
		_searchViewEnabled = false;
		super.onStart();
	}
	
	/**
	 * Downloads the visitors and shows all of them
	 */
	private void showAllVisitors(){
		
		if(_locationID == null) 
			return;
		
		// - Download instance
		ApiDownload apiDownload = new ApiDownload();
		
		// - Start download and get location
		apiDownload.startDownload("location/get/" + _locationID + "?include_feed=0", "", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				
				// - When the object fetch is complete, get the visitor list
				System.out.println("Getting visitors: " + result.getObject().toString());
				ArrayList <ApiObject> visitorList = ApiObjectHandler.LocationObject.getVisitorsList(result);
				
				// - And the update the list view
				updateListView(visitorList);					
			}
			
			//
			// - NOT USED
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
			}
		});
		
	}			
			
			

	/**
	 * Show only the visitors which you are befriended with
	 */
	private void showFriendVisitors(){
		
		
		if(_locationID == null)
			return;
		
		// - Download instance
		ApiDownload apiDownload = new ApiDownload();
		
		//
		// - Start download and get friends on location
		apiDownload.startDownload("location/friends/" + _locationID, "friends", new ApiDownloadInterface() {
			
			//
			// - Not used
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				
				updateListView(result);
			}
		});
		
	}
}
