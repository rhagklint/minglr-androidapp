package com.android.minglr.fragments;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.minglr.models.ApiDownload;
import com.android.minglr.models.ApiDownloadInterface;
import com.android.minglr.models.ApiObject;
import com.android.minglr.models.ApiObjectHandler;
import com.android.minglr.models.Settings;
import com.example.android.navigationdrawerexample.ActivityPrivacy;
import com.example.android.navigationdrawerexample.ActivityTerms;
import com.example.android.navigationdrawerexample.ActivityUpdateUserAge;
import com.example.android.navigationdrawerexample.ActivityUpdateUserAlias;
import com.example.android.navigationdrawerexample.ActivityUpdateUserMail;
import com.example.android.navigationdrawerexample.R;

import android.R.integer;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class Fragment_Settings extends Fragment {

	public Fragment_Settings() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(
			LayoutInflater inflater, 
			ViewGroup container, 
			Bundle savedStateInstance){
		
		View view = inflater.inflate(R.layout.fragment_settings, container, false);
		view.setBackgroundResource(R.drawable.clean_background);
		
		return view;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState){
		super.onViewCreated(view, savedInstanceState);
		
		//
		// - Set text and listeners
		setLabelTexts();
		setListeners();
		
		//
		// - Download status info for user
		ApiDownload statusDownload = new ApiDownload();
		statusDownload.startDownload("user/get/me", "user", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				//
				// - Get the user object
				ApiObject userObject = ApiObjectHandler.refineObject(result, "user");
				System.out.println("User: " + userObject.getObject().toString());
				
				//
				// - Now we want to check if the user has public stats or not
				String status = userObject.getObject().get("public");
				if(status.equals("1")){
					// - Public status
					// - Set the correct checkbox checked and unchecked
					CheckBox friendsCheckBox = (CheckBox)getView().findViewById(R.id.check_privacy_friends);
					CheckBox publicCheckBox  = (CheckBox)getView().findViewById(R.id.check_privacy_public);
					friendsCheckBox.setChecked(false);
					publicCheckBox.setChecked(true);
				}
				else{
					// - Friends only status
					// - Set the correct checkbox checked and unchecked
					CheckBox friendsCheckBox = (CheckBox)getView().findViewById(R.id.check_privacy_friends);
					CheckBox publicCheckBox  = (CheckBox)getView().findViewById(R.id.check_privacy_public);
					friendsCheckBox.setChecked(true);
					publicCheckBox.setChecked(false);
				}
			}
			
			// - NOT USED
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				// TODO Auto-generated method stub
				
			}
		});
	
		//
		// - Download settings for the user, so that we can see if they have 
		// - chat notifications on or off
		ApiDownload settingsDownload = new ApiDownload();
		settingsDownload.startDownload("user/settings", "", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				
				// - Get the settings object
				ApiObject settings = ApiObjectHandler.refineObject(result, "settings");
				String chatNotifications = settings.getObject().get("push_conversation");
			
				// - Check if chatNotifications is on or off
				// - ON
				if(chatNotifications.equals("1")){
					// - Get chatBox
					CheckBox chatCheckBox = (CheckBox)getView().findViewById(R.id.check_chat_notifications);
					chatCheckBox.setChecked( true );
				}
				// - OFF
				else{
					// - Get chatBox
					CheckBox chatCheckBox = (CheckBox)getView().findViewById(R.id.check_chat_notifications);
					chatCheckBox.setChecked( false );
				}
			}
			
			// - NOT USED
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}
	
	// =============================================================================================== //
	// ************************************** HELPER FUNCTIONS *************************************** //
	// =============================================================================================== //
	/**
	 * Set texts and images and text colors for all the layout variables in the layout
	 */
	private void setLabelTexts(){
		
		//
		// - Label/textViews
		TextView generalTextView = (TextView)getView().findViewById(R.id.label_general);
		TextView privacyTextView = (TextView)getView().findViewById(R.id.label_privacy_settings);
		TextView termsTextView   = (TextView)getView().findViewById(R.id.label_terms);
		TextView changeTextView  = (TextView)getView().findViewById(R.id.label_change);
		
		// - Set their corresponding text
		generalTextView.setText("GENERAL");
		privacyTextView.setText("PRIVACY SETTINGS");
		termsTextView.setText("TERMS");
		changeTextView.setText("CHANGE");
		
		//
		// - CheckBoxes
		CheckBox chatCheckBox 	 = (CheckBox)getView().findViewById(R.id.check_chat_notifications);
		CheckBox friendsCheckBox = (CheckBox)getView().findViewById(R.id.check_privacy_friends);
		CheckBox publicCheckBox  = (CheckBox)getView().findViewById(R.id.check_privacy_public);
		
		// - Set their corresponding texts
		chatCheckBox.setText("CHAT - NOTIFICATIONS");
		friendsCheckBox.setText("FRIENDS");
		publicCheckBox.setText("PUBLIC");
		
		//
		// - Buttons
		Button termsButton 	 = (Button)getView().findViewById(R.id.button_user_terms);
		Button privacyButton = (Button)getView().findViewById(R.id.button_privacy);
		Button pictureButton = (Button)getView().findViewById(R.id.button_change_picture);
		Button aliasButton 	 = (Button)getView().findViewById(R.id.button_change_alias);
		Button mailButton 	 = (Button)getView().findViewById(R.id.button_change_mail);
		Button ageButton 	 = (Button)getView().findViewById(R.id.button_change_age);
		
		// - Set texts
		termsButton.setText("USER TERMS");
		privacyButton.setText("PRIVACY");
		pictureButton.setText("PROFILE PICTURE");
		aliasButton.setText("NAME / ALIAS");
		mailButton.setText("E-MAIL");
		ageButton.setText("AGE");
		
		// - TextColor
		termsButton.setTextColor(Color.WHITE);
		privacyButton.setTextColor(Color.WHITE);
		pictureButton.setTextColor(Color.WHITE);
		aliasButton.setTextColor(Color.WHITE);
		mailButton.setTextColor(Color.WHITE);
		ageButton.setTextColor(Color.WHITE);
		
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, termsButton);
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, privacyButton);
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, pictureButton);
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, aliasButton);
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, mailButton);
		setDrawableResourceForLayoutId(R.drawable.list_table_cell_background, ageButton);
	}
	
	/**
	 * Set appropriate listeners for every clickable variable
	 */
	private void setListeners(){
		
		//
		// - CheckBoxes
		CheckBox chatCheckBox 	 = (CheckBox)getView().findViewById(R.id.check_chat_notifications);
		CheckBox friendsCheckBox = (CheckBox)getView().findViewById(R.id.check_privacy_friends);
		CheckBox publicCheckBox  = (CheckBox)getView().findViewById(R.id.check_privacy_public);
		
		chatCheckBox.setOnCheckedChangeListener(mCheckBoxListener);
		friendsCheckBox.setOnCheckedChangeListener(mCheckBoxListener);
		publicCheckBox.setOnCheckedChangeListener(mCheckBoxListener);
		
		//
		// - Buttons
		Button termsButton 	 = (Button)getView().findViewById(R.id.button_user_terms);
		Button privacyButton = (Button)getView().findViewById(R.id.button_privacy);
		Button pictureButton = (Button)getView().findViewById(R.id.button_change_picture);
		Button aliasButton 	 = (Button)getView().findViewById(R.id.button_change_alias);
		Button mailButton 	 = (Button)getView().findViewById(R.id.button_change_mail);
		Button ageButton 	 = (Button)getView().findViewById(R.id.button_change_age);
		
		termsButton.setOnClickListener(mButtonTapListener);
		privacyButton.setOnClickListener(mButtonTapListener);
		pictureButton.setOnClickListener(mButtonTapListener);
		aliasButton.setOnClickListener(mButtonTapListener);
		mailButton.setOnClickListener(mButtonTapListener);
		ageButton.setOnClickListener(mButtonTapListener);
	}
	
	/**
	 * Will adapt the image to the size of the resource_id in the xml layout
	 * @param drawableResource
	 * @param view
	 */
	private void setDrawableResourceForLayoutId(int drawableResource, View view){
		
		view.setBackgroundResource(drawableResource);
		
		BitmapDrawable background = (BitmapDrawable)view.getBackground(); // assuming you have bg_tile as background.
		BitmapDrawable newBackground = new BitmapDrawable(background.getBitmap()) {
		    @Override
		    public int getMinimumWidth() {
		        return 0;
		    }

		    @Override
		    public int getMinimumHeight() {
		        return 0;
		    }
		};
		newBackground.setTileModeXY(background.getTileModeX(), background.getTileModeY());
		view.setBackgroundDrawable(newBackground);
	}
	// =============================================================================================== //
	// ************************************** HELPER FUNCTIONS *************************************** //
	// =============================================================================================== //
	
	// -
	// =============================================================================================== //
	// *********************************** CHECK BOX LISTENERS *************************************** //
	// =============================================================================================== //
	/**
	 * This member class will delegate the click further to tappedCheckBox function
	 */
	OnCheckedChangeListener mCheckBoxListener = new CompoundButton.OnCheckedChangeListener(){

		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			tappedCheckBox((CheckBox)buttonView, isChecked);
		}
		
	};
	
	/**
	 * Detects which of the checkboxes are tapped, and proceeds to del
	 * @param checkBox
	 * @param isChecked
	 */
	private void tappedCheckBox(CheckBox checkBox, boolean isChecked){
		
		//
		// - First we wan't to detect if the checked box is any of the friends or public check box
		// - since only one of these can be checked at a time
		CheckBox friendsCheckBox = (CheckBox)getView().findViewById(R.id.check_privacy_friends);
		CheckBox publicCheckBox  = (CheckBox)getView().findViewById(R.id.check_privacy_public);
		CheckBox chatCheckBox 	 = (CheckBox)getView().findViewById(R.id.check_chat_notifications);
		
		// - Public status OFF 
		if(checkBox.equals(friendsCheckBox) && isChecked){
			publicCheckBox.setChecked(false);
			setPublicStatus(false);
		}
		// - Public status ON
		else if(checkBox.equals(publicCheckBox) && isChecked){
			friendsCheckBox.setChecked(false);
			setPublicStatus(true);
		}
		else if(checkBox.equals(chatCheckBox)){
			setChatNotificationsOn(isChecked);
		}
		
		
	}
	
	// =============================================================================================== //
	// *********************************** CHECK BOX LISTENERS *************************************** //
	// =============================================================================================== //
	
	// =============================================================================================== //
	// ************************************** BUTTON LISTENERS *************************************** //
	// =============================================================================================== //
	
	View.OnClickListener mButtonTapListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			//
			// - We have to detect which button is pressed
			Button termsButton 	 = (Button)getView().findViewById(R.id.button_user_terms);
			Button privacyButton = (Button)getView().findViewById(R.id.button_privacy);
			Button pictureButton = (Button)getView().findViewById(R.id.button_change_picture);
			Button aliasButton 	 = (Button)getView().findViewById(R.id.button_change_alias);
			Button mailButton 	 = (Button)getView().findViewById(R.id.button_change_mail);
			Button ageButton 	 = (Button)getView().findViewById(R.id.button_change_age);
			
			if(v.equals(termsButton)){
				startTermsIntent();
			}
			else if(v.equals(privacyButton)){
				startPrivacyIntent();
			}
			else if(v.equals(pictureButton)){
				startPictureIntent();
			}
			else if(v.equals(aliasButton)){
				startAliasIntent();
			}
			else if(v.equals(mailButton)){
				startMailIntent();
			}
			else if(v.equals(ageButton)){
				startAgeIntent();
			}
		}
	};
	
	//
	// - Intent starts
	private void startTermsIntent(){
		Intent termsIntent = new Intent(getActivity(), ActivityTerms.class);
		startActivity(termsIntent);
	}
	
	private void startPrivacyIntent(){
		Intent privacyIntent = new Intent(getActivity(), ActivityPrivacy.class);
		startActivity(privacyIntent);
	}
	
	private void startPictureIntent(){
		
	}
	
	private void startAliasIntent(){
		Intent aliasIntent = new Intent(getActivity(), ActivityUpdateUserAlias.class);
		startActivity(aliasIntent);
	}
	
	private void startMailIntent(){
		Intent mailIntent = new Intent(getActivity(), ActivityUpdateUserMail.class);
		startActivity(mailIntent);
	}
	
	private void startAgeIntent(){
		Intent ageIntent = new Intent(getActivity(), ActivityUpdateUserAge.class);
		startActivity(ageIntent);
	}
	
	// =============================================================================================== //
	// ************************************** BUTTON LISTENERS *************************************** //
	// =============================================================================================== //
	
	// =============================================================================================== //
	// *************************************** API CALLS ********************************************* //
	// =============================================================================================== //
	
	private void setPublicStatus(boolean publicStatus){
		Settings.getInstance().turnChatNotificationsOn(publicStatus, true);
	}
	
	private void setChatNotificationsOn(boolean notifOn){
		Settings.getInstance().turnChatNotificationsOn(notifOn, true);
	}
	
	// =============================================================================================== //
	// *************************************** API CALLS ********************************************* //
	// =============================================================================================== //

}
 