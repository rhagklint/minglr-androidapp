package com.android.minglr.models;

import android.database.CursorJoiner.Result;

/**
 * Settings class is a singleton which handles all the settings for the user, like 
 * Privacy options, chat notifications on etc. This singleton will be responsible for making
 * appropriate api calls when changes are made to the settings
 * @author Robert
 *
 */
public class Settings {
	
	// ======================================================================================== //
	// ************************************** SINGLETON STUFF ********************************* //
	// ======================================================================================== //
	
	/**
	 * Singleton instanceObject
	 */
	private static Settings _instance;
	
	/*
	 * Init the instance, can be called publicly, but it will most likley not
	 */
	public static void initInstance(){
		if(_instance == null){
			_instance = new Settings();
		}
	}
	
	/**
	 * Retrieve the static instance of Settings
	 * @return
	 */
	public static Settings getInstance(){
		if(_instance == null){
			initInstance();
		}
		return _instance;
	}
	

	/*
	 * Singletons have private constructors
	 */
	private Settings() {
		// TODO Auto-generated constructor stub
	}
	
	// ======================================================================================== //
	// ************************************** SINGLETON STUFF ********************************* //
	// ======================================================================================== //
	
	//
	// - Functions herein will turn the chat notifications on/off etc, and send the update to 
	// - the server.
	// ======================================================================================== //
	// ************************************ CHAT SETTINGS  ************************************ //
	// ======================================================================================== //
	
	private boolean _chatNotifOn;
	/**
	 * Will set a local variable in Settings instance and update to the server for
	 * chat notifications. If updateToServer == true, an update to the api will be sent
	 * @param on
	 * @param updateToServer
	 */
	public void turnChatNotificationsOn(boolean on, boolean updateToServer){
		_chatNotifOn = on;
		
		// - Return if we don't want to update to server
		if(!updateToServer)
			return;
		
		// - Assign a new post
		ApiPost apiPost = new ApiPost();
				
		// - Allocate and assign value depending on boolean public status on
		String value;
		if(on)
			value = "1";
		else value = "0";
		
		apiPost.addParameter("push_conversation", value);
		
		apiPost.startPostDataTo("user/update", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				System.out.println("Response: " + response);
			}
		});
		
	}
	
	/**
	 * Will return the a boolean value for if chat notifications are turned on.
	 * OBS! This function will always return false if this appsession has not yet fetched the
	 * user and updated the values to Settings
	 * @return
	 */
	public boolean isChatNotificationsOn(){
		return _chatNotifOn;
	}
	
	// ======================================================================================== //
	// ************************************ CHAT SETTINGS  ************************************ //
	// ======================================================================================== //
	
	
	//
	// - Functions herein will update and display the two different privacy statuses;
	// - is_public (api name) and geofriend_visible (api name)
	// -
	// - is_public is locally controlled by _publicStatusOn and refers to what your visiblity status
	// - is for non-friends when you are eg. checked-in to a location
	// -
	// - geofriend_visible is locally controlled by _geofriendsOn and refers to what your visibility
	// - is on the map for you. If the user has it turned off, they can neither see their friends on
	// - the map
	// ======================================================================================== //
	// ********************************** PRIVACY SETTINGS * ********************************** //
	// ======================================================================================== //
	
	private boolean _publicStatusOn;
	/**
	 * Will set a local variable in Settings instane and update to the server
	 * for public status. If updateToServer == true, an update to the api will be sent
	 * @param on boolean
	 * @param updateToServer boolean
	 */
	public void turnPublicStatusOn(boolean on, boolean updateToServer){
		_publicStatusOn = on;
		
		// - Return if we don't want to update to server
		if(!updateToServer)
			return;
		
		// - Assign a new post
		ApiPost apiPost = new ApiPost();
		
		// - Allocate and assign value depending on boolean public status on
		String value;
		if(on)
			value = "1";
		else value = "0";
		
		// - Add parameter with the now correct value
		apiPost.addParameter("public", value);
		
		// - And start the post to the server
		apiPost.startPostDataTo("user/update", new ApiPostInterface() {
			@Override
			public void postDataComplete(String response) {
				System.out.println("Reponse: " + response);
			}
		});
	}
	
	/**
	 * Will return the boolean for the public status. It will be false if the user has private/friends mode.
	 * OBS! This function will always return false if the this appsession has not yet fetched the user and
	 * updated the values to Settings
	 * @return
	 */
	public boolean isPublicStatusOn(){
		return _publicStatusOn;
	}
	
	private boolean _geofriendsOn;
	
	/**
	 * Will set a local variable in Settings instance and update to server
	 * for geofriends on. If updateToServer == true, an update to the api will be sent
	 * @param on
	 * @param updateToServer
	 */
	public void turnGeofriendsOn(boolean on, boolean updateToServer){
		_geofriendsOn = on;
		
		// - Return if we don't want to update to server
		if(!updateToServer)
			return;
	}
	
	/**
	 * Will return the boolean for the geofriends status. 
	 * OBS! This function will always return false if the this appsession has not yet fetched the user and
	 * updated the values to Settings
	 * @return
	 */
	public boolean isGeofriendsOn(){
		return _geofriendsOn;
	}
	
	// ======================================================================================== //
	// ********************************** PRIVACY SETTINGS * ********************************** //
	// ======================================================================================== //

}
