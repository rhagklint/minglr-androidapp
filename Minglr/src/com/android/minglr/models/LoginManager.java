package com.android.minglr.models;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;


/**
 * This class will take care of the whole login and creation of account processes. But 
 * at the moment, this only covers the creation of accounts. 
 * @author Robert
 *
 */
public class LoginManager {

	private LoginManager() {
		// TODO Auto-generated constructor stub
	}

	// ======================================================================================== //
	// ************************************** SINGLETON STUFF ********************************* //
	// ======================================================================================== //
	
	//
	// - Private static instance
	private static LoginManager _instance;
	
	// - Init
	private static void initInstance(Context context){
		if(_instance == null){
			_instance = new LoginManager();
		}
	}
	
	/**
	 * Access method for the singleton class LoginManager
	 * @return the singleton instance of LoginManager
	 */
	public static LoginManager getInstance(){
		if(_instance == null){
			initInstance(null);
		}
		return _instance;
	}
	
	// ======================================================================================== //
	// ************************************** SINGLETON STUFF ********************************* //
	// ======================================================================================== //
	
	//
	// - Since I don't know of anything that Objective c calls completionHandlers, we have
	// - this structure instead, where we save the variables for the new user, and when
	// - HomeActivity resumes, that class whill look for a new user
	// ======================================================================================== //
	// ********************************** CREATE ACCOUNT ************************************** //
	// ======================================================================================== //
	
	/**
	 * When a new user are going to be created, we save the mail temporary so that
	 * we can access it where we need it in the HomeActivity
	 */
	private String _newUserEmail;
	
	/**
	 * Save the new userEmail
	 * @param mail
	 */
	public void setNewUserEmail(String mail){ _newUserEmail = mail; }
	
	/**
	 * When this function is usedm we will deallocate the instance variable for the email.
	 * Only use this function before you post the new user to the server
	 */
	public String getNewUserEmail(){
		
		String mail = _newUserEmail;
		
		_newUserEmail = null;
		
		return mail;
	}
	
	// =================================================================
	
	/**
	 * When a new user are going to be created, we save the password temporarily so that
	 * we can access it where we need it in the HomeActivity
	 */
	private String _newUserPassword;
	
	/**
	 * Save the new user password
	 * @param password
	 */
	public void setNewUserPassword(String password){ _newUserPassword = password; }
	
	/**
	 * When this function is used, we will deallocate the instance variabel for 
	 * the password. Only use this function before you post the new user to the server
	 * @return
	 */
	public String getNewUserPassword(){
		
		String password = _newUserPassword;
		
		_newUserPassword = null;
		
		return password;
	}
	
	
	// =================================================================
	
	/**
	 * When a new user are going to be created, we save the name temporary so that
	 * we can access it where we need it in the HomeActivity
	 */
	private String _newUserName;
	
	/**
	 * Set the name for the new user
	 * @param name
	 */
	public void setNewUserName(String name){ _newUserName = name; }
	
	/**
	 * When this function is used, we will deallocate the instance variabel for 
	 * the user name. Only use this function before you post the new user to the server
	 * @return
	 */
	public String getNewUserName(){
		
		String name = _newUserName;
		
		_newUserName = null;
		
		return name;
	}
	
	// =================================================================
	
	/**
	 * When a new user are going to be created, we save the gender temporary so that
	 * we can access it where we need it in the HomeActivity
	 */
	private String _newUserGender;
	
	/**
	 * Set gender of the new user
	 * @param gender
	 */
	public void setNewUserGender(String gender) { _newUserGender = gender; }
	
	/**
	 * When this function is used, we will deallocate the instance variabel for 
	 * the user gender. Only use this function before you post the new user to the server
	 * @return
	 */
	public String getNewUserGender(){
		
		String gender = _newUserGender;
		
		_newUserGender = null;
		
		return gender;
	}
	
	// =================================================================
	
	/**
	 * Will return true if there are variables stored for a new user in the LoginManager.
	 * (The logic for determing if the new user is valid or not is declared in here)
	 * @return
	 */
	public boolean newUserShouldBeCreated(){
		
		if(_newUserEmail != null && _newUserEmail.length() > 0 &&
		   _newUserName != null && _newUserName.length() > 0 &&
		   _newUserPassword != null && _newUserPassword.length() > 0 &&
		   _newUserGender != null && _newUserGender.length() > 0){
			
			return true;
		}
		else return false;
	}
	
	/**
	 * Will post to server the new user
	 */
	public void createNewUser(final UserCreationInterface interfaceObject){
		
		// 
		// - New user variables, will also be deallocated locally after this
		final String name   = getNewUserName();
		final String mail   = getNewUserEmail();
		final String pass   = getNewUserPassword();
		final String gender = getNewUserGender();
		
		System.out.println("New user name: " + name);
		
		ApiPost userPost = new ApiPost();
		userPost.addParameter("name", name);
		userPost.addParameter("email", mail);
		userPost.addParameter("password", pass);
		userPost.addParameter("gender", gender);
		
		userPost.startPostDataTo("user/add", new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				System.out.println("Response when creating new user: " + response);
				if(response.contains("ok")){
					
					String user_id = null;
					try {
						JSONObject responseObject = new JSONObject(response);
						user_id = responseObject.getString("user_id");
						System.out.println("ResponseObject: " + responseObject.toString());
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(user_id != null && user_id.length() > 0)
						interfaceObject.userCreationComplete(true, user_id, mail, pass);
					else
						interfaceObject.userCreationComplete(false, null, null, null);
						
				}
				else{
					interfaceObject.userCreationComplete(false, null, null, null);
				}
			}
		});
	}
	
	// =================================================================
	
	// ======================================================================================== //
	// ********************************** CREATE ACCOUNT ************************************** //
	// ======================================================================================== //
}
