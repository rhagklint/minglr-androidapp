package com.android.minglr.models;

import java.util.HashMap;

import android.R.integer;
import android.content.Context;
import android.graphics.Color;

public class CategoryHandler {

	public CategoryHandler() {
		// TODO Auto-generated constructor stub
	}
	
	//
	// - Convinience variable
	//private static String _categoriesMapKey = "categories_map";
	private static String _categoriesMapKey;
	
	public static boolean isCategorySelected(String category_id, Context context){
		
		_categoriesMapKey = context.getFilesDir().getPath().toString() + "/categories_map";
		
		// - We return false if there is no map yet
		if(!FileManager.mapExists(_categoriesMapKey)){
			System.out.println("isCategorySelected:mapDoesNotExist");
			return false;
		}
		
		HashMap <String, String> cMap = FileManager.readMapFromFile(_categoriesMapKey);
		
		if(cMap.containsKey(category_id)){
			//System.out.println("isCategorySelect:category " + category_id + " is selected");
			return true;
		}
		
		else{
			//System.out.println("isCategorySelect:category " + category_id + " is NOT selected");
			return false;
		}
		
	}
	
	public static void setCategorySelected(String category_id, Context context){
		
		_categoriesMapKey = context.getFilesDir().getPath().toString() + "/categories_map";
		//
		// - If there is no categories map yet, we create a new one
		if(!FileManager.mapExists(_categoriesMapKey)){
			
			//System.out.println("setCategorySelected:mapDoesNotExist");
			
			HashMap<String, String> cMap = new HashMap<String, String>();
			FileManager.writeMapToFile(cMap, _categoriesMapKey);
			
			return;
		}
		
		//
		// - We now load the map
		HashMap<String, String> cMap = FileManager.readMapFromFile(_categoriesMapKey);
		
		// - And put selected on it
		cMap.put(category_id, "selected");
		
		System.out.println("setCategorySelected:category " + category_id + " selected");
		
		FileManager.writeMapToFile(cMap, _categoriesMapKey);
	}
	
	public static void setCategoryUnselected(String category_id, Context context){
		
		_categoriesMapKey = context.getFilesDir().getPath().toString() + "/categories_map";
		
		if(!FileManager.mapExists(_categoriesMapKey)){
			
			System.out.println("setCategoryUnselected map does not exist");
			
			return;
		}
		
		HashMap<String, String> cMap = FileManager.readMapFromFile(_categoriesMapKey);
		
		if(cMap.containsKey(category_id)){
			
			System.out.println("setCategoryUnselected category " + category_id + "is now unselected");
			
			cMap.remove(category_id);
			
			FileManager.writeMapToFile(cMap, _categoriesMapKey);
		}
		else{
			System.out.println("setCategoryUnselected category " + category_id + "was never selected");
		}
			
	}
	
	//
	// - EXP
	// - Helper methods for getting color for unselected or selected states in a listView
	public static int getListItemColorForCategory(String category_id, Context context){
		
		if(isCategorySelected(category_id, context)){
			return Color.YELLOW;
		}
		else{
			return Color.WHITE;
		}
	}
	
}
