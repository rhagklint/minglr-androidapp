package com.android.minglr.models;


import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;

/**
 * Will handle different setting (eg for the map, like "SHOW_GEO_FRIENDS" and so on
 * @author Robert
 *
 */
public class MinglrConfig extends Activity{
	
	public enum kConfigSetting{
		SHOW_PEOPLE(0),
		SHOW_HEATMAP(1),
		SHOW_OPEN(2),
		CATEGORIES(3),
		GEO_FRIENDS(4);
		
		kConfigSetting(int value){
			this.type = value;
		}
		
		private int type;
		
		public int getValue(){
			return type;
		}
		
	}


	private static MinglrConfig _instance;
	
	//
	// - make constructor private;
	private MinglrConfig(){
		super();
		_instance = this;
	}
	
	//
	// - The get Instance method
	public static MinglrConfig getInstance(){
		if(_instance == null){
			_instance = new MinglrConfig();
		}
		return _instance;
	}
	
	//
	// - Get appliationContext method
	public Context getContext(){
		return getInstance().getApplicationContext();
	}
	
	public static Context getInstanceContext(){
		
		if(_instance == null){
			_instance = new MinglrConfig();
		}
		
		return _instance.getApplicationContext();
	}
	
    // ...
	
	public void setSettingOn(kConfigSetting setting, boolean on){
		
		System.out.println("Setting " + setting.toString() + " on: " + on);
		switch (setting) {
		case SHOW_PEOPLE:
			setShowAllOn(on);
			break;
			
		case SHOW_OPEN:
			setShowOpenOn(on);
			break;
			
		case SHOW_HEATMAP:
			setShowHeatMapOn(on);
			break;
		case GEO_FRIENDS:
			setGeoFriendsOn(on);
			break;

		default:
			break;
		}
	}
	

	//
	// - Different settings set in the view for map settings
	// - these settings will also be displayed there
	// ==================================================================================== //
	// ************************************** MAP SETTINGS ******************************** //
	// ==================================================================================== //
	
	// ============================
	// - Variables for map settings
	// ============================
	private boolean _showFriendsOn;
	private boolean _showHeatMapOn;
	private boolean _showOpenOnlyOn;
	private boolean _geoFriendsOn;
	
	// ============================
	// - Show friends
	// ============================
	private void setShowAllOn(boolean on){
		_showFriendsOn = on;
	}
	
	public boolean isShowAllOn(){
		return _showFriendsOn;
	}
	
	// ============================
	// - Heat map
	// ============================
	private void setShowHeatMapOn(boolean on){
		_showHeatMapOn = on;
	}
	
	public boolean isShowHeatMapOn(){
		return _showHeatMapOn;
	}
	
	// ============================
	// - Show open only
	// ============================
	private void setShowOpenOn(boolean on){
		_showOpenOnlyOn = on;
	}
	
	public boolean isShowOpenOnlyOn(){
		return _showOpenOnlyOn;
	}
	
	// ============================
	// - Geo friends setting
	// ============================
	private void setGeoFriendsOn(boolean on){
		_geoFriendsOn = on;
	}
	
	public boolean isGeoFriendsOn(){
		return _geoFriendsOn;
	}
	
	/**
	 * Will download the map settings and set its variables internally in the MinglrConfig 
	 * sinleton instance
	 */
	public void downloadMapSettings(){
		ApiDownload download = new ApiDownload();
		download.startDownload("user/settings", "", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				ApiObject settings = ApiObjectHandler.refineObject(result, "settings");
				Integer geofriend_visible = Integer.parseInt(settings.getObject().get("geofriend_visible"));
				System.out.println("Geofriend_visible: " + geofriend_visible);
				if(geofriend_visible == 1)
					setGeoFriendsOn(true);
				else setGeoFriendsOn(false);
			}
			
			// - Not used
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
			}
		});
	}
	
	// ==================================================================================== //
	// ************************************** MAP SETTINGS ******************************** //
	// ==================================================================================== //
	
	
}
