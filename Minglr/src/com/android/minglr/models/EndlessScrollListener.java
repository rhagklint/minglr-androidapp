package com.android.minglr.models;

import android.hardware.Camera.PreviewCallback;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

/**
 * Will be implemented in all the listViews which must have scroll to paginate, like the
 * main feed and such
 * @author Robert
 *
 */
public class EndlessScrollListener implements OnScrollListener {
	
	/*
	private boolean _isLoading = true;
	private int _previousTotal = 0;
	private int _currentPage = 0;
	private int _visibleTreshhold = 5;
	*/
	
	private EndlessScrollInterface _scrollInterface;

	public EndlessScrollListener() {
		// TODO Auto-generated constructor stub
	}
	
	public EndlessScrollListener(EndlessScrollInterface scrollInterface){
		_scrollInterface = scrollInterface;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		

		//
		// - First we check if the _scrollinterface is loading
		if(_scrollInterface.isLoading()){
			// - If the scroll interface is already downloading new data, we do nothing 
			return;
		}
		
		// 
		// - Else we check if the listView is at the bottom
		if(firstVisibleItem + visibleItemCount >= totalItemCount){
			// - We have reached the end, and want to doPaginate
			_scrollInterface.doPaginate();
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

}
