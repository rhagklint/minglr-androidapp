package com.android.minglr.models;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.ArrayList;

import com.google.maps.android.heatmaps.HeatmapTileProvider;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.provider.ContactsContract.Contacts.Data;
import android.util.Base64;

public class ApiMultipartPost extends AsyncTask<String, Integer, String> {

	private ArrayList<Bitmap> _bitmapList;
	
	static String API_ROOT = "http://api-test.minglr.se/";
 	static String REST_API_KEY = "2udn1gvz9h6f3dl22yjep8qta75a83pvhnwqumyp";
 	static String METHOD_POST = "POST";
 	static String METHOD_GET = "GET";
 	static String BOUNDARY = "---------------------------14737809831466499882746641449";
 	
 	String attachmentName = "bitmap";
 	String attachmentFileName = "bitmap.bmp";
 	String crlf = "\r\n";
 	String twoHyphens = "--";
 	String boundary =  "*****";
 	
 	String _endpoint = "feed/add_status";
 	
 	public Bitmap _bitmap;
 	public String _message;
	
	public ApiMultipartPost() {
		// TODO Auto-generated constructor stub
	}
	
	public void authenticate(){
		// ===========================================================================
		// - Auth
		// ===========================================================================
		final String userName = FlashDrive.getInstance().getCredentials().get("username");
		final String password = FlashDrive.getInstance().getCredentials().get("password");
		
		if(userName != null && password != null){
			Authenticator.setDefault(new Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication(){
					return new PasswordAuthentication(userName, password.toCharArray());
				}
			});
		}
		// ===========================================================================
		// - Auth
		// ===========================================================================		
	}
	
	public void addBitmap(Bitmap bitmap){
		if(_bitmapList == null)
			_bitmapList = new ArrayList<Bitmap>();
		
		_bitmapList.add(bitmap);
	}
	
	public void startConnectionWithEndpoint(String endpoint) throws IOException{
		
		authenticate();
		
		// - Url of the connection
		String completeURL = API_ROOT.concat(endpoint);
		URL url = new URL(completeURL);
		
		// - Connection
		HttpURLConnection connection = null;
		connection = openDefaultConnection(connection, url);
		
		// - Start content wrapper
		DataOutputStream request = new DataOutputStream(connection.getOutputStream());
		
		// - Write data
		writeDataForVariable("text", "AndroidTest", request, connection);
		
		// - End content wrapper
		endContentWrapperForRequest(request);
		
		disconnect(connection, request);
	}
	
	public void postStatusUpdate(String message, Bitmap bitmap, String endpoint) throws IOException{
		authenticate();
		
		// - Url of the connection
		String completeURL = API_ROOT.concat(endpoint);
		URL url = new URL(completeURL);
		
		// - Connection
		HttpURLConnection connection = null;
		connection = openDefaultConnection(connection, url);
		
		// - Start content wrapper
		DataOutputStream request = new DataOutputStream(connection.getOutputStream());	
		
		writeDataForVariable("text", message, request, connection);
		
		// - If we have bitmap, we write that data too
		if(bitmap != null){
			writeDataForBitmap(bitmap, "1.jpg", request, connection);
		}
		
		// - End content wrapper
		endContentWrapperForRequest(request);
		
		disconnect(connection, request);
	}
	
	/**
	 * Convert Bitmap to ByteBuffer
	 * @param bitmap
	 * @return
	 */
	private byte[] convertBitmapToBytes(Bitmap bitmap){
		
		byte[] pixels = new byte[bitmap.getWidth() * bitmap.getHeight()];
		for (int i = 0; i < bitmap.getWidth(); ++i) {
		    for (int j = 0; j < bitmap.getHeight(); ++j) {
		        //we're interested only in the MSB of the first byte, 
		        //since the other 3 bytes are identical for B&W images
		        pixels[i + j] = (byte) ((bitmap.getPixel(i, j) & 0x80) >> 7);
		    }
		}
		
		return pixels;
	}
	
	
	/**
	 * Open default connection
	 * @param connection
	 * @param url
	 * @return
	 * @throws IOException
	 */
	public HttpURLConnection openDefaultConnection(HttpURLConnection connection, URL url) throws IOException{
		
		connection = (HttpURLConnection)url.openConnection();
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		
		connection.setRequestMethod("POST");
		//connection.setRequestProperty("Cache-Control", "no-cache");
		connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
		connection.setRequestProperty("X-API-KEY", REST_API_KEY);
		
		System.out.println("Connection properties: " + connection.getRequestProperties().toString());
		
		return connection;
	}
	
	/**
	 * Close connection and flush and close the request
	 * @param connection
	 * @param request
	 * @throws IOException
	 */
	public void disconnect(HttpURLConnection connection, DataOutputStream request) throws IOException{
		// - Flush
		request.flush();
    	request.close();
    	
    	//System.out.println("Response: " +getResponseFromConnection(connection));
    	System.out.println("ResponseMessage: " + connection.getResponseMessage());
    	System.out.println("ResponseCode: " + connection.getResponseCode());
    	connection.disconnect();		
	}
	
	/**
	 * Write data For variable
	 * @param variable
	 * @param content
	 * @param request
	 * @param connection
	 * @throws IOException
	 */
	public void writeDataForVariable(
			String variable, 
			String content, 
			DataOutputStream request, 
			HttpURLConnection connection) throws IOException{
		
		String byteString = "\r\n--" + BOUNDARY + "\r\n" + "Content-Disposition: form-data; name=\"" +
	            variable + 
	            	"\"" +
	           this.crlf +
	           this.crlf +
	           		//"\"" +
	 			 content + 
   				    //"\"" + 
   			   this.crlf + this.crlf;
		
		System.out.println("Data String: " + byteString);
		request.writeBytes(byteString);
	}
	
	/**
	 * When the multipart form has no more content, we end the content with this
	 * @param request
	 * @throws IOException
	 */
	public void endContentWrapperForRequest(DataOutputStream request) throws IOException{
		//
		// - End content wrapper
		String endWrapperString = this.crlf + this.twoHyphens + BOUNDARY + this.twoHyphens + this.crlf;
		/*request.writeBytes(this.crlf);
		request.writeBytes(this.twoHyphens + BOUNDARY + this.twoHyphens + this.crlf);
		*/
		request.writeBytes(endWrapperString);
		System.out.println("EndWrapperString: " + endWrapperString);
	}
	
	public void writeDataForBitmap(
			Bitmap bitmap,
			String filename,
			DataOutputStream request, 
			HttpURLConnection connection) throws IOException{
		
		
		// - Compress the bitmap
		/*
		ByteArrayOutputStream compressStream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 10, compressStream);
		byte[] array = compressStream.toByteArray();
		bitmap = BitmapFactory.decodeByteArray(array, 0, array.length);
		*/
		
		String jpegString = new String(convertBitmapToBytes(bitmap), "UTF-8");
		//
		// - Start content wrapper
		String byteString = "\r\n--" + BOUNDARY + "\r\n" + "Content-Disposition: form-data; name=\"images[]\"; filename=\"" +
	            filename + 
	            	"\"" +
	           this.crlf +
	           "Content-Type: image/jpeg\r\n\r\n" + jpegString;
	
		System.out.println("ByteString: " + byteString);
		
		// - Write
		request.writeBytes(byteString);
		//request.write(convertBitmapToBytes(bitmap));
		//System.out.println("BitmapBytes: " +)

	}

	@Override
	protected String doInBackground(String... arg0) {
		
		try {
			if(_bitmap == null)
				startConnectionWithEndpoint(_endpoint);
			else if(_bitmap != null && _message != null){
				postStatusUpdate(_message, _bitmap, "feed/add_status");
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
    protected void onProgressUpdate(Integer... progress) {
    	// - TODO
    	System.out.println("Progress " + progress);
    }
     
    @Override
    protected void onPostExecute(String result) {
    	// Publish result
    	
    	System.out.println("OnPostExecute: " + result);
    	
   }
	
	
	
	
	
	
	
	
    
    
    
    
    
    
    
    /*
	private String getResponseFromConnection(HttpURLConnection httpURLConnection) throws IOException{
		InputStream responseStream = new BufferedInputStream(httpURLConnection.getInputStream());

		BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
		String line = "";
		StringBuilder stringBuilder = new StringBuilder();
		while ((line = responseStreamReader.readLine()) != null)
		{
		    stringBuilder.append(line).append("\n");
		}
		responseStreamReader.close();

		String response = stringBuilder.toString();
		return response;
	}
	*/
	
	
	
	
	
	/*
	 * // ===========================================================================
		// - Auth
		// ===========================================================================
		final String userName = FlashDrive.getInstance().getCredentials().get("username");
		final String password = FlashDrive.getInstance().getCredentials().get("password");
		
		if(userName != null && password != null){
			Authenticator.setDefault(new Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication(){
					return new PasswordAuthentication(userName, password.toCharArray());
				}
			});
		}
		// ===========================================================================
		// - Auth
		// ===========================================================================
		
		
		// - Url of the connection
		String completeURL = API_ROOT.concat(endpoint);
		URL url = new URL(completeURL);
		
		// - Connection
		HttpURLConnection connection = null;
		connection = (HttpURLConnection)url.openConnection();
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Cache-Control", "no-cache");
		connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);
		connection.setRequestProperty("X-API-KEY", REST_API_KEY);
		
		
		// - Start content wrapper
		DataOutputStream request = new DataOutputStream(connection.getOutputStream());
		writeDataForVariable("text", "AndroidTest", request, connection);

		request.writeBytes(this.twoHyphens + BOUNDARY + this.crlf);
		request.writeBytes("Content-Disposition: form-data; name=\"" + 
												 this.attachmentName + 
												 	"\";filename=\"" + 
											 this.attachmentFileName + 
											   				    "\"" + 
											   			   this.crlf);
		
		request.writeBytes(this.crlf);
		for(Bitmap bitmap : _bitmapList){
			request.write(convertBitmapToBytes(bitmap));
		}
		
		// - End content wrapper
		request.writeBytes(this.crlf);
		request.writeBytes(this.twoHyphens + BOUNDARY + this.twoHyphens + this.crlf);
		
		// -Flush output buffer:
		request.flush();
		request.close();
	 */

}
