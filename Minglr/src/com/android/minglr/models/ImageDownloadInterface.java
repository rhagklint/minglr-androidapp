package com.android.minglr.models;

import java.util.ArrayList;

import android.graphics.drawable.Drawable;

public interface ImageDownloadInterface {
	
	public void imageListFinished(ArrayList <Drawable> imageList);

}
