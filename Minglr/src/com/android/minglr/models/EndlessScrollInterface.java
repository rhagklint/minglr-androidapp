package com.android.minglr.models;

public interface EndlessScrollInterface {
	
	/**
	 * Gets called from the EndlessScrollListener whenever a listView
	 * should update it's content, when the user scrolls down to paginate
	 */
	public void doPaginate();
	
	/**
	 * The ScrollListener will call this to see if the interface class
	 * is downloadin new data
	 * @return
	 */
	public boolean isLoading();

}
