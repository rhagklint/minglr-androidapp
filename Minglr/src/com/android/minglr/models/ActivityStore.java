package com.android.minglr.models;


import com.example.android.navigationdrawerexample.ActivityFeed;

import android.app.Activity;
import android.content.Intent;

public class ActivityStore {

	public ActivityStore() {
		// TODO Auto-generated constructor stub
	}
	
	public static Activity getActivity(int activityIndex){
		
		// - The selected activity which we return
		Activity selectedActivity;
		
		// - Activity depends upon enum
		if(activityIndex == 0){
			// - TODO
		}
		
		selectedActivity = new ActivityFeed();
		
		// - Return
		return selectedActivity;
	}
	
	// - Get intent for index
	public static Intent getIntentForActivityIndex (Activity mainActivity, int index){
		
		// - Get the activity 
		Activity selectedActivity = ActivityStore.getActivity(index);
		
		// - Init the intent with the selected activity class property and main activity
		Intent intent = new Intent(mainActivity, ActivityFeed.class);
		
		return intent;
	}

}
