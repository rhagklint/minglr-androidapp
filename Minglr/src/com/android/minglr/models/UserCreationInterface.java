package com.android.minglr.models;

public interface UserCreationInterface {
	
	/**
	 * Will be called when the user creation is complete.
	 * @param success declares the outcome. 
	 */
	public void userCreationComplete(Boolean success, String user_id, String mail, String pass);

}
