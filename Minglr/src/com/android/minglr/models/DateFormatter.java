package com.android.minglr.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

	public DateFormatter() {
		// TODO Auto-generated constructor stub
	}
	
	public static Date dateFromString(String dateString) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.parse(dateString);
	}
	
	public static void printDifference(Date startDate, Date endDate){
		 
		//milliseconds
		long different = endDate.getTime() - startDate.getTime();
 
		System.out.println("startDate : " + startDate);
		System.out.println("endDate : "+ endDate);
		System.out.println("different : " + different);
 
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;
 
		long elapsedDays = different / daysInMilli;
		different = different % daysInMilli;
 
		long elapsedHours = different / hoursInMilli;
		different = different % hoursInMilli;
 
		long elapsedMinutes = different / minutesInMilli;
		different = different % minutesInMilli;
 
		long elapsedSeconds = different / secondsInMilli;
 
		System.out.printf(
		    "%d days, %d hours, %d minutes, %d seconds%n", 
		    elapsedDays,
		    elapsedHours, elapsedMinutes, elapsedSeconds);
 
	}
	
	public static String readableTimeDifference(Date startDate, Date endDate){
		long different = endDate.getTime() - startDate.getTime();
		
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;
		
		long elapsedDays = different / daysInMilli;
		different = different % daysInMilli;
		
		if(elapsedDays > 30){
			return "Long time ago";
		}
		
		if(elapsedDays > 0){
			return elapsedDays + " days ago";
		}
 
		long elapsedHours = different / hoursInMilli;
		different = different % hoursInMilli;
		
		if(elapsedHours > 0){
			return elapsedHours + " hours ago";
		}
 
		long elapsedMinutes = different / minutesInMilli;
		different = different % minutesInMilli;
		
		if(elapsedMinutes > 0){
			return elapsedMinutes + " minutes ago";
		}
		
		else return "Just now";
 
		//long elapsedSeconds = different / secondsInMilli;
	}
 

}
