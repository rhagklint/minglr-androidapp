package com.android.minglr.models;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;

import android.R.string;
import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class FileManager{

	public FileManager() {
		// TODO Auto-generated constructor stub
	}
	
	// ************************************************************ //
	// ******************** WRITE TO FILE ************************* //
	// ************************************************************ //
		
	public void writeToFile(String data, String fileName, Context context){
		try{
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("categories.txt", Context.MODE_PRIVATE));
		}
		catch(IOException e){
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}
	
	/**
	 * Writes a hashmap to file
	 * @param map is the hashmap we write to file
	 * @param fileName is the name of the file, so that we later can read it
	 */
	public static void writeMapToFile(HashMap <String, String> map, String fileName){
		
		try{
			File file = new File(fileName);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			
			objectOutputStream.writeObject(map);
			objectOutputStream.flush();
			objectOutputStream.close();
			fileOutputStream.close();
			
		}
		catch(Exception e){
			Log.e("Exception", "Error writing object: " + e.toString());
		}
	}
	
	public static void writeApiObjectToFile(ApiObject object, String fileName, Context context){
		String fixedFileName = context.getFilesDir().getPath().toString().concat(fileName);
		writeMapToFile((HashMap<String, String>)object.getObject(), fixedFileName);
	}
		
	// ************************************************************ //
	// ******************** WRITE TO FILE ************************* //
	// ************************************************************ //

	// ************************************************************ //
	// ******************** READ FROM FILE ************************ //
	// ************************************************************ //
		
	public static String readFromFile(String fileName){
		
		return "Hej";
	}
	
	// - TODO - se what the uncheched warning is about, maybe unsuppress it
	@SuppressWarnings("unchecked")
	public static HashMap<String, String> readMapFromFile(String fileName){
		
		HashMap <String, String> mapInFile = null;
		
		try{
			File readFile = new File(fileName);
			FileInputStream fileInputStream = new FileInputStream(readFile);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			
			mapInFile = (HashMap <String, String>) objectInputStream.readObject();
			
			objectInputStream.close();
			fileInputStream.close();
			
		}
		catch(Exception e){
			Log.e("Exception", "Error reading object: " + e.toString());
		}
		
		return mapInFile;
	}
	
	public static boolean mapExists(String fileName){
		
		//HashMap<String, String> map = readMapFromFile(fileName);
		File file = new File(fileName);
		
		if(!file.exists())
			return false;
		else return true;
	}
	
	/**
	 * 
	 * @param fileEndName The name of the file, disregardning the app directory
	 * @param context, we need a Context related class to make the call to the directory
	 * @return a usable filename for the filemanager
	 */
	public static String getAppendedFileName(String fileEndName, Context context){
		return context.getFilesDir().getPath().toString().concat(fileEndName);
	}
	
	// ************************************************************ //
	// ******************** READ FROM FILE ************************ //
	// ************************************************************ //
}
