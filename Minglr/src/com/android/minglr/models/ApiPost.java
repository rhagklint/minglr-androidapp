package com.android.minglr.models;

import org.apache.http.message.BasicNameValuePair;

import android.R.string;
import android.graphics.Bitmap;

import com.android.minglr.tasks.ApiPostTask;

/*
 * This class should be used as an instance when the application has to post 
 * something to the api/server. The posting class will leave a pointer to itself via
 * ApiPostInterface.
 * 
 */
public class ApiPost {
	
	ApiPostInterface _objectPoster;
	PostDataTask _postTask;
	static String _baseUrlString = "http://api-test.minglr.se/";

	public ApiPost() {
		// TODO Auto-generated constructor stub
	}
	
	//
	// - URL post string
	String _urlPostString;
	public void setUrlPostString(String urlString){
		
		if(!urlString.contains("minglr")){
			String completeString = _baseUrlString.concat(urlString);
			_urlPostString = completeString;
			
		}
		else{
			_urlPostString = urlString;
		}
	}
	
	//
	// _ Add parameter
	public void addParameter(BasicNameValuePair param){
		if(_postTask == null)
			_postTask = new PostDataTask();
		
		_postTask.addParameter(param);
	}
	

	/**
	 * Add a parameter to the post request/instance
	 * @param key, name of the variable we post to the api
	 * @param value, the String that is posted to the api
	 */
	public void addParameter(String key, String value){
		if(_postTask == null){
			_postTask = new PostDataTask();
		}
		
		_postTask.addParameter(new BasicNameValuePair(key, value));
	}
	
	
	// - GetDataTask to fetch data
	private class PostDataTask extends ApiPostTask{
		@Override
		protected void onPostExecute(String result) {
			
			_objectPoster.postDataComplete(result);
		}
	}
	
	public void startPostDataTo(String endPointUrl, ApiPostInterface receiver){
		
		String completeUrl;
		
		if(!endPointUrl.contains("minglr")){
			completeUrl = _baseUrlString.concat(endPointUrl);
		}
		else{
			completeUrl = endPointUrl;
		}
		
		_postTask.setPostRequestURL(completeUrl);
		_objectPoster = receiver;
		_postTask.execute();
		
	}
	
	public void postBitmapWithMessageToEndEndpoint(Bitmap bitmap, String message, String endpoint){
		
		String completeUrlString;
		completeUrlString = _baseUrlString.concat(endpoint);
	}
	

}
