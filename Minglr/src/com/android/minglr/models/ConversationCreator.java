package com.android.minglr.models;

import java.util.ArrayList;

import android.content.Context;

/**
 * This class will have an ArrayList of users which are supposed to create a new conversation.
 * An activity or class can retrieve that list, but once that is done, the list will be emptied
 * from this instance. This is so that we don't create multiple copies of the same conversation if
 * some process is made again. 
 * @author Robert
 *
 */
public class ConversationCreator {

	// - Singleton instance
	private static ConversationCreator _instance;
	
	private ArrayList<ApiObject> _friendsForConversation = new ArrayList<ApiObject>();
		
	public static void initInstance(){
		
		if(_instance == null){
			_instance = new ConversationCreator();
		}
	}
	
	public static ConversationCreator getInstance(){
		initInstance();
		return _instance;
	}

	// - Singleton private constructor
	private ConversationCreator() {
		// TODO Auto-generated constructor stub
	}	
		
		
	// ========================================================================================== //
	// ******************************* INSERTION AND EXTRACTION ********************************* //
	// ========================================================================================== //
	
	public void insertFriendIntoConversation(ApiObject newFriend){
		_friendsForConversation.add(newFriend);
	}
	
	public void removeFriendFromConversation(ApiObject removeFriend){
		_friendsForConversation.remove(removeFriend);
	}
	
	/**
	 * Will returnt he ArrayList containing all the users eligeble for a new conversation, but also remove them
	 * @return
	 */
	public ArrayList<ApiObject> retrieveFriendsForNewConversation(){
		
		ArrayList<ApiObject> tempArrayList = new ArrayList<ApiObject>(_friendsForConversation);
		_friendsForConversation.removeAll(_friendsForConversation);
		//
		// - Output
		System.out.println("TempArray size: " + tempArrayList.size());
		System.out.println("FriendsForConversation size: " + _friendsForConversation.size());
		
		// - Return
		return tempArrayList;
	}
	
	/**
	 * Will tell the caller if there is members for a new conversation within this instance
	 * @return
	 */
	public boolean hasNewConversationMembers(){
		if(_friendsForConversation.size() > 0)
			return true;
		else return false;
	}
	
	// ========================================================================================== //
	// ******************************* INSERTION AND EXTRACTION ********************************* //
	// ========================================================================================== //

}
