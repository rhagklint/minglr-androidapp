package com.android.minglr.models;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

import android.R.integer;

//
// - Wrapper class to get values from a hashmap (specifacally ApiObject)
// - Depending on subclass of api object, we will return different stuff
public class ApiObjectHandler {

	public ApiObjectHandler() {
		// TODO Auto-generated constructor stub
	}
	
	

	/**
	 * This function will take an ApiObject and return another ApiObject from inside the original 
	 * based on the variable name
	 * @param rawObject the full object which contains other objects in it
	 * @param variableName the variable name of the value you want returned
	 * @return a new ApiObject
	 */
	public static ApiObject refineObject(ApiObject rawObject, String variableName){
		
		String jsonString = rawObject.getObject().get(variableName);
		ApiObject refinedObject = null;
		
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			refinedObject = new ApiObject(jsonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return refinedObject;
	}
	
	
	// *************************************************************** //
	// ------------------------- USER-STUFF -------------------------- //
	// *************************************************************** //
	
	public static class UserObject{
		
		
		// *************************************************************** //
		// ------------------------- DEFAULT ----------------------------- //
		// *************************************************************** //
		
		public static String getId(ApiObject apiObject){
			return apiObject.getObject().get("user_id");
		}
		
		public static String getName(ApiObject apiObject){
			
			// - The most common way to receive name from the api
			String userName = apiObject.getObject().get("user_name");
			
			// - But sometimes there is a nother variable name, like "name"
			if(userName == null){
				userName = apiObject.getObject().get("name");
			}
			
			return userName;
		}
		
		public static String getBirthday(ApiObject apiObject){
			return apiObject.getObject().get("birthday");
		}
		
		public static String getGender(ApiObject apiObject){
			if(apiObject.getObject().get("gender") == "m")
				return "Man";
			else if(apiObject.getObject().get("gender") == "f")
				return "Woman";
			else
				return "Unknown";
		}
		
		public static LatLng getCoordinate(ApiObject apiObject){
			String latitude = apiObject.getObject().get("latitude");
			String longitude = apiObject.getObject().get("longitude");
			
			if(latitude != null && longitude != null){
				System.out.println("Lat: " + latitude + " Lon: " + longitude);
			}
			
			
			if(latitude == null || longitude == null || latitude == "null" || longitude == "null"){
				return null;
			}
			
			float fLat = Float.parseFloat(latitude);
			float fLon = Float.parseFloat(longitude);
			
			LatLng latLng = new LatLng(fLat, fLon);
			return latLng;
		}

		/**
		 * Returns the User object in case the handled object is the complete
		 * json base object
		 * @param apiObject, the base apiObject
		 * @return, the scaled down user object
		 */
		public static ApiObject getBaseUserObject(ApiObject apiObject){
			String jsonString = apiObject.getObject().get("user");
			ApiObject userObject = null;
			try {
				JSONObject userJson = new JSONObject(jsonString);
				userObject = new ApiObject(userJson);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return userObject;
		}
		// *************************************************************** //
		// ------------------------- DEFAULT ----------------------------- //
		// *************************************************************** //
		
		
		// --------------------------------------------------------------- //
		
		
		
		// --------------------------------------------------------------- //
		
	}
	
	// *************************************************************** //
	// ------------------------- USER-STUFF -------------------------- //
	// *************************************************************** //
	
	
	// --------------------------------------------------------------- //
	
	
	// *************************************************************** //
	// ------------------------- FEED CONTENT------------------------- //
	// *************************************************************** //
	
	public static class FeedObject{
		
		// *************************************************************** //
		// ------------------------- DEFAULT ----------------------------- //
		// *************************************************************** //
				
		public static String getContentMessage(ApiObject apiObject){
			return apiObject.getObject().get("text");
		}
		
		public static int getNiceCount(ApiObject apiObject){
			String valueString = apiObject.getObject().get("nice_count");
			if(valueString == null)
				return 0;
			return Integer.parseInt(valueString);
		}
		
		public static int getCommentCount(ApiObject apiObject){
			String valueString = apiObject.getObject().get("comment_count");
			if(valueString == null)
				return 0;
			return Integer.parseInt(valueString);
		}
		
		public static ArrayList <ApiObject> getCommentList(ApiObject apiObject){
			return getNestedArrayList(apiObject, "comments");
		}
		
		/**
		 * Will, on the instance when you have the whole JSONObject which has a feed
		 * return the ArrayList of the variable "feed"
		 * @param apiObject, the apiObject which we fetch this from
		 * @return the resulted ArrayList of feed objects
		 */
		public static ArrayList<ApiObject> getFeedList(ApiObject apiObject){
			return getNestedArrayList(apiObject, "feed");
		}
		
		/**
		 * A helper function to return a nested ArrayList <ApiObject> from
		 * a base ApiObject
		 * @param apiObject, the base object
		 * @param variableName, the variable name of the ArrayList
		 * @return, the wanted nested ArrayList within the json
		 */
		private static ArrayList <ApiObject> getNestedArrayList (ApiObject apiObject, String variableName){
			
			ArrayList <ApiObject> arrayList = new ArrayList <ApiObject> ();
			
			try{
				
				JSONArray jsonArray = apiObject.getJSON().getJSONArray(variableName);
				
				// - Loop thru them all
	        	for(int i = 0; i < jsonArray.length(); i++){
	        		
	        		// - Current jsonObject
	        		JSONObject jsonObject = (JSONObject) jsonArray.get(i);
	        		String commentString = jsonArray.getString(i);
	        		
	        		// - Converted to apiObject
	        		ApiObject object = new ApiObject(jsonObject);
	        		
	        		arrayList.add(object);
	        		
	        	}
			} catch(JSONException e ){
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("JsonObject could not be created in ApiObjectHandler.FeedObject.getCommentsList()");
			}
			
			return arrayList;
		}
		
		public static boolean getFeedObjectUserLiked(ApiObject apiObject){
			String valueString = apiObject.getObject().get("has_niced");
			return Boolean.parseBoolean(valueString);
		}
		
		public static String getTimestamp(ApiObject apiObject){
			return apiObject.getObject().get("timestamp");
		}
		
		public static String getId(ApiObject apiObject){
			return apiObject.getObject().get("status_id");
		}
		
		
		// *************************************************************** //
		// ------------------------- DEFAULT ----------------------------- //
		// *************************************************************** //
		
		
		// --------------------------------------------------------------- //
		
		
		// *************************************************************** //
		// ------------------------- IMAGES ------------------------------ //
		// *************************************************************** //
		
		// - TODO (unfinished methods)
		public static class Images{
			
			// - Returns an arrayList of string for the choses keyword (eg. "orignal" or "thumb"
			// - from a given jsonArray
			private static ArrayList <String> getArrayForKeyword(JSONArray jsonArray, String keyword){
				
				// - ArrayList which we will return 
				// - Will have the correct string appended to it
				ArrayList <String> arrayList = new ArrayList <String> ();
				
				// - Loop thru all the jsonobjects
				for(int i = 0; i < jsonArray.length(); i++){
					
					// - JsonObject of one image
					JSONObject imageObject = new JSONObject();
					
					try {
						// - Get the imageObject
						imageObject = (JSONObject) jsonArray.get(i);
						
						String imageString = imageObject.getString(keyword);
						arrayList.add(imageString);
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				return arrayList;
			}
									
			// - Gets an arrayList of strings for the original images
			public static ArrayList <String> getImageStringArray(ApiObject apiObject){
				
				// - JsonArray
				JSONArray jsonArray;
				
				// _ The return array
				ArrayList <String> arrayList = new ArrayList <String> ();
				
				// - Try for the creation of jsonArray
				try {
					
					// - Create the jsonArray of images
					jsonArray = new JSONArray(apiObject.getObject().get("images"));
					
					// - Create array of strings from the jsonArray
					arrayList = getArrayForKeyword(jsonArray, "original");
					
				} catch (JSONException e) {
					// TODO: handle exception
				}
				
				return arrayList;
			}

			// - Gets an arrayList of strings for the thumb images
			public static ArrayList <String> getImageThumbStringArray(ApiObject apiObject){
				
				// - JsonArray
				JSONArray jsonArray;
				
				// _ The return array
				ArrayList <String> arrayList = new ArrayList <String> ();
				
				// - Try for the creation of jsonArray
				try {
					//
					// - Does this key exist?
					String jsonTestString = apiObject.getObject().get("images");
					if(jsonTestString == null)
						return arrayList;
					
					// - Create the jsonArray of images
					jsonArray = new JSONArray(apiObject.getObject().get("images"));
					
					// - Create array of strings from the jsonArray
					arrayList = getArrayForKeyword(jsonArray, "thumb");
					
				} catch (JSONException e) {
					// TODO: handle exception
				}
				
				return arrayList;
			}
			
			// - Returns the count of images
			public static int getImageCount(ApiObject apiObject){
				return getImageStringArray(apiObject).size();
			}
			
		}
		
		// *************************************************************** //
		// ------------------------- IMAGES ------------------------------ //
		// *************************************************************** //
		
		
		// --------------------------------------------------------------- //
	}
	
	// *************************************************************** //
	// ------------------------- FEED CONTENT------------------------- //
	// *************************************************************** //
	

	// --------------------------------------------------------------- //
	
	// *************************************************************** //
	//-------------------- CONVERSATION CONTENT ---------------------- //
	// *************************************************************** //
	
	public static class ConversationObject{
		public static String getId(ApiObject apiObject){
			return apiObject.getObject().get("conversation_id");
		}
		
		public static String getLocationId(ApiObject apiObject){
			return apiObject.getObject().get("location_id");
		}
		
		public static String getLocationName(ApiObject apiObject){
			return apiObject.getObject().get("location_name");
		}
		
		public static ArrayList<ApiObject> getMembers (ApiObject apiObject){
			
			// - The arrayList which will be returned
			ArrayList <ApiObject> memberList = new ArrayList <ApiObject>();
			
			//
			// - The whole jsonObject
			try {
				
				JSONArray jsonArray = apiObject.getJSON().getJSONArray("members");
				
				// - Loop thru them all
	        	for(int i = 0; i < jsonArray.length(); i++){
	        		
	        		// - Current jsonObject
	        		JSONObject jsonObject = (JSONObject) jsonArray.get(i);
	        		
	        		// - Converted to apiObject
	        		ApiObject newObject = new ApiObject(jsonObject);
	        		
	        		memberList.add(newObject);
	        		
	        		//System.out.println("New Object: " + jsonObject);
	        		System.out.println("-----");
	        	}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return memberList;
		}
	}
	
	// *************************************************************** //
	//-------------------- CONVERSATION CONTENT ---------------------- //
	// *************************************************************** //
	
	// *************************************************************** //
	//--------------------- LOCATION CONTENT ------------------------- //
	// *************************************************************** //
	
	/**
	 * Contains static calls which return specific parts of the
	 * ApiObject which is treated as a location
	 * @author Robert
	 *
	 */
	public static class LocationObject{
		
		/**
		 * Get the id of the location
		 * @param apiObject
		 * @return
		 */
		public static String getId(ApiObject apiObject){
			return apiObject.getObject().get("location_id");
		}
		
		/**
		 * Get the name of the location
		 * @param apiObject
		 * @return
		 */
		public static String getName(ApiObject apiObject){
			return apiObject.getObject().get("name");
		}
		
		/**
		 * Get the coordinate of a location object
		 * @param apiObject
		 * @return
		 */
		public static LatLng getCoordinate(ApiObject apiObject){
			String latitude = apiObject.getObject().get("latitude");
			String longitude = apiObject.getObject().get("longitude");
			
			if(latitude != null && longitude != null){
				System.out.println("Lat: " + latitude + " Lon: " + longitude);
			}
			
			
			if(latitude == null || longitude == null || latitude == "null" || longitude == "null"){
				return null;
			}
			
			float fLat = Float.parseFloat(latitude);
			float fLon = Float.parseFloat(longitude);
			
			LatLng latLng = new LatLng(fLat, fLon);
			return latLng;
		}
		
		/**
		 * Get an array list of the visitors of a specific location
		 * @param locationObject is the ENTIRE location object not just the "visitors" part
		 * @return
		 */
		public static ArrayList <ApiObject> getVisitorsList(ApiObject locationObject){
			
			//
			// - The arrayList which will be returned
			ArrayList <ApiObject> visitorList = new ArrayList<ApiObject>();
			
			//
			// - try 
			try{
				
				// - Since the locationObject is the ENTIRE locaiton object, we first have to fetch
				// - the visitors object, which contains serveral things other than the "users" array in it
				// - (eg. statistics of the visitors)
				JSONArray jsonArray = locationObject.getJSON().getJSONObject("visitors").getJSONArray("users");
				
				System.out.println("Visitors json array: " + jsonArray.toString());
				
				// - Loop thru them all
				for(int i = 0; i < jsonArray.length(); i++){
					
					// - Current jsonObject
					JSONObject jsonObject = (JSONObject) jsonArray.get(i);
					
					// - Convert to apiObject
					ApiObject convertedObject = new ApiObject(jsonObject);
					
					visitorList.add(convertedObject);
				}
				
			} catch (JSONException e){
				e.printStackTrace();
			}
			
			// - Return the finished product
			return visitorList;
		}
		
		/**
		 * Returns the profile picture url for the given "size", which is either 'thumb' or 'origninal'
		 * @param object
		 * @param size set to 'thumb' or 'original'
		 * @return
		 */
		public static String getProfilePicture(ApiObject object, String size){
			
			// - The string which will be assigned the return value in the try/catch clause
			String profileURLString = null;
			try {
				
				// - Get the main hash map "profile_picture"
				JSONObject profile_picture = object.getJSON().getJSONObject("profile_picture");
				
				// - Detach and getString with the "size" parameter (thumb or original)
				profileURLString = profile_picture.getString(size);
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			// - Returns string
			return profileURLString;
		}
	}
	
	// *************************************************************** //
	//--------------------- LOCATION CONTENT ------------------------- //
	// *************************************************************** //
	
	// *************************************************************** //
	//--------------------- CATEGORY CONTENT ------------------------- //
	// *************************************************************** //
	
	public static class CategoryObject{
		
		public static String getId(ApiObject apiObject){
			return apiObject.getObject().get("category_id");
		}
		public static String getName(ApiObject apiObject){
			return apiObject.getObject().get("name_en");
		}
	}
	
	// *************************************************************** //
	//--------------------- CATEGORY CONTENT ------------------------- //
	// *************************************************************** //
	
	// *************************************************************** //
	// ------------------------- IMAGES CLASS ------------------------ //
	// *************************************************************** //

	public static class Images{
		
		// - Get profile HashMap / jsonObject
		public static JSONObject getProfileImageHashMap(ApiObject apiObject){
			String jsonProfileString = apiObject.getObject().get("profile_picture");
			//System.out.println("jsonProfileString:" + jsonProfileString);
			JSONObject profileObject = null;
			
			if(jsonProfileString == null){
				//System.out.println("jsonProfileString is null");
				return null;
			}
			if(jsonProfileString.length() == 0){
				//System.out.println("jsonProfileString has no length");
				return new JSONObject();
			}
			
			try {
				profileObject = new JSONObject(jsonProfileString);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			return profileObject;
			//return (JSONObject) apiObject.getObject().get("profile_image");
		}
		
		// - get original profile image
		public static String getProfileOriginalImageString(ApiObject apiObject){
			
			String original = null;
			try {
				original = getProfileImageHashMap(apiObject).getString("original");
				System.out.println("Orignal is set");
				if(original == null){
					System.out.println("original is null");
					return null;
				}
				
				if(original.length() == 0){
					System.out.println("original is empty");
					return null;
				}
				
			} catch (JSONException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return original;
		}
		
		//
		// - Get profile image
		public static String getCategoryImageString(ApiObject apiObject){
			String imageString = apiObject.getObject().get("picture");
			return imageString;
		}
		
		//
		public static String getProfileThumbImageString(ApiObject apiObject){
			
			String thumbString = null;
			try{
				thumbString = getProfileImageHashMap(apiObject).getString("thumb");
				if(thumbString == null)
					return null;
				
			} catch (JSONException e){
				
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
	}		
			
	// *************************************************************** //
	// ------------------------- IMAGES CLASS ------------------------ //
	// *************************************************************** //
}
