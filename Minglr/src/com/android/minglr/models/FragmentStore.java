package com.android.minglr.models;

import com.android.minglr.fragments.Fragment_CheckIn;
import com.android.minglr.fragments.Fragment_ConversationList;
import com.android.minglr.fragments.Fragment_Feed;
import com.android.minglr.fragments.Fragment_FriendList;
import com.android.minglr.fragments.Fragment_ProfileView;
import com.android.minglr.fragments.Fragment_Settings;

import android.app.Fragment;

//
// - TODO
// - Please fill in what this class has as a purpose to live
public class FragmentStore {

	public FragmentStore() {
		// TODO Auto-generated constructor stub
	}
	
	public static Fragment getFragmentForRow(int row){
		
		// - Selected fragment which will be returned
		Fragment selectedFragment;
		selectedFragment = new Fragment_Feed();
		
		if(row == 0){
			// - TODO
			selectedFragment = new Fragment_ProfileView();
		}
		else if(row == 1){
			// - TODO
		}
		else if(row == 2){
			selectedFragment = new Fragment_CheckIn();
		}
		else if(row == 3){
			// - TODO
		}
		else if(row == 4){
			selectedFragment = new Fragment_ConversationList();
		}
		else if(row == 5){
			selectedFragment = new Fragment_FriendList();
		}
		else if(row == 6){
			// - TODO
			selectedFragment = new Fragment_Settings();
		}
		
		return selectedFragment;
	}

}
