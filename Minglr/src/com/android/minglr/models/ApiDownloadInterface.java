/**
 * 
 */
package com.android.minglr.models;

import java.util.ArrayList;

/**
 * @author Robert
 *
 */
public interface ApiDownloadInterface {
	
	/**
	 * Is called when a download has been instigated and finished by an
	 * instance of ApiDownload
	 * @param result the array of api objects that has been downloaded
	 * @param variableName the variable name of the array list on the json object
	 */
	public void arrayFetchComplete(ArrayList<ApiObject> result, String variableName);
	
	/**
	 * 
	 * Is called when a single json object is downloaded by instigating an
	 * instance of ApiDownload
	 * @param result the ApiObject (based on json and hashmap)
	 * @param variableName the name with which we fetch the object
	 */
	public void objectFetchComplete(ApiObject result, String variableName);

}
