package com.android.minglr.models;

import java.util.Map;

import org.json.JSONObject;

/**
 * Will act as header for any ArrayAdapters which only handle ApiObjects
 * @author Robert
 *
 */
public class ApiObjectHeader extends ApiObject {

	public ApiObjectHeader() {
		// TODO Auto-generated constructor stub
	}

	public ApiObjectHeader(JSONObject json) {
		super(json);
		// TODO Auto-generated constructor stub
	}

	public ApiObjectHeader(Map<String, String> map) {
		super(map);
		// TODO Auto-generated constructor stub
	}
	
	public String headerText;
	public String headerID;

}
