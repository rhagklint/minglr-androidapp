package com.android.minglr.models;

import java.util.HashMap;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class FlashDrive {
	
	// - Singleton instance
	private static FlashDrive _instance;
	
	// - static member instance
	private static Context _context;
	
	public static void initInstance(Context context){
		
		if(_instance == null){
			_instance = new FlashDrive();
			_context = context;
		}
	}
	
	public static FlashDrive getInstance(){
		return _instance;
	}

	// - Singleton private constructor
	private FlashDrive() {
		// TODO Auto-generated constructor stub
	}
	
	//
	// - Save string variable to drive
	public void saveString(String value, String key){
		
		if(_context == null){
			System.out.println("CONTEXT IS NULL/VOID!");
		}
		
		//
		// - Get the preferences instance
		SharedPreferences preferences = _context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
		
		// - Set edit mode
		SharedPreferences.Editor editor = preferences.edit();
		
		editor.putString(key, value);
		editor.commit();
	}
	
	//
	// - Load string variable with key
	public String getString(String key){
		
		//
		// - Get the preferences instance
		SharedPreferences preferences = _context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
		
		//
		// - Get the value
		String value = preferences.getString(key, "");
		
		//
		// - Value
		return value;
	}

	/**
	 * Set and save user credentials which will be used when we communicate with the api
	 * @param username 
	 * @param password 
	 * @param context
	 */
	public void setCredentials(String username, String password){
		//
		// -  Get teh preferenes instance
		SharedPreferences preferences = _context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
		
		//
		// _ Set edit mode
		SharedPreferences.Editor editor = preferences.edit();
		
		// - Put values
		editor.putString("kCredentialsPassword", password);
		editor.putString("kCredentialsUsername", username);
		
		editor.commit();
	}
	
	/**
	 * Returns the credentials the user has saved from the last login
	 * @return a HashMap with keys "password" and "username"
	 */
	public static HashMap<String, String> getCredentials(){
		//
		// - Get the preferences
		SharedPreferences preferences = _context.getSharedPreferences("MyPreferences", Context.MODE_PRIVATE);
		
		//
		// - This is the map with which we return the credentials saved on the device
		HashMap <String, String> credentialsMap = new HashMap<String, String>();
		
		// - Get password
		String password = preferences.getString("kCredentialsPassword", "");
		
		// - Get username
		String username = preferences.getString("kCredentialsUsername", "");
		
		if(password == null){
			password = "";
		}
		if(username == null){
			username = "";
		}
		
		credentialsMap.put("password", password);
		credentialsMap.put("username", username);
		
		// - Return
		return credentialsMap;
	}

}
