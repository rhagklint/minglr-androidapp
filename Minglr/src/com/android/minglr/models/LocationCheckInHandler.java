package com.android.minglr.models;

import java.net.URL;
import java.util.ArrayList;

import android.content.Context;

/**
 * This class will handle the user check-ins
 * Upon app startup this singleton will download the info if the user
 * is checked in somewhere
 * 
 * It will also offer convenience methods for uploading/posting a new
 * check-in location
 * 
 * 
 * @author Robert
 *
 */
public class LocationCheckInHandler {
	
	
	// ======================================================================================== //
	// ************************************** SINGLETON STUFF ********************************* //
	// ======================================================================================== //
	
	// - Singleton instance
	private static LocationCheckInHandler _instance;
	
	// - Init
	public static void initInstance(Context context){
		if(_instance == null){
			_instance = new LocationCheckInHandler();
		}
	}
	
	public static LocationCheckInHandler getInstance(){
		if(_instance == null){
			initInstance(null);
		}
		return _instance;
	}

	private LocationCheckInHandler() {
		// TODO Auto-generated constructor stub
	}
	
	// ======================================================================================== //
	// ************************************** SINGLETON STUFF ********************************* //
	// ======================================================================================== //
	
	
	/*
	 * If the user is checked in to any location_id, this will be 
	 * that
	 */
	private String _checkedinID;
	public Boolean isCheckedInto(String location_id){
		
		System.out.println("_checkedinID: " + _checkedinID);
		System.out.println("location_id: + " + location_id);
		
		if(_checkedinID == null)
			return false;
		
		return _checkedinID.equals(location_id);
	}
	
	/**
	 * Returns the location_id of the location the user is checked in to.
	 * Returns nil of there is none
	 */
	public String checkedInIdentifier(){
		return _checkedinID;
	}
	
	//
	// - Download from server/api
	// ======================================================================================== //
	// ************************************* DOWNLOADS **************************************** //
	// ======================================================================================== //
	
	/**
	 * Will update where the user is checked in
	 * Will also save the check-in locaion if it is valid, and there is one
	 */
	public void downloadCheckInLocation(final ApiDownloadInterface interfaceObject){
		
		ApiDownload download = new ApiDownload();
		download.startDownload("location/get_checkin", "", true, new ApiDownloadInterface() {
			
			@Override
			public void objectFetchComplete(ApiObject result, String variableName) {
				
				System.out.println("Result when getting checkin: " + result.getObject().toString());
				
				
				//
				// - First we need to check if the result is actually a location
				// - If it is not, we have no check-in location
				String resultString = result.getObject().get("location");
				System.out.println("ResultString: " + resultString);
				if(resultString.equals("false")){
					return;
				}
				
				// - Refine the result and get the hashmap for "location" variable in the result
				ApiObject location = ApiObjectHandler.refineObject(result, "location");
				String location_id = ApiObjectHandler.LocationObject.getId(location);
				System.out.println("Check in locatio id: " + location_id);
				
				_checkedinID = location_id;
				
				//
				// - Pass it along
				if(interfaceObject != null)
					interfaceObject.objectFetchComplete(result, variableName);
			}
			
			@Override
			public void arrayFetchComplete(ArrayList<ApiObject> result,
					String variableName) {
			}
		});
	}
	
	/**
	 * This function
	 * @param location_id
	 */
	public void getUserCheckInForLocation(String location_id, ApiDownloadInterface interfaceObject){
		
	}
	
	// ======================================================================================== //
	// ************************************* DOWNLOADS **************************************** //
	// ======================================================================================== //
	
	//
	// - Updates to server/api
	// ======================================================================================== //
	// *************************************** UPLOADS **************************************** //
	// ======================================================================================== //
	/**
	 * Posts an update to the server telling the user has 
	 * checked in to location_id
	 * @param location_id
	 */
	public void postCheckinToLocationId(String location_id, ApiDownloadInterface interfaceObject){
		
		String url = "location/add_visitor";
		
		ApiPost post = new ApiPost();
		post.addParameter("location_id", location_id);
		
		post.startPostDataTo(url, new ApiPostInterface() {
			
			@Override
			public void postDataComplete(String response) {
				// TODO Auto-generated method stub
				System.out.println("PostData complete on checkin: " + response);
			}
		});
	}
	
	// ======================================================================================== //
		// *************************************** UPLOADS **************************************** //
		// ======================================================================================== //

}

