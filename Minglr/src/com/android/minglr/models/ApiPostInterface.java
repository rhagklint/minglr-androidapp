package com.android.minglr.models;

public interface ApiPostInterface {

	
	/**
	 * Will be called when the post is complete in the ApiPostTask
	 * @param response
	 */
	public void postDataComplete(String response);

}
