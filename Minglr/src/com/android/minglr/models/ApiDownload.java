package com.android.minglr.models;

import java.util.ArrayList;

import android.R.bool;
import android.content.Context;

import com.android.minglr.tasks.SimpleJsonFetcher;
import com.android.minglr.tasks.SingleJsonFetcher;

//*************************************//
// - This class will have all the tools for the subclass to 
//*************************************//
public class ApiDownload{

	private ApiDownloadInterface _object;
	static String _baseURL = "http://api-test.minglr.se/";
	
	public ApiDownload() {
		// TODO Auto-generated constructor stub
	}
	
	private Context _context;
	
	//
	// - Instance variables
	
	// - The string which gets used when we want to fetch data for the list adapter
	String  _urlGetString;
	public void setURLString(String url){
		_urlGetString = url;
	}
	
	// - The variable which will contain the array that will be displayed in the data we fetched
	String _arrayVariable;
	public void setArrayVariable(String arrayVariable){
		_arrayVariable = arrayVariable;
	}
	
	// - This variable will be set when we KNOW that we are only expecting ONE apiObject in return
	String _objectVariable;
	public void setObjectVariable(String objectVariable){
		_objectVariable = objectVariable;
	}
	
	/****************** PRIVATE CLASS *********************/
	//
	// - GetDataTask to fetch data
	private class GetDataTask extends SimpleJsonFetcher{
		
		@Override
		protected void onPostExecute(ArrayList<ApiObject> result) {
			_object.arrayFetchComplete(result, _arrayVariable);
		}

	}
	
	//
	// _ This class gets the data as a single object instead of as an array
	private class GetSingleDataTask extends SingleJsonFetcher{
		
		@Override
		protected void onPostExecute(ApiObject object){
			_object.objectFetchComplete(object, _objectVariable);
		}
	}
	//
	// - 
	/****************** PRIVATE CLASS *********************/
	
	//
	// _ SET OBJECT WHICH WILL arrayFetchComplete(param)
	public void setFetchingInstance(ApiDownloadInterface instance){
		_object = instance;
	}
	
	//
	// _ Start download task if we have the variables set before hand
	public void startDownload(){
		//
		// - Start the fetching of data
		GetDataTask task = new GetDataTask();
		
		//
		// - Url for the request
		task.setGetRequestURL(_urlGetString);
		
		// 
		// - The "location" variable contains the list of locations
		task.setArrayVariable(_arrayVariable);
		task.execute();
	}
	
	//
	// - Start the download with params
	public void startDownload(String urlString, String arrayVariable){
		
		String completeURLString;
		if(!urlString.contains("minglr")){
			completeURLString = _baseURL.concat(urlString);
		}
		else{
			completeURLString = urlString;
		}
		
		//
		// - Start the fetching of data
		if(_objectVariable == null){
			GetDataTask task = new GetDataTask();
			
			//
			// - Url for the request
			task.setGetRequestURL(completeURLString);
			
			_urlGetString = urlString;
			_arrayVariable = arrayVariable;
			
			// 
			// - The "location" variable contains the list of locations
			task.setArrayVariable(arrayVariable);
			task.execute();
		}
		else{
			
			GetSingleDataTask task = new GetSingleDataTask();
			
			task.setGetRequestURL(completeURLString);
			
			task.execute();
		}
	}
	
	//
	// - Start download with the params AND the instance object
	public void startDownload(String urlString, String arrayVariable, ApiDownloadInterface instanceObject){
		_object = instanceObject;
		
		this.startDownload(urlString, arrayVariable);
	}
	
	//
	// - Start download with singular object as target download
	public void startDownload(
			String urlString, 
			String variable, 
			boolean singleObject,
			ApiDownloadInterface instanceObject){
		
		_object = instanceObject;
		
		if(singleObject == true)
			_objectVariable = variable;
		
		this.startDownload(urlString, variable);
	}
	
}
