package com.android.minglr.models;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.android.minglr.tasks.DownloadImageTask;

/**
 * An instance that downloads an image with specified string
 * @author Robert
 *
 */
public class ImageDownload {

	ImageDownloadInterface _interfaceObject;
	String _urlString;
	
	public ImageDownload() {
		// TODO Auto-generated constructor stub
	}
	
	public void downloadImage(String urlString, ImageDownloadInterface interfaceObject){
		
		//
		// - Set interface object
		_interfaceObject = interfaceObject;
		
		//
		// - Create a download image task
		DownloadImage task = new DownloadImage(urlString);
		task.execute();
	}
	
	//
	// - Private class extends DownloadImageTask
	private class DownloadImage extends DownloadImageTask{

		public DownloadImage(String url) {
			super(url);
			// TODO Auto-generated constructor stub
		}
		
		@Override
		protected void onPostExecute(Drawable drawableImage){
			ArrayList<Drawable> imagesArrayList = new ArrayList<Drawable>();
			imagesArrayList.add(drawableImage);
			
			_interfaceObject.imageListFinished(imagesArrayList);
		}
		
	}

}
