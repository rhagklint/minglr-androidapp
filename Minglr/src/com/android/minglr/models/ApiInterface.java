package com.android.minglr.models;


import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import android.os.AsyncTask;


public class ApiInterface{
	
	static String API_ROOT = "http://api-test.minglr.se/";
 	static String REST_API_KEY = "2udn1gvz9h6f3dl22yjep8qta75a83pvhnwqumyp";
 	static String METHOD_POST = "POST";
 	static String METHOD_GET = "GET";
 	
 	//
 	// _ API METHODS
 	static String API_GET_USER = API_ROOT + "user/get";
 	
  

	public ApiInterface() {
		// TODO Auto-generated constructor stub
	}
	
	
	/*
	public void getUser(String user_id){
		SimpleJsonFetcher jsonFetcher = new SimpleJsonFetcher();
		jsonFetcher.execute(API_ROOT + API_GET_USER);
		
	}
	
	public AsyncTask<String, Integer, String> getTask(){
		SimpleJsonFetcher jsonFetcher = new SimpleJsonFetcher();
		return jsonFetcher.execute(API_ROOT + API_GET_USER);
	}
	*/
	
	/** Task **/
    private class SimpleJsonFetcher extends AsyncTask<String, Integer, String> {
     
    	@Override
        protected void onPreExecute() {
    		// - TODO
        }
         
        @Override
        protected String doInBackground(String... url) {
        	// - TODO
        	HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response;
            String responseString = null;
        	
            try {
                response = httpclient.execute(new HttpGet(url[0]));
                StatusLine statusLine = response.getStatusLine();
                if(statusLine.getStatusCode() == HttpStatus.SC_OK){
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    response.getEntity().writeTo(out);
                    out.close();
                    responseString = out.toString();
                } else{
                    //Closes the connection.
                    response.getEntity().getContent().close();
                    throw new IOException(statusLine.getReasonPhrase());
                }
            } catch (ClientProtocolException e) {
                //TODO Handle problems..
            } catch (IOException e) {
                //TODO Handle problems..
            }
            return responseString;
        }
         
        @Override
        protected void onProgressUpdate(Integer... progress) {
        	// - TODO
        }
         
        @Override
        protected void onPostExecute(String result) {
        	// Publish result
       }
    }
}
