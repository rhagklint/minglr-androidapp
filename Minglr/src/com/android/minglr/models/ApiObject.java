package com.android.minglr.models;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;


//
// - Holds the api json object and acts as a simple wrapper
// - user ApiObjectHandler to get values from this object
// - Structurally this is a super class for the location, person, feedObject api objects
// - which will have their own classes
public class ApiObject {
	
	Map <String, String> _baseObject;
	JSONObject _jsonObject;
	
	// - Convert to map
	private Map <String, String> convertJSON(JSONObject object){
		
		// - Allocate the map which will be returned
		Map <String, String> map  = new HashMap <String, String>();
		
		// - Try
		try{
			// - Get all the keys
			Iterator keys = object.keys();
			
			// - Now iterate thru all the keys
			while(keys.hasNext()){
				// - Get CURRENT key
				String key = (String) keys.next();
				
				// - Put the value for that key in the map with the key
				map.put(key, object.getString(key));
			}
		} catch(JSONException e){
			e.printStackTrace();
		}
		
		return map;
	}

	//
	// - CONSTRUCTORS
	public ApiObject() {
		// TODO Auto-generated constructor stub
		_baseObject = new HashMap <String, String>();

	}
	
	public ApiObject(JSONObject json){
		// _ Use converter
		_baseObject = this.convertJSON(json);
		_jsonObject = json;
	}
	
	public ApiObject(Map<String, String> map){
		_baseObject = map;
	}
	
	public void setBaseObject(Map <String, String> map){
		_baseObject = map;
	}
	
	// - Get
	public Map<String, String> getObject() {
		// TODO Auto-generated method stub
		return _baseObject;
	}
	
	public JSONObject getJSON(){
		return _jsonObject;
	}
	
	
	
	// *********************************************** //
	// ------------------ SUBCLASSES ---------------- //
	// ********************************************* //

}
