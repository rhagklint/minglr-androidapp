package com.android.minglr.models;

import android.location.Location;

public interface LocationListenerInterface {
	
	//
	// - DOCS
	// - this function is called within the custom LocationManager class
	// - to the registred listener
	public void didUpdateToLocation(Location newLocation);

}
