package com.android.minglr.views;

import java.util.ArrayList;

import com.android.minglr.adapters.ArrayAdapterApiObject;
import com.android.minglr.models.ApiObject;
import com.example.android.navigationdrawerexample.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.PopupWindow;

public class ClusterPopup extends PopupWindow {

	//
	// - ListView
	private ListView _listView;
	
	public ClusterPopup() {
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(View contentView) {
		super(contentView);
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(int width, int height) {
		super(width, height);
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(View contentView, int width, int height) {
		super(contentView, width, height);
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(Context context, AttributeSet attrs, int defStyleAttr,
			int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}

	public ClusterPopup(View contentView, int width, int height,
			boolean focusable) {
		super(contentView, width, height, focusable);
		// TODO Auto-generated constructor stub
		
		//_listView = (ListView) contentView.findViewById(R.id.listView1);
	}
	
	public ClusterPopup(View contentView, int width, int height,
			boolean focusable, ArrayList <ApiObject> list, Context context, LayoutInflater inflater){
		
		super(contentView, width, height, focusable);
		
		
		ArrayAdapterApiObject adapter = new ArrayAdapterApiObject(
				context, 
				android.R.layout.simple_list_item_1, 
				list);
		
		_listView = (ListView) contentView.findViewById(R.id.listView1);
		//_listView = (ListView) inflater.inflate(R.id.listView1, (ViewGroup) contentView, false);
		
		//_listView.setAdapter(adapter);
	}
	
	

}
